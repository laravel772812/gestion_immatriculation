<?php



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

});


Route::get('/etudiants',[
    'uses' => 'EtudiantsController@index',
    'as' => 'etudiant'
]);
Route::get('/etudiants/add',[
    'uses' => 'EtudiantsController@add',
    'as' => 'etudiant.add'
]);
Route::post('/etudiant/store',[
    'uses' => 'EtudiantsController@store',
    'as' => 'etudiant.store'
]);
Route::get('/etudiants/delete/{id}',[
    'uses' => 'EtudiantsController@delete',
    'as' => 'etudiant.delete'
]);
Route::get('/etudiants/show/{id}',[
    'uses' => 'EtudiantsController@show',
    'as' => 'etudiant.show'
]);
Route::get('/etudiants/edit/{id}',[
    'uses' => 'EtudiantsController@edit',
    'as' => 'etudiant.edit'
]);
Route::get('/inscription',[
    'uses' => 'inscriptionController@index',
    'as' => 'inscription'
])->middleware('auth');
Route::get('/inscription/principaux_renseignement',[
    'uses' => 'pers_physController@pers',
    'as' => 'inscription.pers'
])->middleware('auth');

Route::get('/inscription/principaux_renseignement/show',[
    'uses' => 'pers_physController@show',
    'as' => 'pers.show'
])->middleware('auth');
Route::get('/inscription/persPhys',[
    'uses' => 'pers_physController@persPhys',
    'as' => 'inscription.persPhys'
])->middleware('auth');
Route::post('/inscription/persPhys',[
    'uses' => 'pers_physController@persPhys',
    'as' => 'inscription.persPhys'
])->middleware('auth');
Route::get('/pers_phys/delete/{id}',[
    'uses' => 'pers_physController@delete',
    'as' => 'pers_phys.delete'
])->middleware('auth:admin');

Route::get('/inscription/activites',[
    'uses' => 'activitesController@activites',
    'as' => 'inscription.activites'
])->middleware('auth');
Route::get('/inscription/activites/add',[
    'uses' => 'activitesController@activites_add',
    'as' => 'activites.add'
])->middleware('auth');
Route::post('/inscription/activites/adds',[
    'uses' => 'activitesController@activite_adds',
    'as' => 'inscription.activites_adds'
])->middleware('auth');
Route::get('/inscription/sieges',[
    'uses' => 'pers_physController@sieges',
    'as' => 'inscription.sieges'
]);
Route::post('/inscription/sieges',[
    'uses' => 'pers_physController@sieges',
    'as' => 'inscription.sieges'
]);
Route::get('/inscription/etablissements',[
    'uses' => 'etablissementsController@etablissements',
    'as' => 'inscription.etablissements'
])->middleware('auth');


Route::get('/etablissements/add',[
    'uses' => 'etablissementsController@etablissements_add',
    'as' => 'etablissements.add'
])->middleware('auth');
Route::post('/etablissements/add',[
    'uses' => 'etablissementsController@etablissements_adds',
    'as' => 'etablissements.adds'
]);

Route::get('/etablissements/delete/{id}',[
    'uses' => 'etablissementsController@delete',
    'as' => 'etablissements.delete'
])->middleware('auth');
Route::get('/inscription/dirigeants',[
    'uses' => 'dirigeantsController@dirigeants',
    'as' => 'inscription.dirigeants'
])->middleware('auth');
Route::get('/inscription/dirigeants/add',[
    'uses' => 'dirigeantsController@dirigeants_add',
    'as' => 'dirigeants.add'
]);
Route::post('/inscription/dirigeants/adds',[
    'uses' => 'dirigeantsController@dirigeants_adds',
    'as' => 'dirigeants.adds'
])->middleware('auth');
Route::get('/dirigeants/delete/{id}',[
    'uses' => 'dirigeantsController@delete',
    'as' => 'dirigeants.delete'
])->middleware('auth');
Route::get('/vehicules/delete/{id}',[
    'uses' => 'vehiculesController@delete',
    'as' => 'vehicules.delete'
])->middleware('auth');
Route::get('/inscription/vehicules',[
    'uses' => 'vehiculesController@vehicules',
    'as' => 'inscription.vehicules'
])->middleware('auth');
Route::get('/inscription/vehicules/add',[
    'uses' => 'vehiculesController@vehicules_add',
    'as' => 'vehicules.add'
])->middleware('auth');
Route::post('/inscription/vehicules/adds',[
    'uses' => 'vehiculesController@vehicules_adds',
    'as' => 'vehicules.adds'
])->middleware('auth');
Route::get('/inscription/interlocuteurs/add',[
    'uses' => 'interlocuteursController@interlocuteurs_add',
    'as' => 'interlocuteurs.add'
])->middleware('auth');
Route::post('/inscription/interlocuteurs/adds',[
    'uses' => 'interlocuteursController@interlocuteurs_adds',
    'as' => 'interlocuteurs.adds'
])->middleware('auth');

Route::get('/inscription/reference',[
    'uses' => 'referencesController@reference_add',
    'as' => 'references'
])->middleware('auth');
Route::post('/inscription/reference_add',[
    'uses' => 'referencesController@reference_adds',
    'as' => 'references.add'
])->middleware('auth');
Route::post('/inscription/reference_add',[
    'uses' => 'referencesController@reference_adds',
    'as' => 'references.add'
])->middleware('auth');
Route::get('/inscription/reference_print',[
    'uses' => 'referencesController@reference_print',
    'as' => 'references.print'
])->middleware('auth');
Route::get('/inscription/reference_liste',[
    'uses' => 'referencesController@reference_liste',
    'as' => 'references.liste'
])->middleware('auth:admin');
Route::get('/references/show/{id}',[
    'uses' => 'referencesController@show',
    'as' => 'references.show'
])->middleware('auth:admin');
Route::get('/references/edit/{id}',[
    'uses' => 'referencesController@edit',
    'as' => 'references.edit'
])->middleware('auth:admin');
Route::post('/references/edit/{id}',[
    'uses' => 'referencesController@update',
    'as' => 'references.update'
])->middleware('auth:admin');
Route::get('/references/edit/{id}',[
    'uses' => 'referencesController@delete',
    'as' => 'references.delete'
])->middleware('auth:admin');
Route::get('/references/search',[
    'uses' => 'referencesController@search',
    'as' => 'references.search'
])->middleware('auth:admin');

Route::get('/inscription/personne_moral',[
    'uses' => 'pers_moralController@persMoral',
    'as' => 'inscription.persMoral'
])->middleware('auth');
Route::post('/inscription/personne_moral',[
    'uses' => 'pers_moralController@persMoral_add',
    'as' => 'inscription.persMoral'
])->middleware('auth');


//moral



Route::get('/inscription/etablissements_moral',[
    'uses' => 'etableController@etablissements',
    'as' => 'inscription.etablissements_m'
])->middleware('auth');


Route::get('/etablissements_m/add',[
    'uses' => 'etableController@etablissements_add',
    'as' => 'etablissements_m.add'
])->middleware('auth');
Route::post('/etablissementsm/add',[
    'uses' => 'etableController@etablissements_adds',
    'as' => 'etablissements_m.adds'
]);
Route::get('/etablissements_m/delete/{id}',[
    'uses' => 'etableController@delete',
    'as' => 'etablissements_m.delete'
])->middleware('auth:admin');

Route::get('/inscription/associer',[
    'uses' => 'associesController@associer',
    'as' => 'inscription.associer'
])->middleware('auth');


Route::get('/associer/add',[
    'uses' => 'associesController@associer_add',
    'as' => 'associer.add'
])->middleware('auth');
Route::post('/associer/adds',[
    'uses' => 'associesController@associer_adds',
    'as' => 'associer.adds'
]);
Route::get('/associer/delete/{id}',[
    'uses' => 'associesController@delete',
    'as' => 'associer.delete'
])->middleware('auth');

Route::get('/inscription/dirigeant_m',[
    'uses' => 'dirigeant_mController@dirigeants',
    'as' => 'inscription.dirigeants_m'
])->middleware('auth');


Route::get('/dirigeant_m/add',[
    'uses' => 'dirigeant_mController@dirigeants_add',
    'as' => 'dirigeants_m.add'
])->middleware('auth');
Route::post('/dirigeant_m/adds',[
    'uses' => 'dirigeant_mController@dirigeants_adds',
    'as' => 'dirigeants_m.adds'
]);
Route::get('/dirgeantçm/delete/{id}',[
    'uses' => 'dirigeant_mController@delete',
    'as' => 'dirigeants_m.delete'
])->middleware('auth');

Route::get('/inscription/vehicule_m',[
    'uses' => 'vehicule_mController@vehicules',
    'as' => 'inscription.vehicules_m'
])->middleware('auth');
Route::get('/vehicule_m/add',[
    'uses' => 'vehicule_mController@vehicules_add',
    'as' => 'vehicules_m.add'
])->middleware('auth');
Route::post('/vehicule_m/adds',[
    'uses' => 'vehicule_mController@vehicules_adds',
    'as' => 'vehicules_m.adds'
]);
Route::get('/vehicule_m/delete/{id}',[
    'uses' => 'vehicule_mController@delete',
    'as' => 'vehicules_m.delete'
])->middleware('auth:admin');

Route::get('/inscription/interlocuteurs_m/add',[
    'uses' => 'interlocuteurs_mController@interlocuteurs_add',
    'as' => 'interlocuteurs_m.add'
])->middleware('auth');
Route::post('/inscription/interlocuteurs_m/adds',[
    'uses' => 'interlocuteurs_mController@interlocuteurs_adds',
    'as' => 'interlocuteurs_m.adds'
])->middleware('auth');
Route::get('/inscription/reference_m',[
    'uses' => 'references_mController@reference_add',
    'as' => 'references_m'
])->middleware('auth');
Route::get('/inscription/confirmation',[
    'uses' => 'references_mController@confirmation',
    'as' => 'confirmation'
])->middleware('auth:admin');
Route::post('/inscription/reference_m_add',[
    'uses' => 'references_mController@reference_adds',
    'as' => 'references_m.add'
])->middleware('auth');
Route::post('/inscription/reference_m_add',[
    'uses' => 'references_mController@reference_adds',
    'as' => 'references_m.add'
])->middleware('auth');
Route::get('/inscription/reference_m_print',[
    'uses' => 'references_mController@reference_print',
    'as' => 'references_m.print'
])->middleware('auth');
Route::get('/inscription/reference_m_liste',[
    'uses' => 'references_mController@reference_liste',
    'as' => 'references_m.liste'
])->middleware('auth:admin');
Route::get('/references_m/show/{id}',[
    'uses' => 'references_mController@show',
    'as' => 'references_m.show'
])->middleware('auth:admin');
Route::get('/references_m/edit/{id}',[
    'uses' => 'references_mController@edit',
    'as' => 'references_m.edit'
])->middleware('auth:admin');
Route::post('/references_m/edit/{id}',[
    'uses' => 'references_mController@update',
    'as' => 'references_m.update'
])->middleware('auth:admin');
Route::get('/references_m/edit/{id}',[
    'uses' => 'references_mController@delete',
    'as' => 'references_m.delete'
])->middleware('auth:admin');
Route::get('/references_m/search',[
    'uses' => 'references_mController@search',
    'as' => 'references_m.search'
])->middleware('auth:admin');



//requete
Route::get('/liste_inscrit',[
    'uses' => 'pers_physController@liste_inscrit',
    'as' => 'liste_inscrit'
])->middleware('auth:admin');

Route::get('/liste_inscrit/search',[
    'uses' => 'pers_physController@search',
    'as' => 'liste_inscrit.search'
])->middleware('auth:admin');
Route::get('/liste_inscrit/between',[
    'uses' => 'pers_physController@between',
    'as' => 'liste_inscrit.between'
])->middleware('auth:admin');
Route::get('/liste_inscrit/date_choisie',[
    'uses' => 'pers_physController@date_choisie',
    'as' => 'liste_inscrit.date_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/month_choisie',[
    'uses' => 'pers_physController@month_choisie',
    'as' => 'liste_inscrit.month_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/day_choisie',[
    'uses' => 'pers_physController@day_choisie',
    'as' => 'liste_inscrit.day_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/annee_choisie',[
    'uses' => 'pers_physController@annee_choisie',
    'as' => 'liste_inscrit.annee_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/fokontany',[
    'uses' => 'pers_physController@fokontany',
    'as' => 'liste_inscrit.fokontany'
])->middleware('auth:admin');
Route::get('/liste_inscrit/commune',[
    'uses' => 'pers_physController@commune',
    'as' => 'liste_inscrit.commune'
]);
Route::get('/liste_inscrit/district',[
    'uses' => 'pers_physController@district',
    'as' => 'liste_inscrit.district'
])->middleware('auth:admin');
Route::get('/liste_inscrit/region',[
    'uses' => 'pers_physController@region',
    'as' => 'liste_inscrit.region'
])->middleware('auth:admin');
Route::get('/liste_inscrit/province',[
    'uses' => 'pers_physController@province',
    'as' => 'liste_inscrit.province'
]);
Route::get('/wizard',[
    'uses' => 'pers_physController@wizard',
    'as' => 'wizard'
]);
Route::get('/charts',[
    'uses' => 'chartsController@charts',
    'as' => 'charts'
]);
Route::get('/admin/inscrit',[
    'uses' => 'inscriptionController@inscrit',
    'as' => 'inscrit'
])->middleware('auth:admin');


Route::get('/admin/etablissements',[
    'uses' => 'inscriptionController@etablissements',
    'as' => 'etablissements'
])->middleware('auth:admin');
Route::get('/admin/etablissements/delete/{id}',[
    'uses' => 'inscriptionController@deleteEtabl',
    'as' => 'etabl.delete'
])->middleware('auth:admin');
Route::get('/admin/etablissements/search',[
    'uses' => 'inscriptionController@searchEtabl',
    'as' => 'etablSearch'
])->middleware('auth:admin');

Route::get('/admin/dirigeants',[
    'uses' => 'inscriptionController@dirigeants',
    'as' => 'dirigeants'
])->middleware('auth:admin');
Route::get('/admin/dirigeants/delete/{id}',[
    'uses' => 'inscriptionController@deleteDir',
    'as' => 'dir.delete'
])->middleware('auth:admin');
Route::get('/admin/dirigeants/search',[
    'uses' => 'inscriptionController@searchDir',
    'as' => 'dirSearch'
])->middleware('auth:admin');

Route::get('/admin/vehicules',[
    'uses' => 'inscriptionController@vehicules',
    'as' => 'vehicules'
])->middleware('auth:admin');
Route::get('/admin/vehicules/delete/{id}',[
    'uses' => 'inscriptionController@deleteVeh',
    'as' => 'veh.delete'
])->middleware('auth:admin');

Route::get('/admin/vehicules/search',[
    'uses' => 'inscriptionController@searchVeh',
    'as' => 'vehSearch'
])->middleware('auth:admin');

Route::get('/admin/interlocuteurs',[
    'uses' => 'inscriptionController@interlocuteurs',
    'as' => 'interlocuteurs'
])->middleware('auth:admin');
Route::get('/admin/interlocuteurs/delete/{id}',[
    'uses' => 'inscriptionController@deleteInter',
    'as' => 'inter.delete'
])->middleware('auth:admin');

Route::get('/admin/interlocuteurs/search',[
    'uses' => 'inscriptionController@searchInter',
    'as' => 'interSearch'
])->middleware('auth:admin');



Route::get('/admin/pers_morals',[
    'uses' => 'inscriptionController@pers_morals',
    'as' => 'pers_morals'
])->middleware('auth:admin');
Route::get('/admin/pers_morals/delete/{id}',[
    'uses' => 'inscriptionController@deleteMorals',
    'as' => 'deleteMorals'
])->middleware('auth:admin');

Route::get('/admin/pers_morals/search',[
    'uses' => 'inscriptionController@searchMorals',
    'as' => 'searchMorals'
])->middleware('auth:admin');


Route::get('/liste_inscrit/between_m',[
    'uses' => 'inscriptionController@between',
    'as' => 'liste_inscrit.between'
])->middleware('auth:admin');
Route::get('/liste_inscrit/date_choisie_m',[
    'uses' => 'inscriptionController@date_choisie',
    'as' => 'liste_inscrit.date_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/month_choisie_m',[
    'uses' => 'inscriptionController@month_choisie',
    'as' => 'liste_inscrit.month_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/day_choisie_m',[
    'uses' => 'inscriptionController@day_choisie',
    'as' => 'liste_inscrit.day_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/annee_choisie_m',[
    'uses' => 'inscriptionController@annee_choisie',
    'as' => 'liste_inscrit.annee_choisie'
])->middleware('auth:admin');
Route::get('/liste_inscrit/fokontany_m',[
    'uses' => 'inscriptionController@fokontany',
    'as' => 'liste_inscrit.fokontany'
])->middleware('auth:admin');
Route::get('/liste_inscrit/fokontany_mm',[
    'uses' => 'inscriptionController@fokontany_m',
    'as' => 'liste_inscrit.fokontany_m'
])->middleware('auth:admin');



Route::get('/admin/etablissements_m',[
    'uses' => 'inscriptionController@etablissements_m',
    'as' => 'etablissements_m'
])->middleware('auth:admin');
Route::get('/admin/etablissements_m/delete/{id}',[
    'uses' => 'inscriptionController@deleteEtabl_m',
    'as' => 'etabl_m.delete'
])->middleware('auth:admin');
Route::get('/admin/etablissements_m/search',[
    'uses' => 'inscriptionController@searchEtabl_m',
    'as' => 'etabl_mSearch'
])->middleware('auth:admin');


Route::get('/admin/dirigeants_m',[
    'uses' => 'inscriptionController@dirigeants_m',
    'as' => 'dirigeants_m'
])->middleware('auth:admin');
Route::get('/admin/dirigeants_m/delete/{id}',[
    'uses' => 'inscriptionController@deleteDir_m',
    'as' => 'dir_m.delete'
])->middleware('auth:admin');
Route::get('/admin/dirigeants_m/search',[
    'uses' => 'inscriptionController@searchDir_m',
    'as' => 'dir_mSearch'
])->middleware('auth:admin');


Route::get('/admin/vehicules_m',[
    'uses' => 'inscriptionController@vehicules_m',
    'as' => 'vehicules_m'
])->middleware('auth:admin');

Route::get('/admin/vehicules_m/delete/{id}',[
    'uses' => 'inscriptionController@deleteVeh_m',
    'as' => 'veh_m.delete'
])->middleware('auth:admin');

Route::get('/admin/vehicules_m/search',[
    'uses' => 'inscriptionController@searchVeh_m',
    'as' => 'veh_mSearch'
])->middleware('auth:admin');

Route::get('/admin/interlocuteur_m',[
    'uses' => 'inscriptionController@interlocuteurs_m',
    'as' => 'interlocuteurs_m'
])->middleware('auth:admin');
Route::get('/admin/interlocuteurs_m/delete/{id}',[
    'uses' => 'inscriptionController@deleteInter_m',
    'as' => 'inter_m.delete'
])->middleware('auth:admin');
Route::get('/admin/interlocuteurs_m/search',[
    'uses' => 'inscriptionController@searchInter_m',
    'as' => 'inter_mSearch'
])->middleware('auth:admin');



Route::get('/get-post-chart-data', 'ChartsController@getMonthlyPostData');
Route::get('/pie-charts', 'ChartsController@pieCharts');
Route::get('/charts-morales', 'ChartsController@bar');


Route::get('/admin/utilisateur_liste',[
    'uses' => 'utilisateurController@utilisateur_liste',
    'as' => 'utilisateur.liste'
])->middleware('auth:admin');
Route::get('/admin/utilisateur_show/{id}',[
    'uses' => 'utilisateurController@show',
    'as' => 'utilisateur.show'
])->middleware('auth:admin');
Route::get('/utilisateur/update/{id}',[
    'uses' => 'utilisateurController@update',
    'as' => 'utilisateur.update'
])->middleware('auth:admin');
Route::post('/utilisateur/update/{id}',[
    'uses' => 'utilisateurController@update',
    'as' => 'utilisateur.update'
])->middleware('auth:admin');
Route::get('/utilisateur/delete/{id}',[
    'uses' => 'utilisateurController@delete',
    'as' => 'utilisateur.delete'
])->middleware('auth:admin');

Route::get('/statistique',[
    'uses' => 'statistiqueController@statistique',
    'as' => 'statistique'
])->middleware('auth:admin');




Route::get('/references/carte_fiscal/{id}',[
    'uses' => 'references_mController@carte',
    'as' => 'carte'
])->middleware('auth:admin');
Route::get('/references/carte_p_fiscal/{id}',[
    'uses' => 'referencesController@carte_p',
    'as' => 'carte_p'
])->middleware('auth:admin');

