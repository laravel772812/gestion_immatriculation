<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Classes::class, function (Faker\Generator $faker) {
  return [
         'code'=> $faker->ean8,
         'name'=> $faker->unique()->name,
  ];
});