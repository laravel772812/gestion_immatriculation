<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Etudiants::class, function (Faker\Generator $faker) {
  return [
         'firstname'=> $faker->firstname,
         'lastname'=> $faker->lastname,
         'email'=> $faker->unique()->safeEmail,
         'phone'=> $faker->phoneNumber,
         'birthday'=> $faker->date,
         'classes_id'=> function(){
             return App\Models\Classes::all()->random();
         }
  ];
});