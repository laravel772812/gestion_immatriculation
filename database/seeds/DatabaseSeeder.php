<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
      factory(App\Models\Classes::class, 10)->create();
      factory(App\Models\Etudiants::class, 50)->create();

    }
}
