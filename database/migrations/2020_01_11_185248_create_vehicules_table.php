<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_veh');
            $table->string('marque')->nullable();
            $table->string('genre')->nullable();
            $table->string('type')->nullable();
            $table->string('puissance')->nullable();
            $table->integer('nbr_place')->nullable();
            $table->integer('charge')->nullable();
            $table->integer('poid')->nullable();
            $table->date('date_circ')->nullable();
            $table->string('usage_proff')->nullable();
            $table->date('date_debut')->nullable();
            $table->string('exploitation')->nullable();
            $table->integer('pers_phys_id')->unsigned()->index();
            $table->foreign('pers_phys_id')->references('id')->on('pers_phys')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
