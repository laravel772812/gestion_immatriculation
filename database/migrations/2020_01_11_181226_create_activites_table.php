<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('precision')->nullable();
            $table->string('reg_comm')->nullable();
            $table->date('date_reg_comm')->nullable();
            $table->string('debut');
            $table->string('cloture');
            $table->string('import');
            $table->string('export');
            $table->integer('nbr_salarier')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activites');
    }
}
