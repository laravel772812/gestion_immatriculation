<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirigeantMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dirigeant_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_dir_m');
            $table->string('fonction_m');
            $table->string('cin_dir_m');
            $table->string('adrs_dir_m');
            $table->string('email_dir_m');
            $table->string('tel_m');
            $table->string('autre_act');
            $table->string('activite_dir_m')->nullable();
            $table->string('nif_activite_dir_m')->nullable();
            $table->integer('etablissement_m_id')->unsigned()->index();
            $table->foreign('etablissement_m_id')->references('id')->on('etablissement_ms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dirigeant_ms');
    }
}
