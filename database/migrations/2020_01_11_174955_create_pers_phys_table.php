<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersPhysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pers_phys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_pers_phys');
            $table->string('prenom_pers_phys')->nullable();
            $table->string('sit_mat');
            $table->string('sexe');
            $table->string('cin');
            $table->date('date_del_cin')->nullable();
            $table->string('lieu_del_cin');
            $table->string('num_stat');
            $table->date('date_deliv_stat')->nullable();
            $table->date('date_naiss');
            $table->string('lieu_naiss');
            
            $table->string('adrs');
          

            $table->string('description');
            $table->string('precision')->nullable();
            $table->string('reg_comm')->nullable();
            $table->date('date_reg_comm')->nullable();
            $table->string('debut');
            $table->string('cloture');
            $table->string('import');
            $table->string('export');
            $table->integer('nbr_salarier')->nullable();

          
            $table->integer('capital')->nullable();
            $table->string('rib')->nullable();
            $table->string('reg_fisc')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pers_phys');
    }
}
