<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirigeantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dirigeants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_dir');
            $table->string('fonction');
            $table->string('cin_dir');
            $table->string('adrs_dir');
            $table->string('email_dir');
            $table->string('tel_dir');
            $table->string('activite_dir')->nullable();
            $table->string('nif_activite_dir')->nullable();
            $table->integer('etablissement_id')->unsigned()->index();
            $table->foreign('etablissement_id')->references('id')->on('etablissement')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dirigeants');
    }
}
