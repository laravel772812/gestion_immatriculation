<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtablissementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etablissements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_etabl')->nullable();
           
            $table->date('date_ouvert');
            $table->string('tel');
            $table->string('autre_tel')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('exportateur')->nullable();
            $table->string('importateur')->nullable();
            $table->string('type_prop')->nullable();
            $table->string('nif_prop')->nullable();
            $table->string('nom_prop')->nullable();
            $table->string('cin_prop')->nullable();
            $table->string('adrs_prop')->nullable();
            $table->string('tel_prop')->nullable();

            $table->integer('pers_phys_id')->unsigned()->index();
            $table->foreign('pers_phys_id')->references('id')->on('pers_phys')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etablissements');
    }
}
