<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterlocuteurMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interlocuteur_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_inter_m');
            $table->string('titre_m')->nullable();
            $table->string('adrs_inter_m');
            $table->string('tel_inter_m');
            $table->string('email_inter_m')->nullable();
            $table->integer('pers_moral_id')->unsigned()->index();
            $table->foreign('pers_moral_id')->references('id')->on('pers_morals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interlocuteur_ms');
    }
}
