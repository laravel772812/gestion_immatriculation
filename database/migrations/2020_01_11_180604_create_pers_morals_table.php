<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersMoralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pers_morals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('raison_social');
            $table->string('form_jurid');
           

            $table->string('adrs_m');
            $table->string('Fokontany');

            $table->string('description_m');
            $table->string('precision_m')->nullable();
            $table->string('reg_comm_m')->nullable();
            $table->date('date_reg_comm_m')->nullable();
            $table->string('debut_m');
            $table->string('cloture_m');
            $table->string('import_m');
            $table->string('export_m');
            $table->integer('nbr_salarier_m')->nullable();

           
            $table->integer('capital_m')->nullable();
            $table->string('rib_m')->nullable();
            $table->string('reg_fisc_m')->nullable();
            $table->integer('peri_grace_m')->nullable();
         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pers_morals');
    }
}
