<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEtablissementMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etablissement_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_etabl_m')->nullable();
           
            $table->date('date_ouvert_m');
            $table->string('tel_m');
            $table->string('autre_tel_m')->nullable();
            $table->string('fax_m')->nullable();
            $table->string('email_m')->nullable();
            $table->string('exportateur_m')->nullable();
            $table->string('importateur_m')->nullable();
            $table->string('type_prop_m')->nullable();
            $table->string('nif_prop_m')->nullable();
            $table->string('nom_prop_m')->nullable();
            $table->string('cin_prop_m')->nullable();
            $table->string('adrs_prop_m')->nullable();
            $table->string('tel_prop_m')->nullable();

            $table->integer('pers_moral_id')->unsigned()->index();
            $table->foreign('pers_moral_id')->references('id')->on('pers_morals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etablissement_ms');
    }
}
