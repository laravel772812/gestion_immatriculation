<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculeMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicule_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('num_veh_m');
            $table->string('marque_m')->nullable();
            $table->string('genre_m')->nullable();
            $table->string('type_m')->nullable();
            $table->string('puissance_m')->nullable();
            $table->integer('nbr_place_m')->nullable();
            $table->integer('charge_m')->nullable();
            $table->integer('poid_m')->nullable();
            $table->date('date_circ_m')->nullable();
            $table->string('usage_proff_m')->nullable();
            $table->date('date_debut')->nullable();
            $table->string('exploitation_m')->nullable();
            $table->integer('pers_moral_id')->unsigned()->index();
            $table->foreign('pers_moral_id')->references('id')->on('pers_morals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicule_ms');
    }
}
