<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterlocuteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interlocuteurs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_inter');
            $table->string('titre')->nullable();
            $table->string('adrs_inter');
            $table->string('tel_inter');
            $table->string('email_inter')->nullable();
            $table->integer('pers_phys_id')->unsigned()->index();
            $table->foreign('pers_phys_id')->references('id')->on('pers_phys')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interlocuteurs');
    }
}
