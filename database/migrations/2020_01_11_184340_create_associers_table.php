<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_ass');
            $table->string('nom_ass')->nullable();
            $table->string('fonction_ass')->nullable();
            $table->string('cin_ass')->nullable();
            $table->string('adrs_ass')->nullable();
            $table->string('activite_ass')->nullable();
            $table->string('nif_activite_ass')->nullable();
            $table->string('email_ass')->nullable();
            $table->string('tel_ass')->nullable();
            $table->string('ass_unique')->nullable();
            $table->string('action')->nullable();
            $table->string('nif')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associers');
    }
}
