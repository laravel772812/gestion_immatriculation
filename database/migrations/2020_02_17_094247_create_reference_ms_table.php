<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceMsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_ms', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date_ref_m');
            $table->string('confirmation_m');
            $table->string('nouveau_nif_m')->unique()->nullable();
            $table->integer('pers_moral_id')->unsigned()->index();
            $table->integer('etablissement_m_id')->unsigned()->index();
            $table->foreign('pers_moral_id')->references('id')->on('pers_morals')->onDelete('cascade');
            $table->foreign('etablissement_m_id')->references('id')->on('etablissement_ms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_ms');
    }
}
