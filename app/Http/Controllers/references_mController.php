<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reference_m;
use App\Models\Etablissement;
use App\Models\pers_moral;
use DB;
use Session;
class references_mController extends Controller
{
 

    public function reference_add()
    {
      
        $c=DB::table('pers_moral')->latest()->first();
        $e=DB::table('etablissement_ms')->latest()->first();
        return view('inscription.references_m')->with('pers',$c)->with('etabl',$e);
    }
    public function reference_adds(Request $request)
    {
        $this->validate($request,[
            'date_ref_m' => 'required',
            'pers_moral_id' => 'required',
            'nouveau_nif_m' => 'nullable|unique:reference_ms',
            'confirmation_m' => 'required',
        ],[
            'date_ref_m.required' => 'Champ obligatoire',
            'pers_moral_id.required' => 'Champ obligatoire',
            'nouveau_nif_m.unique' => 'le NIF est unique',
           
            'confirmation_m.email' => 'Champ obligatoire',
        ]);
         $ref = new Reference_m;
         $ref ->date_ref_m = $request ->date_ref_m;
         $ref ->confirmation_m= $request ->confirmation_m;
         $ref ->nouveau_nif_m= $request ->nouveau_nif_m;
         $ref ->pers_moral_id = $request ->pers_moral_id;
         $ref ->etablissement_m_id = $request ->etablissement_m_id;
         $ref->save();
         $c=DB::table('reference_ms')->latest()->first();
        
         $listes=DB::table('reference_ms')->latest()->first();
         
         Session::flash('success','ok');
         return view('inscription.references_m_show',['listes' => $listes,'ref' =>$c]);
        
    }
    function reference_show(){
        $search = \Request::get('title');
        $total = DB::table('pers_morals')->selectRaw('count(id) as total')->where('raison_social','like','%' .$search. '%' )->orwhere('id','like','%' .$search. '%' )->orderBy('id')->get();
        $listes = Pers_moral::where('raison_social','like','%' .$search. '%' )->orwhere('id','like','%' .$search. '%' )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_search',['listes' => $listes,'total' => $total ]);
    }
    function reference_print(){
       
        $search = \Request::get('a');
        $listes = DB::table('reference_ms')
        ->join('pers_morals', 'reference_ms.pers_moral_id', '=', 'pers_morals.id')
        ->join('etablissement_ms', 'reference_ms.etablissement_m_id', '=', 'etablissement_ms.id')
        ->select('reference_ms.*', 'pers_morals.*', 'etablissement_ms.*')
        ->where('reference_ms.id','=',$search)
        ->get();
       return view('inscription.print_m',['listes' => $listes]);
    }
    function reference_liste(){
      
        $listes = DB::table('reference_ms')
        ->join('pers_morals', 'reference_ms.pers_moral_id', '=', 'pers_morals.id')
        ->join('etablissement_ms', 'reference_ms.etablissement_m_id', '=', 'etablissement_ms.id')
        ->select('reference_ms.id','reference_ms.pers_moral_id','reference_ms.nouveau_nif_m','pers_morals.raison_social','etablissement_ms.nom_etabl_m')
        ->get();
       return view('requete.references_m_liste',['listes_ref' => $listes]);
    }
    function search(){
      $search=\Request::get('title');
        $listes = DB::table('reference_ms')
        ->join('pers_morals', 'reference_ms.pers_moral_id', '=', 'pers_morals.id')
        ->join('etablissement_ms', 'reference_ms.etablissement_m_id', '=', 'etablissement_ms.id')
        ->select('reference_ms.id','reference_ms.pers_moral_id','reference_ms.nouveau_nif_m','pers_morals.raison_social','etablissement_ms.nom_etabl_m')
        ->where('reference_ms.id', '=', $search)
        ->orWhere('pers_morals.raison_social', 'like','%' .$search. '%')
        ->orWhere('etablissement_ms.nom_etabl_m', 'like','%' .$search. '%')
        ->get();
        return view('requete.references_m_liste',['listes_ref' => $listes]);
    }
    public function show($id){
     
        $c=DB::table('pers_morals')->latest()->first();
        $e=DB::table('etablissement_ms')->latest()->first();
        $ref = Reference_m::where('id',$id)->first();
        return view('inscription.references_m_show_edit',['ref'=>$ref, 'pers'=>$c,'etabl'=>$e]);
    }
    public function edit($id){
      
        $c=DB::table('pers_morals')->latest()->first();
        $e=DB::table('etablissement_ms')->latest()->first();
        $ref = Reference_m::where('id',$id)->first();
        return view('inscription.references_m_show_edit',['ref'=>$ref, 'pers'=>$c,'etabl'=>$e]);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Reference_m::find($id)->delete();

        Session::flash('success','Suppression succés');

        return redirect()->back();

    }
    public function update(Request $request, $id){
        
        $this->validate($request,[
           
            'nouveau_nif_m' => 'unique:reference_ms',
           
        ],[
            
            'nouveau_nif_m.unique' => 'le NIF est unique, NIF existe deja',
           
           
        ]);
         $ref = Reference_m::find($id);
   
         $ref ->nouveau_nif_m= $request ->nouveau_nif_m;
 
         $ref->update();

       
         Session::flash('success','Nouveau NIF bien ajouté');
         return redirect()->back();
    }
    public function confirmation(){
        $totalp = DB::table('references')
                 ->selectRaw('count(id) as totalp')
                 ->where('nouveau_nif','=',null)
                 ->get();  
        $totalf = DB::table('reference_ms')
                 ->selectRaw('count(id) as totalf')
                 ->where('nouveau_nif_m','=',null)
                 ->get();  
      return view("inscription.confirmation",['totalp' =>$totalp,'totalf' =>$totalf]);
   }
   function carte($id){
      
    $listes = DB::table('reference_ms')
    ->join('pers_morals', 'reference_ms.pers_moral_id', '=', 'pers_morals.id')
    ->join('etablissement_ms', 'reference_ms.etablissement_m_id', '=', 'etablissement_ms.id')
    ->join('vehicule_ms', 'pers_morals.id', '=', 'vehicule_ms.pers_moral_id')
    ->select('reference_ms.id','reference_ms.date_ref_m','reference_ms.nouveau_nif_m','reference_ms.created_at','pers_morals.raison_social','pers_morals.adrs_m','pers_morals.fokontany','pers_morals.description_m','etablissement_ms.nom_etabl_m','vehicule_ms.num_veh_m','vehicule_ms.marque_m','vehicule_ms.puissance_m','vehicule_ms.nbr_place_m','vehicule_ms.charge_m')
    ->where('reference_ms.id','=',$id)
    ->get();
   return view('requete.carte_fiscal',['listes' => $listes]);
  }
}
