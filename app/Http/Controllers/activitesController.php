<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activite;
use App\http\Requests\activitesRequest;
use Session;
class activitesController extends Controller
{
  
    public function activites()
    {
      
        return view('inscription.activites_liste')->with('activites',Activite::paginate(5));
    }
    public function activites_add()
    {
      
        return view('inscription.activite_adds');
    }
    public function activite_adds(activitesRequest $request)
    {
      
        $activites = new Activite;
        $activites ->description = $request ->description;
        $activites ->precision = $request ->precision;
        $activites ->reg_comm = $request ->reg_comm;
        $activites ->date_reg_comm= $request ->date_reg_comm;
        $activites ->debut = $request ->debut;
        $activites ->cloture = $request ->cloture;
        $activites ->import = $request ->import;
        $activites ->export = $request ->export;
        $activites ->nbr_salarier = $request ->nbr_salarier;
     
 
        $activites->save();
       
        return view('inscription.activites_listes')->with('activites',Activite::all());
        
    }
}
