<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class statistiqueController extends Controller
{
    function statistique(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('Pers_phys')->paginate(2);
        $m = DB::table('Pers_morals')->paginate(2);

        return view('inscription.statistique.stat',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_p' =>$c,
            'listes_m' =>$m
            ]);
    }
}
