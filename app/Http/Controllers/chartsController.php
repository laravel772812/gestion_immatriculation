<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pers_phys;
use App\Models\Pers_moral;
use App\Admin;
use App\User;
use DB;
class ChartsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function charts()
    {
     return view('inscription.charts.charts');
	}
	
    function getAllMonths(){

		$month_array = array();
		$posts_dates = Pers_phys::orderBy( 'created_at', 'ASC' )->pluck( 'created_at' );
		$posts_dates = json_decode( $posts_dates );

		if ( ! empty( $posts_dates ) ) {
			foreach ( $posts_dates as $unformatted_date ) {
				$date = new \DateTime( $unformatted_date->date );
				$month_no = $date->format( 'm' );
				$month_name = $date->format( 'M' );
				$month_array[ $month_no ] = $month_name;
			}
		}
		return $month_array;
	}

	function getMonthlyPostCount( $month ) {
		$monthly_post_count = Pers_phys::whereMonth( 'created_at', $month )->get()->count();
		return $monthly_post_count;
	}

	function getMonthlyPostData() {

		$monthly_post_count_array = array();
		$month_array = $this->getAllMonths();
		$month_name_array = array();
		if ( ! empty( $month_array ) ) {
			foreach ( $month_array as $month_no => $month_name ){
				$monthly_post_count = $this->getMonthlyPostCount( $month_no );
				array_push( $monthly_post_count_array, $monthly_post_count );
				array_push( $month_name_array, $month_name );
			}
		}

		$max_no = max( $monthly_post_count_array );
		$max = round(( $max_no + 10/2 ) / 10 ) * 10;
		$monthly_post_data_array = array(
			'months' => $month_name_array,
			'post_count_data' => $monthly_post_count_array,
			'max' => $max,
		);

		return $monthly_post_data_array;

	}
	

	function persMorale( ) {
		$monthly_post_count = Pers_moral::get()->count();
		return $monthly_post_count;
	}
	function persPhys( ) {
		$persPhys = Pers_phys::get()->count();
		return $persPhys;
	}
	function admins( ) {
		$persPhys = Admin::get()->count();
		return $persPhys;
	}
	function users( ) {
		$user= User::get()->count();
		return $user;
	}

    function pieCharts(){
	
		$pers_m =  $this->persMorale( );
		$pers_p =  $this->persPhys( );
		$admin =  $this->admins( );
		$user =  $this->users( );
		$monthly_post_data_array = array(
		
			'sommeM' => $pers_m,
			'sommeP' => $pers_p,
			'admins' => $admin,
			'users' => $user
		
		);

		return $monthly_post_data_array;
	}

	function getAllMonths1(){

		$month_array = array();
		$posts_dates = Pers_moral::orderBy( 'created_at', 'ASC' )->pluck( 'created_at' );
		$posts_dates = json_decode( $posts_dates );

		if ( ! empty( $posts_dates ) ) {
			foreach ( $posts_dates as $unformatted_date ) {
				$date = new \DateTime( $unformatted_date->date );
				$month_no = $date->format( 'm' );
				$month_name = $date->format( 'M' );
				$month_array[ $month_no ] = $month_name;
			}
		}
		return $month_array;
	}

	function getMonthlyPostCount1( $month ) {
		$monthly_post_count = Pers_moral::whereMonth( 'created_at', $month )->get()->count();
		return $monthly_post_count;
	}


	

	function bar(){
		$monthly_post_count_array = array();
		$month_array = $this->getAllMonths1();
		$month_name_array = array();
		if ( ! empty( $month_array ) ) {
			foreach ( $month_array as $month_no => $month_name ){
				$monthly_post_count = $this->getMonthlyPostCount1( $month_no );
				array_push( $monthly_post_count_array, $monthly_post_count );
				array_push( $month_name_array, $month_name );
			}
		}

		$max_no = max( $monthly_post_count_array );
		$max = round(( $max_no + 10/2 ) / 10 ) * 10;
		$monthly_post_data_array = array(
			'months' => $month_name_array,
			'post_count_data' => $monthly_post_count_array,
			'max' => $max,
		);

		return $monthly_post_data_array;
	}
}
