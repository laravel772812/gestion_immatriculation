<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interlocuteurs;
use App\Models\Vehicule;
use Session;
use DB;
class interlocuteursController extends Controller
{
  
    public function interlocuteurs_add()
    {
        $e = DB::table('pers_phys')->latest() ->first();
        return view('inscription.interlocuteurs_add',['pers' => $e]);
    }
    public function interlocuteurs_adds(Request $request)
    {
        $this->validate($request,[
            'nom_inter' => 'required',
            'adrs_inter' => 'required',
            'tel_inter' => 'required',
            'email_inter' => 'email',
        ],[
            'nom_inter.required' => 'Nom interlocuteur est obligatoire',
            'adrs_inter.required' => 'Adresse interlocuteur est obligatoire',
            'tel_inter.required' => 'Telephone interlocuteur est obligatoire',
           
            'email_inter.email' => 'Email  invalide',
        ]);
         $inter = new Interlocuteurs;
         $inter ->nom_inter = $request ->nom_inter;
         $inter ->titre = $request ->titre;
         $inter ->adrs_inter = $request ->adrs_inter;
         $inter ->tel_inter = $request ->tel_inter;
         $inter ->email_inter = $request ->email_inter;
         $inter ->pers_phys_id = $request ->pers_phys_id;
        
         $inter->save();
         $c=DB::table('pers_phys')->latest()->first();
         $e=DB::table('etablissements')->latest()->first();
         return view('inscription.references')->with('pers',$c)->with('etabl',$e);
        
    }
    public function interlocuteurs_test()
    {
      
        return view('inscription.test');
    }
}
