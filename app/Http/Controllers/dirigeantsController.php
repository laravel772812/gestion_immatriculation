<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Dirigeant;
use App\Models\Etablissement;
use Session;
use DB;
class dirigeantsController extends Controller
{
  
    public function dirigeants()
    {
      
        return view('inscription.dirigeants_liste')->with('dirigeants',Dirigeant::all());
    }
    public function dirigeants_add()
    {
        $c = DB::table('etablissements')->latest()->first();
        return view('inscription.dirigeants_add')->with('etablissements',$c);
    }
    public function dirigeants_adds(Request $request)
    {
        $this->validate($request,[
            'nom_dir' => 'required',
            'fonction' => 'required',
            'cin_dir' => 'required',
            'adrs_dir' => 'required',
            'email_dir' => 'required|email',
            'tel_dir' => 'required',
          
            
        ],[
            'nom_dir.required' => 'Champ nom est obligatoire',
            'fonction.required' => 'Champ fonction est obligatoire',
            'cin_dir.required' => 'Champ CIN est obligatoire',
            'adrs_dir.required' => 'Champ adresse est obligatoire',
            'email.required' => 'Champ email est obligatoire',
            'email.email' => 'Email invalide',
            'tel_dir.required' => 'Champ telephone est obligatoire',
        ]);
         $dirigeant = new Dirigeant;
         $dirigeant ->nom_dir = $request ->nom_dir;
         $dirigeant ->fonction = $request ->fonction;
         $dirigeant ->cin_dir = $request ->cin_dir;
         $dirigeant ->adrs_dir = $request ->adrs_dir;
         $dirigeant ->email_dir = $request ->email_dir;
         $dirigeant ->tel_dir = $request ->tel_dir;
         $dirigeant ->activite_dir = $request ->activite_dir;
         $dirigeant ->nif_activite_dir = $request ->nif_activite_dir;
         $dirigeant ->etablissement_id = $request ->etablissement_id;
         $dirigeant->save();
         $e = DB::table('dirigeants')->latest() ->first();
         Session::flash('successs','dirigeants enregistrer');
        return view('inscription.dirigeants_listes')->with('dirigeant',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Dirigeant::find($id)->delete();

        Session::flash('success','Dirigeant supprimé');

        return view('inscription.dirigeants_listes')->with('dirigeant',DB::table('dirigeants')->latest() ->first());
    

    }
}
