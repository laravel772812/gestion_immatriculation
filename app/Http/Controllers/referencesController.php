<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reference;
use App\Models\Etablissement;
use App\Models\Pers_phys;
use DB;
use Session;
class referencesController extends Controller
{
 

    public function reference_add()
    {
      
        $c=DB::table('pers_phys')->latest()->first();
        $e=DB::table('etablissements')->latest()->first();
        return view('inscription.references')->with('pers',$c)->with('etabl',$e);
    }
    public function reference_adds(Request $request)
    {
        $this->validate($request,[
            'date_ref' => 'required',
            'pers_phys_id' => 'required',
            'etablissement_id' => 'required',
            'confirmation' => 'required',
        ],[
            'date_ref.required' => 'Champ obligatoire',
            'pers_phys_id.required' => 'Champ obligatoire',
            'etablissement_id.required' => 'Champ obligatoire',
           
            'confirmation.email' => 'Champ obligatoire',
        ]);
         $ref = new Reference;
         $ref ->date_ref = $request ->date_ref;
         $ref ->confirmation= $request ->confirmation;
         $ref ->nouveau_nif= $request ->nouveau_nif;
         $ref ->pers_phys_id = $request ->pers_phys_id;
         $ref ->etablissement_id = $request ->etablissement_id;
         $ref->save();
         $c=DB::table('references')->latest()->first();
        
         $listes=DB::table('references')->latest()->first();
         
         Session::flash('success','ok');
         return view('inscription.references_show',['listes' => $listes,'ref' =>$c]);
        
    }
    function reference_show(){
        $search = \Request::get('title');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->where('nom_pers_phys','like','%' .$search. '%' )->orwhere('prenom_pers_phys','like','%' .$search. '%' )->orwhere('sit_mat','like','%' .$search. '%' )->orwhere('sexe','like','%' .$search. '%' )->orwhere('adrs','like','%' .$search. '%' )->orderBy('id')->get();
        $listes = Pers_phys::where('nom_pers_phys','like','%' .$search. '%' )->orwhere('prenom_pers_phys','like','%' .$search. '%' )->orwhere('sit_mat','like','%' .$search. '%' )->orwhere('sexe','like','%' .$search. '%' )->orwhere('adrs','like','%' .$search. '%' )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_search',['listes' => $listes,'total' => $total ]);
    }
    function reference_print(){
       
        $search = \Request::get('a');
        $listes = DB::table('references')
        ->join('pers_phys', 'references.pers_phys_id', '=', 'pers_phys.id')
        ->join('etablissements', 'references.etablissement_id', '=', 'etablissements.id')
        ->select('references.*', 'pers_phys.*', 'etablissements.*')
        ->where('references.id','=',$search)
        ->get();
       return view('inscription.print',['listes' => $listes]);
    }
    function reference_liste(){
      
        $listes = DB::table('references')
        ->join('pers_phys', 'references.pers_phys_id', '=', 'pers_phys.id')
        ->join('etablissements', 'references.etablissement_id', '=', 'etablissements.id')
        ->select('references.id','references.pers_phys_id','references.nouveau_nif','pers_phys.nom_pers_phys','etablissements.nom_etabl')
        ->get();
       return view('requete.references_liste',['listes_ref' => $listes]);
    }
    function search(){
      $search=\Request::get('title');
        $listes = DB::table('references')
        ->join('pers_phys', 'references.pers_phys_id', '=', 'pers_phys.id')
        ->join('etablissements', 'references.etablissement_id', '=', 'etablissements.id')
        ->select('references.id','references.pers_phys_id','references.nouveau_nif','pers_phys.nom_pers_phys','etablissements.nom_etabl')
        ->where('references.id', '=', $search)
        ->orWhere('pers_phys.nom_pers_phys', 'like','%' .$search. '%')
        ->orWhere('etablissements.nom_etabl', 'like','%' .$search. '%')
        ->get();
        return view('requete.references_liste',['listes_ref' => $listes]);
    }
    public function show($id){
     
        $c=DB::table('pers_phys')->latest()->first();
        $e=DB::table('etablissements')->latest()->first();
        $ref = Reference::where('id',$id)->first();
        return view('inscription.references_show_edit',['ref'=>$ref, 'pers'=>$c,'etabl'=>$e]);
    }
    public function edit($id){
      
        $c=DB::table('pers_phys')->latest()->first();
        $e=DB::table('etablissements')->latest()->first();
        $ref = Reference::where('id',$id)->first();
        return view('inscription.references_show_edit',['ref'=>$ref, 'pers'=>$c,'etabl'=>$e]);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Reference::find($id)->delete();

        Session::flash('success','Suppression succés');

        return redirect()->back();

    }
    public function update(Request $request, $id){
      
        $this->validate($request,[
           
            'nouveau_nif' => 'unique:references',
           
        ],[
            
            'nouveau_nif.unique' => 'le NIF est unique, NIF existe deja',
           
           
        ]);
         $ref = Reference::find($id);
   
         $ref ->nouveau_nif= $request ->nouveau_nif;
 
         $ref->update();

       
         Session::flash('success','Nouveau NIF bien ajouté');
         return redirect()->back();
    }
    function carte_p($id){
      
        $listes = DB::table('references')
        ->join('pers_phys', 'references.pers_phys_id', '=', 'pers_phys.id')
        ->join('etablissements', 'references.etablissement_id', '=', 'etablissements.id')
        ->join('vehicules', 'pers_phys.id', '=', 'vehicules.pers_phys_id')
        ->select('references.id','references.date_ref','references.nouveau_nif','references.created_at','pers_phys.nom_pers_phys','pers_phys.prenom_pers_phys','pers_phys.adrs','pers_phys.description','pers_phys.num_stat','pers_phys.cin','etablissements.nom_etabl','vehicules.num_veh','vehicules.marque','vehicules.puissance','vehicules.nbr_place','vehicules.charge')
        ->where('references.id','=',$id)
        ->get();
       return view('requete.carte_fiscal_phys',['listes' => $listes]);
      }
}
