<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dirigeant_m;
use App\Models\Etablissement_m;
use Session;
use DB;
class dirigeant_mController extends Controller
{
    public function dirigeants()
    {
      
        return view('inscription.dirigeants_m_liste')->with('dirigeants',Dirigeant_m::all());
    }
    public function dirigeants_add()
    {
        $c = DB::table('etablissement_ms')->latest()->first();
        return view('inscription.dirigeant_m_add')->with('etablissements',$c);
    }
    public function dirigeants_adds(Request $request)
    {
        $this->validate($request,[
            'nom_dir_m' => 'required',
            'fonction_m' => 'required',
            'cin_dir_m' => 'required',
            'adrs_dir_m' => 'required',
            'email_dir_m' => 'required|email',
            'tel_m' => 'required',
            'autre_act' => 'nullable',
          
            
        ],[
            'nom_dir_m.required' => 'Champ nom est obligatoire',
            'fonction_m.required' => 'Champ fonction_m est obligatoire',
            'cin_dir_m.required' => 'Champ CIN est obligatoire',
            'adrs_dir_m.required' => 'Champ adresse est obligatoire',
            'email.required' => 'Champ email est obligatoire',
            'email.email' => 'Email invalide',
            'tel_m.required' => 'Champ telephone est obligatoire',
        ]);
         $dirigeant = new Dirigeant_m;
         $dirigeant ->nom_dir_m = $request ->nom_dir_m;
         $dirigeant ->fonction_m = $request ->fonction_m;
         $dirigeant ->cin_dir_m = $request ->cin_dir_m;
         $dirigeant ->adrs_dir_m = $request ->adrs_dir_m;
         $dirigeant ->email_dir_m = $request ->email_dir_m;
         $dirigeant ->tel_m = $request ->tel_m;
         $dirigeant ->autre_act = $request ->autre_act;
         $dirigeant ->activite_dir_m = $request ->activite_dir_m;
         $dirigeant ->nif_activite_dir_m = $request ->nif_activite_dir_m;
         $dirigeant ->etablissement_m_id = $request ->etablissement_m_id;
         $dirigeant->save();
         $e = DB::table('dirigeant_ms')->latest() ->first();
         Session::flash('successs','dirigeants enregistrer');
        return view('inscription.dirigeants_m_listes')->with('dirigeant',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Dirigeant::find($id)->delete();

        Session::flash('success','Dirigeant supprimé');

        return view('inscription.dirigeants_m_listes')->with('dirigeants',Dirigeant::all());
    

    }
}
