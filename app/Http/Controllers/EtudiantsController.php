<?php

namespace App\Http\Controllers;
use App\Models\Etudiants;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Requests\EtudiantsRequest;
use Session;
class EtudiantsController extends Controller
{
   public function index(){
       return view('etudiants')->with('etudiants',Etudiants::all());
   }
   public function add(){
       //recuperation la liste des classes
       $c = Classes::orderBy('id','asc')->get();

       return view('etudiants_add')->with('classes',$c);
    }

    public function store(EtudiantsRequest $request){
       
       $etudiants = new Etudiants;
       $etudiants ->firstname = $request ->firstname;
       $etudiants ->lastname = $request ->lastname;
       $etudiants ->email = $request ->email;
       $etudiants ->phone = $request ->phone;
       $etudiants ->birthday = $request ->birthday;
       $etudiants ->classes_id = $request ->classes_id;

       $etudiants->save();
       return view('etudiants');
    }

    public function delete($id){
        //recherche par rapport a id supprimer
        Etudiants::find($id)->delete();

        Session::flash('success','Etudiant supprimé');

        return redirect()->back();

    }
    public function show($id){
        //recherche par rapport a id supprimer
        $etudiant = Etudiants::where('id',$id)->first();
        return view('etudiant_show',['etudiant'=>$etudiant, 'classes'=>classes::all()]);



    }
}
