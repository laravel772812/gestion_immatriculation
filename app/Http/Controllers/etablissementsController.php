<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Etablissement;
use App\Models\Pers_phys;
use App\Http\Requests\etablissementsRequest;
use Session;
use DB;
class etablissementsController extends Controller
{
    public function etablissements()
    {
      
        return view('inscription.etablissements')->with('etablissements',Etablissement::all());
    }
    public function etablissements_add()
    {
       
      $c = DB::table('pers_phys')->latest() ->first();
        return view('inscription.etablissements_add')->with('pers',$c)->with('etablissements',Etablissement::get());
    }
    public function etablissements_adds(etablissementsRequest $request)
    {
      
        $etablissement = new Etablissement;
        $etablissement ->nom_etabl = $request ->nom_etabl;
       
        $etablissement ->date_ouvert = $request ->date_ouvert;
        $etablissement ->tel = $request ->tel;
        $etablissement ->autre_tel = $request ->autre_tel;
        $etablissement ->fax = $request ->fax;
        $etablissement ->email = $request ->email;
        $etablissement ->exportateur = $request ->exportateur;
        $etablissement ->importateur = $request ->importateur;
        $etablissement ->type_prop = $request ->type_prop;
        $etablissement ->nif_prop = $request ->nif_prop;
        $etablissement ->nom_prop = $request ->nom_prop;
        $etablissement ->cin_prop = $request ->cin_prop;
        $etablissement ->adrs_prop = $request ->adrs_prop;
        $etablissement ->tel_prop = $request ->tel_prop;
        $etablissement ->pers_phys_id = $request ->pers_phys_id;
        $etablissement->save();
        $e = DB::table('etablissements')->latest() ->first();
        Session::flash('successs','Etablissement enregistrer');
        return view('inscription.etablissementss')->with('etablissement',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Etablissement::find($id)->delete();

        Session::flash('success','Etablissement supprimé');
        $e = DB::table('etablissements')->latest() ->first();
        return view('inscription.etablissementss')->with('etablissement',$e);
    

    }
}
