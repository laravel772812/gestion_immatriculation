<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pers_moral;
use App\Models\Associer;
use Session;
use DB;
class pers_moralController extends Controller
{
   
    public function persMoral()
    {
       
        return view('inscription.persMoral');
    }
    public function  persMoral_add(Request $request){
        $this->validate($request,[
            'raison_social' => 'required',
            'form_jurid' => 'required',
            'adrs_m' => 'required',
            'rib_m' => 'nullable',
            'description_m' =>'required',
            'debut_m' =>'required',
            'cloture_m' =>'required',
            'import_m' =>'required',
            'export_m' =>'required',
            'nbr_salarier_m' =>'nullable|numeric',
        ],[
            'raison_social.required' =>'nom est obligatoire',
            'form_jurid.required' =>'champ est obligatoire',
            'nom_pers_phys.alpha' =>'nom doit être en lettre',
            'sexe.required' =>'sexe est obligatoire',
            'cin.required' =>'cin est obligatoire',
            
            'date_del_cin.required' =>'date de delivrance cin est obligatoire',
            'lieu_del_cin.required' =>'lieu de delivrance cin est obligatoire',
            'num_stat.required' =>'numero statistique est obligatoire',
           
            'date_naiss.required' =>'date de naissance est obligatoire',
            'lieu_naiss.required' =>'lieu de naissance est obligatoire',
            'adrs_m.required' =>'Adresse est obligatoire',
            'description_m.required' =>'champ obligatoire',
            'debut_m.required' =>'champ obligatoire',
           'cloture_m.required' =>'champ obligatoire',
           'import_m.required' =>'champ obligatoire',
           'export_m.required' =>'champ obligatoire',
           'nbr_salarier_m.numeric' =>'Nombre salarier doit être en chiffre',
        ]);


        $persMoral = new Pers_moral;

        $persMoral ->raison_social = $request ->raison_social;
        $persMoral ->form_jurid = $request ->form_jurid;
     

        $persMoral ->adrs_m= $request ->adrs_m;
        $persMoral ->fokontany = $request ->fokontany;
      

        $persMoral ->description_m = $request ->description_m;
        $persMoral ->precision_m = $request ->precision_m;
        $persMoral ->reg_comm_m = $request ->reg_comm_m;
        $persMoral ->date_reg_comm_m= $request ->date_reg_comm_m;
        $persMoral ->debut_m = $request ->debut_m;
        $persMoral ->cloture_m = $request ->cloture_m;
        $persMoral ->import_m = $request ->import_m;
        $persMoral ->export_m = $request ->export_m;
        $persMoral ->nbr_salarier_m = $request ->nbr_salarier_m;

       
        $persMoral ->capital_m = $request ->capital_m;
        $persMoral ->rib_m = $request ->rib_m;
        $persMoral ->reg_fisc_m = $request ->reg_fisc_m;
       
        $persMoral ->peri_grace_m = $request ->peri_grace_m;
 
        $persMoral->save();
        Session::flash('success','renseignement enregistrer');
        return view('inscription.associes_liste')->with('associer',Associer::all());
    }
}
