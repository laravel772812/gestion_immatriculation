<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Dirigeant;
use App\Models\Etablissement;
use App\Models\Vehicule_m;
use App\Models\Pers_morals;
use Session;
use DB;
class vehicule_mController extends Controller
{
    public function vehicules()
    {
      
        return view('inscription.vehicules_m_liste')->with('vehicules',Vehicule_m::all());
    }
    public function vehicules_add()
    {
        $c = DB::table('pers_morals')->latest()->first();
        return view('inscription.vehicules_m_add')->with('pers',$c);
    }
    public function vehicules_adds(Request $request)
    {
        $this->validate($request,[
            'num_veh_m' => 'required|numeric',
            'nbr_place_m' => 'numeric',
            'charge_m' => 'numeric',
            'poid_m' => 'numeric',
        ],[
            'num_veh_m.required' => 'Le numero du vehicule est obligatoire',
            'nbr_place_m.numeric' => 'Ce champ doit être en chiffre',
            'charge_m.numeric' =>'Ce champ doit être en chiffre',
            'poid_m.numeric' => 'Ce champ doit être en chiffre',
           
        ]);
         $vehicule = new Vehicule_m;
         $vehicule ->num_veh_m = $request ->num_veh_m;
         $vehicule ->marque_m = $request ->marque_m;
         $vehicule ->genre_m = $request ->genre_m;
         $vehicule ->type_m = $request ->type_m;
         $vehicule ->puissance_m = $request ->puissance_m;
         $vehicule ->nbr_place_m = $request ->nbr_place_m;
         $vehicule ->charge_m = $request ->charge_m;
         $vehicule ->poid_m = $request ->poid_m;
         $vehicule ->date_circ_m = $request ->date_circ_m;
         $vehicule ->usage_proff_m = $request ->usage_proff_m;
         $vehicule ->date_debut = $request ->date_debut;
         $vehicule ->exploitation_m = $request ->exploitation_m;
         $vehicule ->pers_moral_id = $request ->pers_moral_id;

         
         $vehicule->save();
         $e = DB::table('vehicule_ms')->latest() ->first();
         Session::flash('successs','vehicules enregistrer');
        return view('inscription.vehicules_m_listes')->with('vehicule',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Vehicule::find($id)->delete();

        Session::flash('success','Vehicule supprimé');
        $e = DB::table('vehicule_ms')->latest() ->first();
        return view('inscription.vehicules_m_listes')->with('vehicule',$e);
    

    }
}
