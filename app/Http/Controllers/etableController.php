<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pers_moral;
use App\Models\Associer;
use App\Models\Etablissement_m;
use Session;
use DB;
class etableController extends Controller
{
    public function etablissements()
    {
      
        return view('inscription.etablissements_m')->with('etablissements',Etablissement_m::all());
    }
    public function etablissements_add()
    {
       
      $c = DB::table('pers_morals')->latest() ->first();
        return view('inscription.etablissements_m_add')->with('pers',$c)->with('etablissements',Etablissement_m::get());
    }
    public function etablissements_adds(Request $request)
    {
        $this->validate($request,[
             
            'date_ouvert_m' => 'required',
            'tel_m' => 'required',
            'email_m'=>'nullable|email',
            'pers_moral_id'=>'nullable',
            'nom_etabl_m'=>'required',
        ],[
             
            'date_ouvert_m.required' => 'Date d ouverture est obligatoire',
            'tel_m.required' => 'Telephone est obligatoire',
            'email_m.email'=>'email invalide',
            'nom_etabl_m.required'=>'Nom commercial est obligatoire',
        ]);
        $etablissement = new Etablissement_m;
        $etablissement ->nom_etabl_m = $request ->nom_etabl_m;
       
        $etablissement ->date_ouvert_m = $request ->date_ouvert_m;
        $etablissement ->tel_m = $request ->tel_m;
        $etablissement ->autre_tel_m = $request ->autre_tel_m;
        $etablissement ->fax_m = $request ->fax_m;
        $etablissement ->email_m = $request ->email_m;
        $etablissement ->exportateur_m = $request ->exportateur_m;
        $etablissement ->importateur_m = $request ->importateur_m;
        $etablissement ->type_prop_m = $request ->type_prop_m;
        $etablissement ->nif_prop_m = $request ->nif_prop_m;
        $etablissement ->nom_prop_m = $request ->nom_prop_m;
        $etablissement ->cin_prop_m = $request ->cin_prop_m;
        $etablissement ->adrs_prop_m = $request ->adrs_prop_m;
        $etablissement ->tel_prop_m = $request ->tel_prop_m;
        $etablissement ->pers_moral_id = $request ->pers_moral_id;
        $etablissement->save();
        $e = DB::table('etablissement_ms')->latest() ->first();
        Session::flash('successs','Etablissement enregistrer');
        return view('inscription.etablissementss_m')->with('etablissement',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Etablissement_m::find($id)->delete();

        Session::flash('success','Etablissement supprimé');
        $e = DB::table('etablissement_ms')->latest() ->first();
        return view('inscription.etablissementss_m')->with('etablissement',$e);
    

    }
}
