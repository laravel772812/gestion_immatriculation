<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\user;
use DB;
class utilisateurController extends Controller
{
    function utilisateur_liste(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
      
        $listes = DB::table('users')->paginate(5);
        
       return view('utilisateur.liste',[
           'listes' =>$listes,
           'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
           ]);
    }
    public function show($id){
     
       
       
        $ref = DB::table('users')->where('id','=',$id)->first();
        return view('utilisateur.show',['ref'=>$ref]);
    }
    public function update(Request $request, $id){
      
  
        $ref = User::find($id);
     
        $ref ->name = $request ->name;
        $ref ->email= $request ->email;
        $ref ->password= $request ->password;
        $ref->update();

      
        Session::flash('success','modification reussie');
        return redirect()->back();
   }
   public function delete($id){
    //recherche par rapport a id supprimer
    User::find($id)->delete();

    Session::flash('success','Suppression succés');

    return redirect()->back();

}
}
