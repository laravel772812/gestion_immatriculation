<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Etablissement;
use App\Models\Etablissement_m;
use App\Models\Pers_phys;
use App\Models\Pers_moral;
use App\Models\Dirigeant;
use App\Models\Dirigeant_m;
use App\Models\Vehicule;
use App\Models\Vehicule_m;
use App\Models\Interlocuteur;
use App\Models\Interlocuteur_m;
use App\Models\Associer;
use Session;
use DB;

class inscriptionController extends Controller
{
 
   
   

    public function index()
    {
       
        return view('inscription.inscription');
    }
    //personne physique
    function inscrit(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('Pers_phys')->paginate(2);
        $m = DB::table('Pers_morals')->paginate(2);

        return view('requete.inscrit',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_p' =>$c,
            'listes_m' =>$m
            ]);
    }
    function etablissements(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('etablissements')
                 ->join('pers_phys','etablissements.pers_phys_id','pers_phys.id')->select('pers_phys.*','etablissements.*')
                 ->paginate(5);
      

        return view('requete.etablissements',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_e' =>$c
           
            ]);
    }
    public function searchEtabl(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('etablissements')
        ->join('pers_phys','etablissements.pers_phys_id','pers_phys.id')
        ->select('pers_phys.*','etablissements.*')
        ->where('etablissements.id','like','%'.$search.'%')
        ->orWhere('etablissements.nom_etabl','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.etablissements_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_e' =>$c
           
            ]);
    }
    public function deleteEtabl($id){
        //recherche par rapport a id supprimer
        Etablissement::find($id)->delete();

        Session::flash('success','Etablissement supprimé');

        return redirect()->back();

    }

    function dirigeants(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('dirigeants')
                 ->join('etablissements','dirigeants.etablissement_id','etablissements.id')->select('dirigeants.*','etablissements.nom_etabl')
                 ->paginate(5);
      

        return view('requete.dirigeants',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_d' =>$c
           
            ]);
    }
    public function searchDir(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('dirigeants')
        ->join('etablissements','dirigeants.etablissement_id','etablissements.id')
        ->select('dirigeants.*','etablissements.nom_etabl')
        ->where('dirigeants.id','like','%'.$search.'%')
        ->orWhere('dirigeants.nom_dir','like','%'.$search.'%')
        ->orWhere('dirigeants.adrs_dir','like','%'.$search.'%')
        ->orWhere('dirigeants.activite_dir','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.dirigeants_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_d' =>$c
            ]);
    }
    public function deleteDir($id){
        //recherche par rapport a id supprimer
        Dirigeant::find($id)->delete();

        Session::flash('success','Dirigeant supprimé');

        return redirect()->back();

    }

    
    function vehicules(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('vehicules')
                 ->join('pers_phys','vehicules.pers_phys_id','pers_phys.id')->select('vehicules.*','pers_phys.nom_pers_phys')
                 ->paginate(5);
      

        return view('requete.vehicules',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_v' =>$c
           
            ]);
    }
    function searchVeh(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('vehicules')
                 ->join('pers_phys','vehicules.pers_phys_id','pers_phys.id')
                 ->select('vehicules.*','pers_phys.nom_pers_phys')
                 ->where('vehicules.id','like','%'.$search.'%')
                 ->orWhere('vehicules.num_veh','like','%'.$search.'%')
                 ->orWhere('vehicules.marque','like','%'.$search.'%')
                 ->paginate(5);
      

        return view('requete.vehicules_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_v' =>$c
           
            ]);
    }
    public function deleteVeh($id){
        //recherche par rapport a id supprimer
        Vehicule::find($id)->delete();

        Session::flash('success','Vehicule supprimé');

        return redirect()->back();

    }
        
    function interlocuteurs(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('interlocuteurs')
                 ->join('pers_phys','interlocuteurs.pers_phys_id','pers_phys.id')->select('interlocuteurs.*','pers_phys.nom_pers_phys')
                 ->paginate(5);
      

        return view('requete.interlocuteurs',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_i' =>$c
           
            ]);
    }
    function searchInter(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search=\Request::get('title');
        $c = DB::table('interlocuteurs')
                 ->join('pers_phys','interlocuteurs.pers_phys_id','pers_phys.id')
                 ->select('interlocuteurs.*','pers_phys.nom_pers_phys')
                 ->where('interlocuteurs.id','like','%'.$search.'%')
                 ->orWhere('interlocuteurs.nom_inter','like','%'.$search.'%')
                 ->where('interlocuteurs.adrs_inter','like','%'.$search.'%')
                 ->paginate(5);
      

        return view('requete.interlocuteurs_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_i' =>$c
           
            ]);
    }
    public function deleteInter($id){
        //recherche par rapport a id supprimer
        Interlocuteur::find($id)->delete();

        Session::flash('success','interlocuteur supprimé');

        return redirect()->back();

    }

    //personne morals
    function pers_morals(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('Pers_morals')->paginate(5);
        return view('requete.pers_morals',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function searchMorals(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('Pers_morals')
                 ->where('id','like','%'.$search.'%')
                 ->orWhere('raison_social','like','%'.$search.'%')
                 ->orWhere('description_m','like','%'.$search.'%')
                 ->paginate(5);
        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    public function deleteMorals($id){
        //recherche par rapport a id supprimer
        Pers_moral::find($id)->delete();

        Session::flash('success','Personne moral supprimé');

        return redirect()->back();

    }
    function between(){
        $debut = \Request::get('debut');
        $fin = \Request::get('fin');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = Pers_moral::whereBetween('created_at',[$debut,$fin] )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function date_choisie(){
        $dateC = \Request::get('date_choisie');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = Pers_moral::whereDate('created_at',$dateC )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function month_choisie(){
        $dateC = \Request::get('month_choisie');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
     
        $c = Pers_moral::whereMonth('created_at',$dateC )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function day_choisie(){
        $day = \Request::get('day_choisie');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
      
        $c = Pers_moral::whereDay('created_at',$day )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function annee_choisie(){
        $annee = \Request::get('annee_choisie');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = Pers_moral::whereYear('created_at',$annee )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function fokontany(){
        $foko = \Request::get('fokontany');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = Pers_moral::where('adrs_m','=',$foko )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function fokontany_m(){
        $foko = \Request::get('fokontany_m');
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = Pers_moral::where('Fokontany','=',$foko )->orderBy('id')->paginate(5);

        return view('requete.pers_morals_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes' =>$c,
            
            ]);
    }
    function etablissements_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('etablissement_ms')
                 ->join('pers_morals','etablissement_ms.pers_moral_id','pers_morals.id')
                 ->select('pers_morals.raison_social','etablissement_ms.*')
                 ->paginate(5);
      

        return view('requete.etablissements_m',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_e' =>$c
           
            ]);
    }
    public function searchEtabl_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('etablissement_ms')
        ->join('pers_morals','etablissement_ms.pers_moral_id','pers_morals.id')
        ->select('pers_morals.raison_social','etablissement_ms.*')
        ->where('etablissement_ms.id','like','%'.$search.'%')
        ->orWhere('etablissement_ms.nom_etabl_m','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.etablissements_m_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_e' =>$c
           
            ]);
    }
    public function deleteEtabl_m($id){
        //recherche par rapport a id supprimer
        Etablissement_m::find($id)->delete();

        Session::flash('success','Etablissement supprimé');

        return redirect()->back();

    }
    function dirigeants_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('dirigeant_ms')
                 ->join('etablissement_ms','dirigeant_ms.etablissement_m_id','etablissement_ms.id')
                 ->select('dirigeant_ms.*','etablissement_ms.nom_etabl')
                 ->paginate(5);
      

        return view('requete.dirigeants_m',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_d' =>$c
           
            ]);
    }
    public function searchDir_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('dirigeant_ms')
        ->join('etablissement_ms','dirigeant_ms.etablissement_m_id','etablissement_ms.id')
        ->select('dirigeant_ms.*','etablissement_ms.nom_etabl')
        ->where('dirigeant_ms.id','like','%'.$search.'%')
        ->orWhere('dirigeant_ms.nom_dir_m','like','%'.$search.'%')
        ->orWhere('dirigeant_ms.adrs_dir_m','like','%'.$search.'%')
        ->orWhere('dirigeant_ms.activite_dir_m','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.dirigeants_m_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_d' =>$c
            ]);
    }
    public function deleteDir_m($id){
        //recherche par rapport a id supprimer
        Dirigeant_m::find($id)->delete();

        Session::flash('success','Dirigeant supprimé');

        return redirect()->back();

    }
    function vehicules_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('vehicule_ms')
                 ->join('pers_morals','vehicule_ms.pers_moral_id','pers_morals.id')
                 ->select('vehicule_ms.*','pers_morals.raison_social')
                 ->paginate(5);
      

        return view('requete.vehicules_m',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_v' =>$c
           
            ]);
    }
    public function searchVeh_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('vehicule_ms')
        ->join('pers_morals','vehicule_ms.pers_moral_id','pers_morals.id')
        ->select('vehicule_ms.*','pers_morals.raison_social')
        ->where('vehicule_ms.id','=',$search)
        ->orWhere('vehicule_ms.marque_m','like','%'.$search.'%')
        ->orWhere('vehicule_ms.num_veh_m','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.vehicules_m_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_v' =>$c
            ]);
    }
    public function deleteVeh_m($id){
        //recherche par rapport a id supprimer
        Vehicule_m::find($id)->delete();

        Session::flash('success','Vehicule supprimé');

        return redirect()->back();

    }
    function interlocuteurs_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $c = DB::table('interlocuteur_ms')
                 ->join('pers_morals','interlocuteur_ms.pers_moral_id','pers_morals.id')
                 ->select('interlocuteur_ms.*','pers_morals.raison_social')
                 ->paginate(5);
      

        return view('requete.interlocuteurs_m',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_i' =>$c
           
            ]);
    }
    public function searchInter_m(){
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $search = \Request::get('title');
        $c = DB::table('interlocuteur_ms')
        ->join('pers_morals','interlocuteur_ms.pers_moral_id','pers_morals.id')
        ->select('interlocuteur_ms.*','pers_morals.raison_social')
        ->where('interlocuteur_ms.id','=',$search)
        ->orWhere('interlocuteur_ms.nom_inter_m','like','%'.$search.'%')
        ->orWhere('interlocuteur_ms.adrs_inter_m','like','%'.$search.'%')
        ->paginate(5);
        return view('requete.interlocuteurs_m_search',[
            'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            'listes_i' =>$c
            ]);
    }
    public function deleteInter_m($id){
        //recherche par rapport a id supprimer
        Interlocuteur_m::find($id)->delete();

        Session::flash('success','Interlocuteur supprimé');

        return redirect()->back();

    }
}
