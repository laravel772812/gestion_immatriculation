<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pers_phys;
use App\Models\Etablissement;
use App\http\Requests\PersPhysRequest;

use App\Models\persPhysblissement;
use DB;

use Session;

class pers_physController extends Controller
{
   
    public function pers(Request $request)
    {
      
      
        return view('inscription.persPhys');
    }
    public function wizard(Request $request)
    {
      
      
        return view('inscription.wizard');
    }
    public function persPhys(PersPhysRequest $request)
    {
   
        $persPhys = new Pers_Phys;
        $persPhys ->nom_pers_phys = $request ->nom_pers_phys;
        $persPhys ->prenom_pers_phys = $request ->prenom_pers_phys;
        $persPhys ->sit_mat = $request ->sit_mat;
        $persPhys ->sexe = $request ->sexe;
        $persPhys ->cin = $request ->cin;
        $persPhys ->date_del_cin = $request ->date_del_cin;
        $persPhys ->lieu_del_cin = $request ->lieu_del_cin;
        $persPhys ->num_stat = $request ->num_stat;
        $persPhys ->date_deliv_stat = $request ->date_deliv_stat;
        $persPhys ->date_naiss = $request ->date_naiss;
        $persPhys ->lieu_naiss = $request ->lieu_naiss;

        $persPhys ->adrs= $request ->adrs;
      

        $persPhys ->description = $request ->description;
        $persPhys ->precision = $request ->precision;
        $persPhys ->reg_comm = $request ->reg_comm;
        $persPhys ->date_reg_comm= $request ->date_reg_comm;
        $persPhys ->debut = $request ->debut;
        $persPhys ->cloture = $request ->cloture;
        $persPhys ->import = $request ->import;
        $persPhys ->export = $request ->export;
        $persPhys ->nbr_salarier = $request ->nbr_salarier;

       
        $persPhys ->capital = $request ->capital;
        $persPhys ->rib = $request ->rib;
        $persPhys ->reg_fisc = $request ->reg_fisc;
      
 
        $persPhys->save();
        Session::flash('success','renseignement enregistrer');
        return view('inscription.etablissements')->with('etablissements',Etablissement::all());
        
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Pers_phys::find($id)->delete();

        Session::flash('success','Personne supprimé');

        return redirect()->back();

    }
    function liste_inscrit(){
        $c = DB::table('Pers_phys')->paginate(5);
        $pers_phys = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
        $pers_morals = DB::table('Pers_morals')->selectRaw('count(id) as total')->get();
        $admins = DB::table('admins')->selectRaw('count(id) as total')->get();
        $users = DB::table('users')->selectRaw('count(id) as total')->get();
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->get();
       return view('requete.liste_inscrit',[
           'listes' => $c,'total' => $total,
           'pers_phys' => $pers_phys,
            'pers_morals' =>$pers_morals,
            'admins' =>$admins,
            'users' =>$users,
            ]);
    }
   
    function search(){
        $search = \Request::get('title');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->where('nom_pers_phys','like','%' .$search. '%' )->orwhere('prenom_pers_phys','like','%' .$search. '%' )->orwhere('sit_mat','like','%' .$search. '%' )->orwhere('sexe','like','%' .$search. '%' )->orwhere('adrs','like','%' .$search. '%' )->orderBy('id')->get();
        $listes = Pers_phys::where('nom_pers_phys','like','%' .$search. '%' )->orwhere('prenom_pers_phys','like','%' .$search. '%' )->orwhere('sit_mat','like','%' .$search. '%' )->orwhere('sexe','like','%' .$search. '%' )->orwhere('adrs','like','%' .$search. '%' )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_search',['listes' => $listes,'total' => $total ]);
    }
    function between(){
        $debut = \Request::get('debut');
        $fin = \Request::get('fin');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->whereBetween('created_at',[$debut,$fin] )->get();
        $listes = Pers_phys::whereBetween('created_at',[$debut,$fin] )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_between',['listes' => $listes, 'total' => $total ]);
    }
    function date_choisie(){
        $dateC = \Request::get('date_choisie');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->whereDate('created_at',$dateC )->get();
        $listes = Pers_phys::whereDate('created_at',$dateC )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_date',['listes' => $listes,'total' => $total ]);
    }
    function month_choisie(){
        $dateC = \Request::get('month_choisie');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->whereMonth('created_at',$dateC )->get();
        $listes = Pers_phys::whereMonth('created_at',$dateC )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_month',['listes' => $listes,'total' => $total ]);
    }
    function day_choisie(){
        $day = \Request::get('day_choisie');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->whereDay('created_at',$day )->get();
        $listes = Pers_phys::whereDay('created_at',$day )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_day',['listes' => $listes,'total' => $total ]);
    }
    function annee_choisie(){
        $annee = \Request::get('annee_choisie');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->whereYear('created_at',$annee )->get();
        $listes = Pers_phys::whereYear('created_at',$annee )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_annee',['listes' => $listes,'total' => $total ]);
    }
    function fokontany(){
        $foko = \Request::get('fokontany');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->where('adrs','=',$foko )->get();
        $listes = Pers_phys::where('adrs','like','%' .$foko. '%' )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_adresse',['listes' => $listes,'total' => $total  ]);
    }
    function commune(){
        $foko = \Request::get('commune');
        $total = DB::table('Pers_phys')->selectRaw('count(id) as total')->where('commune','=',$foko )->get();
        $listes = Pers_phys::where('commune','=',$foko )->orderBy('id')->paginate(5);

        return view('requete.liste_inscrit_adresse',['listes' => $listes,'total' => $total ]);
    }
   
    public function show(){
     
        $c=DB::table('pers_phys')->latest()->first();
      
        return view('inscription.pers_phys_show',['pers'=>$c,]);
    }
}
