<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicule;
use App\Models\Pers_phys;
use Session;
use DB;
class vehiculesController extends Controller
{
  
    public function vehicules()
    {
      
        return view('inscription.vehicules_liste')->with('vehicules',Vehicule::all());
    }
    public function vehicules_add()
    {
        $c = DB::table('pers_phys')->latest()->first();
        return view('inscription.vehicules_add')->with('pers',$c);
    }
    public function vehicules_adds(Request $request)
    {
        $this->validate($request,[
            'num_veh' => 'required|numeric',
            'nbr_place' => 'numeric|nullable',
            'poid' => 'numeric|nullable',
            'charge' => 'numeric|nullable',
        ],[
            'num_veh.required' => 'Le numero du vehicule est obligatoire',
            'num_veh.numeric' => 'le numero du vehicule doit être en chriffre',
            'nbr_place.numeric' => 'Ce champ doit être en chriffre',
            'poid.numeric' => 'Ce champ doit être en chriffre',
            'charge.numeric' => 'Ce champ doit être en chriffre'
        ]);
         $vehicule = new Vehicule;
         $vehicule ->num_veh = $request ->num_veh;
         $vehicule ->marque = $request ->marque;
         $vehicule ->genre = $request ->genre;
         $vehicule ->type = $request ->type;
         $vehicule ->puissance = $request ->puissance;
         $vehicule ->nbr_place = $request ->nbr_place;
         $vehicule ->charge = $request ->charge;
         $vehicule ->poid = $request ->poid;
         $vehicule ->date_circ = $request ->date_circ;
         $vehicule ->usage_proff = $request ->usage_proff;
         $vehicule ->date_debut = $request ->date_debut;
         $vehicule ->exploitation = $request ->exploitation;
         $vehicule ->pers_phys_id = $request ->pers_phys_id;

         
         $vehicule->save();
         $e = DB::table('vehicules')->latest() ->first();
         Session::flash('successs','vehicules enregistrer');
        return view('inscription.vehicules_listes')->with('vehicule',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Vehicule::find($id)->delete();

        Session::flash('success','Vehicule supprimé');
        $e = DB::table('vehicules')->latest() ->first();
        return view('inscription.vehicules_listes')->with('vehicule',$e);
    

    }
}
