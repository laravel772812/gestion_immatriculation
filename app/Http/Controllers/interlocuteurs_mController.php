<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Interlocuteur_m;
use App\Models\Vehicule;
use Session;
use DB;
class interlocuteurs_mController extends Controller
{
  
    public function interlocuteurs_add()
    {
        $e = DB::table('pers_morals')->latest() ->first();
        return view('inscription.interlocuteurs_m_add',['pers' => $e]);
    }
    public function interlocuteurs_adds(Request $request)
    {
        $this->validate($request,[
            'nom_inter_m' => 'required',
            'adrs_inter_m' => 'required',
            'tel_inter_m' => 'required',
            'email_inter_m' => 'email',
        ],[
            'nom_inter_m.required' => 'Nom interlocuteur est obligatoire',
            'adrs_inter_m.required' => 'Adresse interlocuteur est obligatoire',
            'tel_inter_m.required' => 'Telephone interlocuteur est obligatoire',
           
            'email_inter_m.email' => 'Email  invalide',
        ]);
         $inter = new Interlocuteur_m;
         $inter ->nom_inter_m = $request ->nom_inter_m;
         $inter ->titre_m = $request ->titre_m;
         $inter ->adrs_inter_m = $request ->adrs_inter_m;
         $inter ->tel_inter_m = $request ->tel_inter_m;
         $inter ->email_inter_m = $request ->email_inter_m;
         $inter ->pers_moral_id = $request ->pers_moral_id;
        
         $inter->save();
         $c=DB::table('pers_morals')->latest()->first();
         $e=DB::table('etablissement_ms')->latest()->first();
         return view('inscription.references_m')->with('pers',$c)->with('etabl',$e);
        
    }
  
}
