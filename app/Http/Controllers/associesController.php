<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Associer;
use Session;
use DB;
class associesController extends Controller
{
    
    public function associer()
    {
      
        return view('inscription.associes_liste')->with('associer',Associer::all());
    }
    public function associer_add()
    {
       
     
        return view('inscription.associes_add');
    }
    public function associer_adds(Request $request)
    {
      
        $associer = new associer;
        $associer ->type_ass = $request ->type_ass;
       
        $associer ->nom_ass = $request ->nom_ass;
        $associer ->fonction_ass = $request ->fonction_ass;
        $associer ->cin_ass = $request ->cin_ass;
       
        $associer ->adrs_ass = $request ->adrs_ass;
        $associer ->activite_ass = $request ->activite_ass;
        $associer ->nif_activite_ass= $request ->nif_activite_ass;
        $associer ->email_ass = $request ->email_ass;
        $associer ->tel_ass = $request ->tel_ass;
        $associer ->ass_unique = $request ->ass_unique;
        $associer ->action = $request ->action;
        $associer ->nif = $request ->nif;
        $associer->save();
        $e = DB::table('associers')->latest() ->first();
        Session::flash('successs','associer enregistrer');
        return view('inscription.associer_listes')->with('associer',$e);
    }
    public function delete($id){
        //recherche par rapport a id supprimer
        Associer::find($id)->delete();

        Session::flash('success','associer supprimé');
        $e = DB::table('associers')->latest() ->first();
        return view('inscription.associer_listes')->with('associer',$e);
    

    }
}