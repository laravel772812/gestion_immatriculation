<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EtudiantsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
     {
      
    }
    public function messages()
    {
        return [
            'firstname.required' =>'le nom est obligatoire',
            'firstname.alpha' =>'le nom doit etre en lettre',
            'lastname.alpha' =>'le prenom doit etre en lettre'
        ];
    }
}
