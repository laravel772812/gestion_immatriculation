<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class activitesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' =>'required',
            'debut' =>'required',
            'cloture' =>'required',
            'import' =>'required',
            'export' =>'required',
            'nbr_salarier' =>'nullable|numeric',
        ];
    }
    public function messages()
    {
        return [
            'description.required' =>'champ obligatoire',
             'debut.required' =>'champ obligatoire',
            'cloture.required' =>'champ obligatoire',
            'import.required' =>'champ obligatoire',
            'export.required' =>'champ obligatoire',
            'nbr_salarier.numeric' =>'Nombre salarier doit être en chiffre',
        ];
    }
}
