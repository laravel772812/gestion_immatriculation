<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersPhysRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom_pers_phys' => 'required',
            'sit_mat' => 'required',
            'cin' => 'required',
            'sexe' => 'required',
            'date_del_cin' => 'required',
            'lieu_del_cin' => 'required',
            'num_stat' => 'required',
            'date_naiss' => 'required',
            'lieu_naiss' => 'required',
            'adrs' => 'required',
            'rib' => 'nullable',
            'description' =>'required',
            'debut' =>'required',
            'cloture' =>'required',
            'import' =>'required',
            'export' =>'required',
            'nbr_salarier' =>'nullable|numeric',
            
        ];
    }
    public function messages()
    {
        return [
            'nom_pers_phys.required' =>'nom est obligatoire',
            'sit_mat.required' =>'Champ est obligatoire',
            'sexe.required' =>'sexe est obligatoire',
         
            'cin.unique' =>'CIN deja utiliser',
            
            'date_del_cin.required' =>'date de delivrance cin est obligatoire',
            'lieu_del_cin.required' =>'lieu de delivrance cin est obligatoire',
            'num_stat.required' =>'numero statistique est obligatoire',
           
            'date_naiss.required' =>'date de naissance est obligatoire',
            'lieu_naiss.required' =>'lieu de naissance est obligatoire',
            'adrs.required' =>'Adresse est obligatoire',
            'description.required' =>'champ obligatoire',
            'debut.required' =>'champ obligatoire',
           'cloture.required' =>'champ obligatoire',
           'import.required' =>'champ obligatoire',
           'export.required' =>'champ obligatoire',
           
           'nbr_salarier.numeric' =>'Nombre salarier doit être en chiffre',
           
        ];
    }
}
