<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class etablissementsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          
            'date_ouvert' => 'required',
            'tel' => 'required',
            'email'=>'nullable|email',
            'pers_phys_id'=>'nullable',
        ];
    }
    public function messages()
    {
        return [
           
            'date_ouvert.required' => 'Date d ouverture est obligatoire',
            'tel.required' => 'Telephone est obligatoire',
            'email.email'=>'email invalide'
        ];
    }
}
