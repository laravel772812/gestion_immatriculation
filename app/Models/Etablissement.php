<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etablissement extends Model
{
    protected $fillable = [
        'nom_etabl','date_ouvert','tel','autre_tel','fax','email','exportateur','importateur','type_prop','nif_prop','nom_prop','cin_prop','adrs_prop','tel_prop','pers_phys_id'
    ];
 
    public function per_phys(){
        return $this->belongsTo(App\Models\Pers_phys);
    }
    public function dirigeant(){
        return $this->hasMany(App\Models\Dirigeant);
    }
}
