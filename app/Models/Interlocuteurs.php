<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interlocuteurs extends Model
{
    protected $fillable = [
        'nom_inter','titre','adrs_inter','tel_inter','email_inter','pers_phys_id'
    ];
 
    public function pers_phys(){
        return $this->belongsTo(App\Models\pers_phys);
    }
}
