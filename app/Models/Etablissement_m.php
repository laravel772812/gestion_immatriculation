<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class etablissement_m extends Model
{
    protected $fillable = [
        'nom_etabl_m','date_ouvert_m','tel_m','autre_tel_m','fax_m','email_m','exportateur_m','importateur_m','type_prop_m','nif_prop_m','nom_prop_m','cin_prop_m','adrs_prop_m','tel_prop_m','pers_morals_id'
    ];
 
    public function per_morals(){
        return $this->belongsTo(App\Models\Pers_moral);
    }
    public function dirigeant(){
        return $this->hasMany(App\Models\Dirigeant_m);
    }
}
