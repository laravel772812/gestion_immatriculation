<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $fillable = [
        'date_ref','confirmation','nouveau_nif','pers_phys_id','etablissement_id'
    ];
 
    public function per_phys(){
        return $this->belongsTo(App\Models\Pers_phys);
    }
    public function etablissement(){
        return $this->belongsTo(App\Models\Etablissement);
    }
}
