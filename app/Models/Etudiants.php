<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etudiants extends Model
{
    protected $fillable = ['code','name'];

    public function etudiants(){
        return $this->hasMany(App\Models\Etudiants);
    }
}
