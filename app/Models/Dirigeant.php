<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dirigeant extends Model
{
    protected $fillable = [
        'nom_dir','fonction','cin_dir','adrs_dir','email_dir','tel_dir','activite_dir','nif_activite_dir','etablissement_id'
    ];
 
    public function Etablissement(){
        return $this->belongsTo(App\Models\Etablissement);
    }
}
