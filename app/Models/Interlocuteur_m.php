<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class interlocuteur_m extends Model
{
    protected $fillable = [
        'nom_inter_m','titre_m','adrs_inter_m','tel_inter_m','email_inter_m','pers_moral_id'
    ];
 
    public function pers_moral(){
        return $this->belongsTo(App\Models\Pers_morals);
    }
}
