<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siege extends Model
{
    protected $fillable = [
        'adrs','fokontany','commune','district','region','province'
    ];
 
    public function classe(){
        return $this->belongsTo(App\Models\Classes);
    }
}
