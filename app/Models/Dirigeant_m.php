<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class dirigeant_m extends Model
{
    protected $fillable = [
        'nom_dir_m','fonction_m','cin_dir_m','adrs_dir_m','email_dir_m','tel_dir_m','autre_act','activite_dir_m','nif_activite_dir_m','etablissement_id'
    ];
 
    public function Etablissement_m(){
        return $this->belongsTo(App\Models\Etablissement_m);
    }
}
