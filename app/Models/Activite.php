<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Activite extends Model
{
    protected $fillable = [
        'description','precision','num_stat','date_deliv_stat','reg_comm','date_reg_comm','debut','cloture','import','export','nbr_salarier'
    ];
 
    public function classe(){
        return $this->belongsTo(App\Models\Classes);
    }
}
