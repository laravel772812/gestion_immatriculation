<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class reference_m extends Model
{
    protected $fillable = [
        'date_ref_m','confirmation_m','nouveau_nif_m','pers_moral_id','etablissement_m_id'
    ];
 
    public function per_moral(){
        return $this->belongsTo(App\Models\Pers_morals);
    }
    public function etablissement_ms(){
        return $this->belongsTo(App\Models\Etablissement_m);
    }
}
