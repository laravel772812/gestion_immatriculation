<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
   protected $fillable = [
       'firstname','lastname','email','phone','birthday','classes_id'
   ];

   public function classe(){
       return $this->belongsTo(App\Models\Classes);
   }
}
