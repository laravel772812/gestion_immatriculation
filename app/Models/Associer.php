<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Associer extends Model
{
    protected $fillable = [
        'type_ass','nom_ass','fonction_ass','cin_ass','adrs_ass','activite_ass','nif_activite_ass','email_ass','tel_ass','ass_unique','nif'
    ];
 
    public function classe(){
        return $this->belongsTo(App\Models\Classes);
    }
}
