<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class vehicule_m extends Model
{
    protected $fillable = [
        'num_veh_m','marque_m','genre_m','type_m','puissance_m','nbr_place_m','charge_m','poid_m','date_circ_m','usage_proff_m','date_debut_m','exploitation_m','pers_moral_id'
    ];
 
    public function pers_morals(){
        return $this->belongsTo(App\Models\Pers_morals);
    }
}
