<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicule extends Model
{
    protected $fillable = [
        'num_veh','marque','genre','type','puissance','nbr_place','charge','poid','date_circ','usage_proff','date_debut','exploitation','pers_phys_id'
    ];
 
    public function pers_phys(){
        return $this->belongsTo(App\Models\Pers_phys);
    }
}
