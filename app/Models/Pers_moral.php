<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pers_moral extends Model
{
    protected $fillable = [
        'raison_social','form_jurid','adrs_m','fokontany','description_m','precision_m','reg_comm_m','date_reg_comm_m','debut_m','cloture_m','nbr_salarier_m','capital_m','rib_m','reg_fisc_m','peri_grace_m'
    ];
 
    public function etablissement_ms(){
        return $this->hasMany(App\Models\Etablissement_m);
    }
    public function vehicule_ms(){
        return $this->hasMany(App\Models\Vehicule_m);
    }
    public function interlocuteur_ms(){
        return $this->hasMany(App\Models\Interlocuteur_m);
    }
}
