<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pers_phys extends Model
{
    protected $fillable = [
        'nom_pers_phys','prenom_pers_phys','sit_mat','sexe','cin','date_del_cin','lieu_del_cin','date_naiss','lieu_naiss','capital','rib','reg_fisc'
    ];
 
    public function etablissements(){
        return $this->hasMany(App\Models\Etablissement);
    }
    public function vehicules(){
        return $this->hasMany(App\Models\Vehicule);
    }
    public function interlocuteurs(){
        return $this->hasMany(App\Models\Interlocuteurs);
    }
}
