@extends('layouts.application')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div  id="carousel">
        <div class="container">
         
          <div class="row justify-content-center">
            <div class="col">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block" src="{{asset('dist/img/car1.jfif')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                   
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car2.jfif')}}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car3.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car4.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
            
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div><br>
   
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
    
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-3 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Plateforme DGI
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
  
                <div class="tab-content p-0">
                  <ul > 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/227-nifonline" title="NIFONLINE">NIFONLINE</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="https://entreprises.impots.mg/" title="Télé-declaration" target="_blank">Télé-declaration</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://www.a2d.impots.mg/" title="Attestation de destination" target="_blank">Attestation de destination</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://immo.impots.mg/" title="Valeur admin. immo" target="_blank">Valeur admin. immo</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://vehicule.impots.mg/" title="Valeur admin. vehicule" target="_blank">Valeur admin. vehicule</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://portal.impots.mg/" title="Portail de l'administration" target="_blank">Portail de l'administration</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="http://portal.impots.mg/textes" title="Textes Législatifs et Règlementaires" target="_blank">Textes Législatifs et Règlementaires</a></li> 
                    <li style="color:gray"><a style="color:gray;font-size:14px;" href="https://hetraonline.impots.mg/" title="HetraOnline" target="_blank">HetraOnline</a></li> 
                  </ul>
                </div>
              </div><!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                     DRI
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
  
                <div class="tab-content p-0">
                <ul>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/5-dri-alaotra-mangoro">DRI Alaotra Mangoro</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/35-dri-analamanga">DRI Analamanga</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/40-dri-anosy">DRI Anosy</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/57-dri-atsimo-andrefana">DRI Atsimo Andrefana</a></li>
                   <li  style="color:gray"><a  style="color:gray;font-size:14px;"  href="http://www.impots.mg/fr/contact/7-dri-atsinanana">DRI Atsinanana</a></li>
                   <li  style="color:gray"><a style="color:gray;font-size:14px;"  href="http://www.impots.mg/fr/contact/59-dri-boeny">DRI Boeny</a></li>
                   <li  style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/86-dri-diana">DRI Diana</a></li>
                   <li style="color:gray"><a style="color:gray;font-size:14px;"  href="http://www.impots.mg/fr/contact/92-dri-haute-matsiatra">DRI Haute Matsiatra</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/38-dri-itasy">DRI Itasy</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/109-dri-menabe">DRI Menabe</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/114-dri-sava">DRI Sava</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/76-dri-sofia">DRI Sofia</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/119-dri-vakinakaratra">DRI Vakinakaratra</a></li>
                   <li style="color:gray"><a  style="color:gray;font-size:14px;" href="http://www.impots.mg/fr/contact/127-dri-vatovavy-fitovinagny">DRI Vatovavy Fitovinagny</a></li>
                  </ul>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->

          </section>
          
          <!-- /.Left col -->
                <!-- center col -->
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
            
              <div class="card-body">
                <div class="tab-content p-0">
                  <h3>
                    <center> Bienvenue sur "DGI" <center>
                  </h3>  
                  <p style="color:gray; " >
                      Ce site regroupe en un seul espace les différentes fonctionnalités (Immatriculation, inscription, Consultation de situation fiscale).
                     
                  </p> <br>
                  <div class="card-header">
                    <h4 class="" style="color:orange">
                      <i class="nav-icon far fa-circle text-warning"></i>
                      Historique
                    </h4>
                  </div>
                <p style="color:gray;font-size:14px;">
                 <br>De 1975 jusqu’à 1997, la Direction des Impôts et la Direction des Douanes étaient dirigées par une seule Direction Générale dénommée « Direction Générale des Régies Financières (DGRF) ». La Direction chargée des Impôts était divisée en 3 services pratiquement indépendants : service des contributions directes, service des contributions indirectes et services de l’enregistrement et du timbre. En 1998, la Direction générale des impôts (DGI) a été créée et les 3...
                    <a href="#" style="color:green">Plus de detail</a>
                        
                  
                  </p><br>
                  <div class="card-header">
                    <h4 class="" style="color:orange">
                      <i class="nav-icon far fa-circle text-warning"></i>
                      Immatriculation
                    </h4>
                  </div>
                <p style="color:gray;font-size:14px;" >
                 <br> La Direction Générale des Impôts a initié des procédures en vue d’améliorer les services à distance qu’il offre aux contribuables. Les personnes physiques et morales de droit malgache il a été mis à leur disposition un site web permettant, désormais, d'obtenir un Numéro d’Identification Fiscale (NIF) en ligne.<br>
                    <a href="{{route('inscription')}}" style="color:green"><span class="nav-icon fas fa-edit"></span> S'inscrire en ligne</a>
                        
                  
                  </p>
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  
                </div>
                <!-- /.card-body -->
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  <br>
                </div>
                <!-- /.card-body -->
              </div>
              
            <!-- /.card -->

          </section>
          <!-- /.center col -->
         <!-- right col -->
         <section class="col-lg-3 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="fas fa-map mr-1"></i>
                 Adresse
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                   <p style="color:orange">DGI</p>

                   <span style="font-size:13px; color:gray;">  Immeuble MFB, Antaninarenina
                    Antananarivo, 101, Madagascar
                    Tél: (020) xx-xxx-xx <br>
                    E-mail: dgimpots@moov.mg </span><br>
                    <div class="card-header">
                    </div>
                    <span style="color:orange">DRI Haute  Matsiatra</span><br>

                   <span style="font-size:13px; color:gray;"> Immeuble des domaines TSIANOLONDROA , Fianarantsoa <br>
                    code postal:301, Madagascar
                    <br>Tél: 75 503 34, 032 12 010 59
                     E-mail: dri.hautematsiatra@gmail.com </span>
                </div>
              </div>
              
              <!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="nav-icon far fa-calendar-alt"></i>
                 Calendrier
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
               
               
              
                  <div class="card-body pt-0">
                    <!--The calendar -->
                   
                    <div id="calendar" style="width: 100%;"></div>
                  </div>
                  <!-- /.card-body -->
               
              </div>
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

         </section>
         <!-- /.right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
       
    <!-- /.content -->
  </div>
  <div id="sparkline-1"></div>
          <div id="sparkline-2"></div>
      
          <div id="sparkline-3"></div>
  <!-- /.content-wrapper -->
@endsection
