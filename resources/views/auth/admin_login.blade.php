@extends('layouts.app_admin')

@section('content')
<div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
          <div class="card-header bg-gradient-success pb-5">
              <div class="text-muted text-center text-white"> <strong>Vous êtes administrateur</strong><br>
                    <strong>Pour un utilisateur , appuyer   <a href="{{route('login')}}" class="">ici</a></strong></div>
             
            </div>
            <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                    <small>Entrer votre mot email et votre de passe </small><br>
                  
                </div>
                    <form class="form-horizontal" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}
                        <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input id="email" placeholder="Entrer votre email"  type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                             
                            </div>
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                 <input id="password"  placeholder="Entrer votre mot de passe"  type="password" class="form-control" name="password" required>

                              
                            </div>
                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input type="checkbox" id=" customCheckLogin" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                            <label class="custom-control-label" for=" customCheckLogin">
                                <span class="text-muted">Souvient moi</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Connecter</button>
                        </div>
                    </form>
                </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
                               
@endsection
