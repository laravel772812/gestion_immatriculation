@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
          <div class="card-header bg-gradient-success pb-5">
              <div class="text-muted text-center text-white"><strong>Creée un compte utilisateur</strong></div>
             
            </div>
            <div class="card-body px-lg-5 py-lg-5">
                
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group mb-3{{ $errors->has('name') ? ' has-error' : '' }}">
                          <div class="input-group input-group-alternative">

                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input id="name" type="text" placeholder="Nom" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                          </div>
                        </div>

                        <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                          <div class="input-group input-group-alternative">  

                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input id="email" type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                          </div>
                        </div>

                        <div class="form-group mb-3{{ $errors->has('password') ? ' has-error' : '' }}">
                          <div class="input-group input-group-alternative">  

                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input id="password" placeholder="Mot de passe" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                          </div>
                        </div>

                        <div class="form-group mb-3">
                          <div class="input-group input-group-alternative">  

                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input id="password-confirm" placeholder="Retaper mot de passe" type="password" class="form-control" name="password_confirmation" required>
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registre
                                </button>
                            </div>
                        </div>
                    </form>
                    </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
@endsection
