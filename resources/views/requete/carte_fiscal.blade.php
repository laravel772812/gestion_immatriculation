@extends('layouts.application')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  

    <br><section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
           

           @foreach($listes as $liste)     
            <!-- Main content -->
            <div class="col">
            <!-- general form elements disabled -->
            <div class="card card-warning" style="background-color: #f6fad9; border-left-color: #117a8b;">
              <div class="card-header" style="background-color: #f7f7bb;">
               <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>DIRECTION GENERALE DES IMPOTS</strong></h1> </center> <br><strong></strong>
              </div>
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#6bcf75;border: none;border-radius: unset;">
                <br><br><br><br><br><br><br><strong style="color:white">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 19%;" src="{{asset('dist/img/logo.png')}}" >
              </div>
              
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 520%;" src="{{asset('dist/img/logo-mfb.png')}}" >
              </div>
              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>CARTE FISCALE - ANNEE  {{ $liste->created_at + 0 }}</strong><span style="margin-left: 23%;font-size: medium;position: absolute;width: max-content;min-width: 10%;">Manankery hatramin'ny: {{ $liste->date_ref_m }}</span></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>(KARATRIN'NY MPANDOA HETRA)</strong><span style="margin-left: 50%;font-size: medium;position: absolute;width: max-content;font-style: italicmin-width: 10%;">(Valable jusqu'au)</span></h3><br>
              </div>

              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>Numéro d'identification fiscale  {{ $liste->nouveau_nif_m }}</small></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>(Laharam-pamatarana ara-ketra)</small></h3><br>
              </div>
              
              <div class="card-body" style="padding: 3.25rem;">
                <form role="form">
                  <div class="row">
                    <div class="col-6">
                      <p class="lead"></p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody><tr>
                            <th style="width:50%">Raison social(Anaran'ny orinasa):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->raison_social}}</td>
                          </tr>
                          <tr>
                            <th>Nom commercial(Anaran'ny tsena):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->nom_etabl_m}}</td>
                          </tr>
                          <tr>
                            <th>N°RCS/CIN(RCS/Karam-panondro):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                          <tr>
                            <th>Activité(Asa atao):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->description_m}}</td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>
                    <div class="col-6">
                      <p class="lead"></p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="width:50%">Adresse siège(Adiresy foibe):</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->adrs_m}} {{ $liste->fokontany}} </td>
                            </tr>
                            <tr>
                              <th>Numéro statistique</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                            </tr>
                            <tr>
                              <th>le(Tamin'ny):</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->created_at }}</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col">
                       <h6 class="text-right">Signature et cachet du chef du centre fiscale <br><br></h6>
                    </div>
                  </div>
          
              </div>
                
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#fd7b63;border: none;border-radius: unset;margin-right: -7%;float:left; left: 97%;">
                  <br><br><br><br><br><br><br><strong style="color:white;text-align:center">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="card-footer" style="background-color: #f7f7bb;">
              <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>N° 00{{ $liste->id }} /DGI</strong></h1> </center> <br><strong></strong>
              </div>
              <!-- /.card-body -->
            </div>
          @endforeach  
            <!-- /.card -->
          @foreach($listes as $liste)  
            <div class="card card-warning" style="background-color: #f6fad9; border-left-color: #117a8b;">
              <div class="card-header" style="background-color: #f7f7bb;">
               <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>DIRECTION GENERALE DES IMPOTS</strong></h1> </center> <br><strong></strong>
              </div>
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#6bcf75;border: none;border-radius: unset;">
                <br><br><br><br><br><br><br><strong style="color:white">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 19%;" src="{{asset('dist/img/logo.png')}}" >
              </div>
              
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 520%;" src="{{asset('dist/img/logo-mfb.png')}}" >
              </div>
              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>CARTE FISCALE - ANNEE  {{ $liste->created_at + 0 }}</strong><span style="margin-left: 23%;font-size: medium;position: absolute;width: max-content;min-width: 10%;">Manankery hatramin'ny: {{ $liste->date_ref_m }}</span></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>(KARATRIN'NY MPANDOA HETRA)</strong><span style="margin-left: 50%;font-size: medium;position: absolute;width: max-content;font-style: italicmin-width: 10%;">(Valable jusqu'au)</span></h3><br>
              </div>

              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>Numéro d'identification fiscale  {{ $liste->nouveau_nif_m }}</small></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>(Laharam-pamatarana ara-ketra)</small></h3><br>
              </div>
              
              <div class="card-body" style="padding: 3.25rem;">
               
                  <div class="row">
                    <div class="col-6">
                      <p class="lead">Etablissements</p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                          <tr>
                            <th>Nom commercial(Anaran'ny tsena):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->nom_etabl_m}}</td>
                          </tr>
                          <tr>
                            <th>Lieu d'exploitation(Taerana iasana):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                          <tr>
                            <th>Regime d'imposition:</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                          <tr>
                            <th>Anneé(taona):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                          <tr>
                            <th>Quittance(fanamarinana fandoavam-bola):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                          <tr>
                            <th>Date(Daty nandoavana azy):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;"></td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>
                    <div class="col-6">
                      <p class="lead">Vehicule/Fiara</p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="width:50%">N°imm/laharana:</th>
                              <td>{{ $liste->num_veh_m}}</td>
                            </tr>
                           
                            <tr>
                              <th>Marque(anarany)</th>
                              <td>{{ $liste->marque_m}}</td>
                            </tr>
                            
                            <tr>
                              <th>Puissance(tanjany)</th>
                              <td>{{ $liste->puissance_m}}</td>
                            </tr>
                            <tr>
                              <th>Nombre de place(isan'toerana)</th>
                              <td>{{ $liste->nbr_place_m}}</td>
                            </tr>
                            <tr>
                              <th>Charge utiles(vesatra antonony)</th>
                              <td>{{ $liste->charge_m}}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col">
                       <h6 class="text-right">Signature et cachet du gestionnaire <br><br></h6>
                    </div>
                  </div>
          
              </div>
                
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#fd7b63;border: none;border-radius: unset;margin-right: -7%;float:left; left: 97%;">
                  <br><br><br><br><br><br><br><strong style="color:white;text-align:center">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="card-footer" style="background-color: #f7f7bb;">
              <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:#f7f7bb;;min-width: 10%;"><strong>N° 00{{ $liste->id }} /DGI</strong></h1> </center> <br><strong></strong>
              </div>
              <!-- /.card-body -->
            </div>
            @endforeach  
           
          </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection