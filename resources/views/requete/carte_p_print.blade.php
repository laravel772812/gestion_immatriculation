<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
        <div class="row">
          <div class="col-12">
           

           @foreach($listes as $liste)     
            <!-- Main content -->
            <div class="col">
            <!-- general form elements disabled -->
            <div class="card card-warning" style="background-color: #f6fad9; border-left-color: #117a8b;">
              <div class="card-header" style="background-color: #f7f7bb;">
               <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>DIRECTION GENERALE DES IMPOTS</strong></h1> </center> <br><strong></strong>
              </div>
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#6bcf75;border: none;border-radius: unset;">
                <br><br><br><br><br><br><br><strong style="color:white">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 19%;" src="{{asset('dist/img/logo.png')}}" >
              </div>
              
              <div class="col-sm-2">
                <img  style="width: 60%;margin: 0;position: absolute;left: 520%;" src="{{asset('dist/img/logo-mfb.png')}}" >
              </div>
              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>CARTE FISCALE - ANNEE  {{ $liste->created_at + 0 }}</strong><span style="margin-left: 23%;font-size: medium;position: absolute;width: max-content;min-width: 10%;">Manankery hatramin'ny: {{ $liste->date_ref }}</span></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>(KARATRIN'NY MPANDOA HETRA)</strong><span style="margin-left: 50%;font-size: medium;position: absolute;width: max-content;font-style: italicmin-width: 10%;">(Valable jusqu'au)</span></h3><br>
              </div>

              <div class="col-sm-2">
                <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>Numéro d'identification fiscale  {{ $liste->nouveau_nif}}</small></h1><br>
              </div>
              <div class="col-sm-2">
                <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>(Laharam-pamatarana ara-ketra)</small></h3><br>
              </div>
              
              <div class="card-body" style="padding: 3.25rem;">
           
                  <div class="row">
                    <div class="col-6">
                      <p class="lead"></p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody><tr>
                            <th style="width:50%">Nom et prénom(Anarana sy fanampiny):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->nom_pers_phys}} {{ $liste->prenom_pers_phys}}</td>
                          </tr>
                          <tr>
                            <th>Nom commercial(Anaran'ny tsena):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->nom_etabl}}</td>
                          </tr>
                          <tr>
                            <th>CIN/Karam-panondro:</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->cin }}</td>
                          </tr>
                          <tr>
                            <th>Activité(Asa atao):</th>
                            <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->description}}</td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>
                    <div class="col-6">
                      <p class="lead"></p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="width:50%">Adresse siège(Adiresy foibe):</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->adrs}} </td>
                            </tr>
                            <tr>
                              <th>Numéro statistique</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->num_stat}}</td>
                            </tr>
                            <tr>
                              <th>le(Tamin'ny):</th>
                              <td style="font-weight: 600;text-align: left;letter-spacing: 1px;white-space: break-spaces;">{{ $liste->created_at }}</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col">
                       <h6 class="text-right">Signature et cachet du chef du centre fiscale <br><br></h6>
                    </div>
                  </div>
          
              </div>
                
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#fd7b63;border: none;border-radius: unset;margin-right: -7%;float:left; left: 97%;">
                  <br><br><br><br><br><br><br><strong style="color:white;text-align:center">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="card-footer" style="background-color: #f7f7bb;">
              <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>N° 00{{ $liste->id }} /DGI</strong></h1> </center> <br><strong></strong>
              </div>
              <!-- /.card-body -->
            </div>
        
            <!-- /.card -->
       
            <div class="card card-warning" style="background-color: #f6fad9; border-left-color: #117a8b;">
                    <div class="card-header" style="background-color: #f7f7bb;">
                    <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:gray;min-width: 10%;"><strong>DIRECTION GENERALE DES IMPOTS</strong></h1> </center> <br><strong></strong>
                    </div>
                    <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#6bcf75;border: none;border-radius: unset;">
                    <br><br><br><br><br><br><br><strong style="color:white">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
                    </div>
                    <div class="col-sm-2">
                    <img  style="width: 60%;margin: 0;position: absolute;left: 19%;" src="{{asset('dist/img/logo.png')}}" >
                    </div>
                    
                    <div class="col-sm-2">
                    <img  style="width: 60%;margin: 0;position: absolute;left: 520%;" src="{{asset('dist/img/logo-mfb.png')}}" >
                    </div>
                    <div class="col-sm-2">
                    <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>CARTE FISCALE - ANNEE  {{ $liste->created_at + 0 }}</strong><span style="margin-left: 23%;font-size: medium;position: absolute;width: max-content;min-width: 10%;">Manankery hatramin'ny: {{ $liste->date_ref }}</span></h1><br>
                    </div>
                    <div class="col-sm-2">
                    <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><strong>(KARATRIN'NY MPANDOA HETRA)</strong><span style="margin-left: 50%;font-size: medium;position: absolute;width: max-content;font-style: italicmin-width: 10%;">(Valable jusqu'au)</span></h3><br>
                    </div>

                    <div class="col-sm-2">
                        <h1  style="position: absolute;float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>Numéro d'identification fiscale  {{ $liste->nouveau_nif }}</small></h1><br>
                    </div>
                    <div class="col-sm-2">
                        <h3  style="font-style: italic;position: absolute;float: revert;font-size: 1.1rem;font-weight: 400;margin: -3px;margin-left: -3px;color:#4a5243;margin-left: 78%;width: max-content;min-width: 10%;"><small>(Laharam-pamatarana ara-ketra)</small></h3><br>
                    </div>
              
                <div class="card-body" style="padding: 3.25rem;">
               
                  <div class="row">
                    <div class="col-6">
                      <p class="lead">Etablissements</p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                          <tr>
                            <th>Nom commercial(Anaran'ny tsena):</th>
                            <td>{{ $liste->nom_etabl}}</td>
                          </tr>
                          <tr>
                            <th>Lieu d'exploitation(Taerana iasana):</th>
                            <td></td>
                          </tr>
                          <tr>
                            <th>Regime d'imposition:</th>
                            <td></td>
                          </tr>
                          <tr>
                            <th>Anneé(taona):</th>
                            <td></td>
                          </tr>
                          <tr>
                            <th>Quittance(fanamarinana fandoavam-bola):</th>
                            <td></td>
                          </tr>
                          <tr>
                            <th>Date(Daty nandoavana azy):</th>
                            <td></td>
                          </tr>
                        </tbody></table>
                      </div>
                    </div>
                    <div class="col-6">
                      <p class="lead">Vehicule/Fiara</p>

                      <div class="table-responsive">
                        <table class="table">
                          <tbody>
                            <tr>
                              <th style="width:50%">N°imm/laharana:</th>
                              <td>{{ $liste->num_veh}}</td>
                            </tr>
                           
                            <tr>
                              <th>Marque(anarany)</th>
                              <td>{{ $liste->marque}}</td>
                            </tr>
                            
                            <tr>
                              <th>Puissance(tanjany)</th>
                              <td>{{ $liste->puissance}}</td>
                            </tr>
                            <tr>
                              <th>Nombre de place(isan'toerana)</th>
                              <td>{{ $liste->nbr_place}}</td>
                            </tr>
                            <tr>
                              <th>Charge utiles(vesatra antonony)</th>
                              <td>{{ $liste->charge}}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col">
                       <h6 class="text-right">Signature et cachet du gestionnaire <br><br></h6>
                    </div>
                  </div>
          
              </div>
                
              <div class="callout callout-info" style="width: 0%;height: 80%;position: absolute;margin-top: 7%;background-color:#fd7b63;border: none;border-radius: unset;margin-right: -7%;float:left; left: 97%;">
                  <br><br><br><br><br><br><br><strong style="color:white;text-align:center">D</strong><br><br><br><br><strong style="color:white">G</strong><br><br><br><br><strong style="color:white">I</strong>
              </div>
              <div class="card-footer" style="background-color: #f7f7bb;">
              <center> <h1 class="card-title" style="float: revert;font-size: 1.6rem;font-weight: 400;margin: -3px;color:#f7f7bb;;min-width: 10%;"><strong>N° 00{{ $liste->id }} /DGI</strong></h1> </center> <br><strong></strong>
              </div>
              <!-- /.card-body -->
            </div>
            @endforeach  
           
            <script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>