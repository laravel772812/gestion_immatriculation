@extends('layouts.application')

@section('content')
<div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Confirmation</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Confirmation</li>
             
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>References</small></h1></center>
                </div>
                <form method="GET" action="{{route('references.search')}}"  class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
                      <div class="form-group mb-0">
                        <div class="input-group input-group-alternative">
                        
                          <input style="color:green;" class="form-control" placeholder="Search" type="text" name="title">
                          <div class="input-group-prepend">
                            <button class="btn bg-transparent" type="submit"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                </form>
            @if(Auth::guard('admin')->check())
             
              <div class="table-responsive">
                @if(Session::has('success'))
                <div class="col-md-6">
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                    
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <strong>Succés! </button> {{Session::get('success')}}
                  </div>
                </div>
                @endif
                <div class="row">
               
                </div>
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                    <th scope="col">Numero reference</th>
                  
                 
                      <th scope="col">nom personne</th>
                  
                      <th scope="col">nom commerciale</th>
                   
                      <th scope="col">nouveau_nif</th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                @if(count($listes_ref)>0)    
                  @foreach ($listes_ref as $liste)
              
                      <tr scope="row">
                        <td>{{ $liste->id }}</td>
                        <td>{{ $liste->nom_pers_phys }}</td>
                        <td>{{ $liste->nom_etabl }}</td>
                      
                        <td> 
                            @if(  $liste->nouveau_nif  == null)
                              <span class="badge badge-success">En attente</span>
                            @else  
                              {{ $liste->nouveau_nif }} 
                            @endif   
                          </td>
                         
                           
                            <td class="text-right">
                              <a class="btn btn-danger btn-sm"  href="{{route('references.delete',['id'=>$liste->id]) }}"><i  class="fas fa-trash"></i></a>
                            </td>
                            <td class="text-right">
                              <a class="btn btn-info btn-sm"  href="{{route('references.show',['id'=>$liste->id]) }}"><i  class="fas fa-pencil-alt"></i></a>
                            </td>
                            <td class="text-right">
                            @if(  $liste->nouveau_nif  != 0)
                              <a class="btn btn-default"  href="{{ route('carte_p',['id'=>$liste->id]) }}"><i  class="fas fa-print"></i></a>
                            @endif
                            </td>
                      </tr>

                  @endforeach
                @else
               
                 <center><h1 class="mb-0" style="color:gray;"><small>Oups! Aucun resultat pour  "{{ Request::get('title') }}""  !!!</small></h1>   </center>  
                
                @endif      
                 
                      
                  
                  
                  </tbody>
                </table>
                <center><a href="{{ route('references.liste') }}" class="btn btn-outline-success">Retour</a></center>
              </div>
             
           
             
             
            @endif  
            <div class="card-footer py-4">
                <div class="pagination">
                 
                </div>
              </div>
              </div> 
            </div> 
           
          </div>
        </div>
      </section>
</div>
@endsection