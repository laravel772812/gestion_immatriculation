@extends('layouts.application')

@section('content')
<div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Liste</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">liste inscrit</li>
             
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Resultat de votre recherche</small></h1></center>
                </div>
               
            @if(Auth::guard('admin')->check())
              <div class="table-responsive">
                <div class="row">
                    @foreach($total as $tot)
                     &nbsp &nbsp &nbsp &nbsp<h2 class="mb-0" style="color:orange;"><small> Total : {{ $tot->total}} </small></h2>
                    @endforeach
                  </div>
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Nom</th>
                      <th scope="col">Prenom</th>
                      <th scope="col">Situation matrimonial</th>
                      <th scope="col">Sexe</th>
                      <th scope="col">CIN</th>
                      <th scope="col">Date de delivrance CIN</th>
                      <th scope="col">Lieu delivrance CIN</th>
                      <th scope="col">Numero statistique</th>
                      <th scope="col">Date de délivrance statistique</th>
                      <th scope="col">Date de naissance</th>
                      <th scope="col">Lieu de naissance</th>
                      <th scope="col">Adresse actuel</th>
                      <th scope="col">Fokontany</th>
                      <th scope="col">Commune</th>
                      <th scope="col">District</th>
                      <th scope="col">Region</th>
                      <th scope="col">Province</th>
                      <th scope="col">liste</th>
                      <th scope="col">precision liste</th>
                      <th scope="col">registre de commerce</th>
                      <th scope="col">Début d'exercice</th>
                      <th scope="col">Cloture d'exercice</th>
                      <th scope="col">Importateur</th>
                      <th scope="col">Exportateur</th>
                      <th scope="col">Nombre de salarier</th>
                      <th scope="col">Date de creation</th>
                      <th scope="col">capital</th>
                      <th scope="col">rib</th>
                      <th scope="col">regime fiscal</th>
                      <th scope="col">Date agreer</th>
                      <th scope="col">ref agre</th>
                      <th scope="col">periode grace</th>
                      <th scope="col">Date d' inscription</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
               @if(count($listes)>0)   
                @foreach ($listes as $liste)
              
                      <tr scope="row">
                            <td>{{ $liste->nom_pers_phys }}</td>
                            <td>{{ $liste->prenom_pers_phys }}</td>
                            <td>{{ $liste->sit_mat }}</td>
                            <td>{{ $liste->sexe }}</td>
                            <td>{{ $liste->cin}}</td>
                            <td>{{ $liste->date_del_cin }}</td>
                            <td>{{ $liste->lieu_del_cin }}</td>
                            <td>{{ $liste->num_stat}}</td>
                            <td>{{ $liste->date_deliv_cin }}</td>
                            <td>{{ $liste->date_naiss }}</td>
                            <td>{{ $liste->lieu_naiss }}</td>
                            <td>{{ $liste->adrs}}</td>

                            <td>{{ $liste->fokontany }}</td>
                            <td>{{ $liste->commune }}</td>
                            <td>{{ $liste->district }}</td>
                            <td>{{ $liste->region}}</td>
                            <td>{{ $liste->province}}</td>

                            <td>{{ $liste->description }}</td>
                            <td>{{ $liste->precision }}</td>
                            <td>{{ $liste->reg_comm }}</td>
                            <td>{{ $liste->date_reg_comm }}</td>
                            <td>{{ $liste->debut }}</td>
                            <td>{{ $liste->cloture }}</td>
                            <td>{{ $liste->import }}</td>
                            <td>{{ $liste->export}}</td>
                            <td>{{ $liste->nbt_salarier}}</td>

                            
                            <td>{{ $liste->date_creation}}</td>
                            <td>{{ $liste->capital }}</td>
                            <td>{{ $liste->rib }}</td>
                            <td>{{ $liste->reg_fisc}}</td>
                            <td>{{ $liste->date_agre }}</td>
                            <td>{{ $liste->ref_agre}}</td>
                            <td>{{ $liste->peri_grace }}</td>
                            <td>{{ $liste->created_at}}</td>
                         
                            <td class="text-right">
                            <td class="text-right">
                              <a class="btn btn-danger btn-sm"  href="{{route('pers_phys.delete',['id'=>$liste->id]) }}"><i  class="fas fa-trash"></i></a>
                            </td>
                            </td>
                      </tr>

                  @endforeach
                @else
                  <center><h1 class="mb-0" style="color:gray;"><small>Oups! Aucun resultat pour  "{{ Request::get('title') }}""  !!!</small></h1>   </center>  
                @endif    
                      
                  
                  
                  </tbody>
                </table>
                
              </div>
              
                <div class="pagination">
                 {{ $listes->links() }}
                </div> 
                <div class="card-footer py-4">
                <center><a href="{{ route('liste_inscrit') }}" class="btn btn-outline-success">Toutes les listes</a></center>
              </div>
            @endif  
         
            </div> 
            </div> 
           
          </div>
        </div>
      </section>
</div>
@endsection