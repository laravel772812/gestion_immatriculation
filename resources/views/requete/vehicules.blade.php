@extends('layouts.application')

@section('content')
<div class="content-wrapper"><br>
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>@foreach($pers_phys as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Personne physique</p>
              </div>
              <div class="icon">
                 <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>@foreach($pers_morals as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Personne morale</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>@foreach($users as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Utilisateur</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>@foreach($admins as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Administrateur</p>
              </div>
              <div class="icon">
              <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
        </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
          
              
                <section class="col bg-light" >
                  <div class="card">
                    <div class="card-body">
                      <ul  style="list-style:none; ">  
                        <li class="nav-item dropdown" style="float:left;" >
                          <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Date d'inscription</a>
                          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a data-toggle="modal" data-target="#modal-default" href="#"class="dropdown-item">Date entre()</a></li>
                            <li><a  data-toggle="modal" data-target="#modal-overlay" href="#" class="dropdown-item">Choisir une date</a></li>
                            <li><a  data-toggle="modal" data-target="#month" href="#" class="dropdown-item">Choisir un Moi</a></li>
                            <li><a  data-toggle="modal" data-target="#day" href="#" class="dropdown-item">Choisir un jour</a></li>
                            <li><a  data-toggle="modal" data-target="#annee" href="#" class="dropdown-item">Choisir une année</a></li>
                         

                          
                          </ul>
                        </li>
                        <li class="nav-item dropdown" style="float:left;">
                          <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Adresse</a>
                          <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                            <li><a data-toggle="modal" data-target="#fokontany" href="#"class="dropdown-item">Adresse actuel</a></li>

                          </ul>
                        </li>
                        <li class="nav-item" style="float:left;">
                          <a href="{{route('etablissements')}}" class="nav-link">Etablissements</a>
                        </li>
                        <li class="nav-item" style="float:left;">
                          <a href="{{route('dirigeants')}}" class="nav-link">Dirigeants</a>
                        </li>
                        <li class="nav-item" style="float:left;">
                          <a href="#" class="nav-link">Vehicules</a>
                        </li>
                        <li class="nav-item" style="float:left;">
                          <a href="{{route('interlocuteurs')}}" class="nav-link">Interlocuteurs</a>
                        </li>
                      </ul>  
                   
                    </div>
                  </div>
                </section>
                
                   
                    <script language="javascript">
                        function controlleMoi(form_moi){
                                        var val=document.form_moi.month_choisie.value;
                                        if(isNaN(val) == false){
                                            var n=val.length;
                                            if (n==1 || n==2 ){
                                                var pc = val.substring(0,1);
                                                var pcc = val.substring(0,2);
                                                
                                                if(pc==0 || pc==1 || pcc==01  || pcc==02 || pcc==03 || pcc==04 || pcc==05  || pcc==06 || pc==7  || pcc==08  || pcc==09  || pcc==10 || pcc==11 || pcc==12){
                                                    document.form_moi.month_choisie.value=val.substring(0,2);
                                                    
                                                }else{
                                                  document.getElementById('month_choisie').innerHTML= "Ce moi n'existe pas";
                                                  
                                                }
                                            }else{
                                                document.getElementById('month_choisie').innerHTML ="ce moi est incorrect";
                                        
                                            }
                                        }else{
                                          document.getElementById('month_choisie').innerHTML="moi doit être en chriffre";
                                        }
                                    }
                    </script>
                    <div class="modal fade" id="modal-default">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Choisir la date</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form method="GET"  action="{{ url('/liste_inscrit/between') }}">
                                <div class="col-md-7">
                                    <div class="form-group">
                                    <p>Du:</p>
                                        <input class="form-control form-control-sm" type="date"  name="debut">
                                    </div>
                                
                                </div> 
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <p>Jusque à:</p>
                                        <input class="form-control form-control-sm" type="date"  name="fin">
                                    </div>
                                
                                </div>  
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-primary">Valider</button>
                          </div>
                          </form>

                      </div>
                      <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <div class="modal fade" id="modal-overlay">
                    <div class="modal-dialog">
                      <div class="modal-content">
                       
                        <div class="modal-header">
                          <h4 class="modal-title"></h4>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <form method="GET" action="{{ url('/liste_inscrit/date_choisie') }}">
                            <div class="col-md-7">
                                <div class="form-group">
                                <p>Choisir la date:</p>
                                    <input class="form-control form-control-sm" type="date"  name="date_choisie">
                                </div>
                            
                            </div> 
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                          <button type="submit" class="btn btn-primary">Ok</button>
                        </div>
                        </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                  <!-- /.modal-dialog -->
                  </div>

                  <div class="modal fade" id="month">
                      <div class="modal-dialog">
                        <div class="modal-content">
                         
                          <div class="modal-header">
                            <h4 class="modal-title"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form method="GET" id="form_moi" name="form_moi" action="{{ url('/liste_inscrit/month_choisie') }}">
                              <div class="col-md-7">
                                  <div class="form-group">
                                  <p>Choisir un moi:</p>
                                      <input  onmouseOut="controlleMoi(form_moi)" autoComplete="off"  placeholder="01 jusque à 12" class="form-control form-control-sm" type="text"  name="month_choisie">
                                  </div>
                                  <p class="text-danger" id="month_choisie"></p>
                              </div> 
                          </div>
                          <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <button type="submit"  class="btn btn-primary">Ok</button>
                          </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                    <!-- /.modal-dialog -->
                  </div>
                  <div class="modal fade" id="day">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h4 class="modal-title"></h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <form   method="GET" action="{{ url('/liste_inscrit/day_choisie') }}">
                                <div class="col-md-7">
                                    <div class="form-group">
                                    <p>Choisir un jour :</p>
                                        <input placeholder="1 jusque à 31" class="form-control form-control-sm" type="text"  name="day_choisie">
                                    </div>
                                
                                </div> 
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                              <button type="submit" class="btn btn-primary">Ok</button>
                            </div>
                            </form>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                      <!-- /.modal-dialog -->
                  </div>
                  <div class="modal fade" id="annee">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            
                            <div class="modal-header">
                              <h4 class="modal-title"></h4>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                              <form method="GET" action="{{ url('/liste_inscrit/annee_choisie') }}">
                                <div class="col-md-7">
                                    <div class="form-group">
                                    <p>Choisir une année :</p>
                                        <input placeholder="" class="form-control form-control-sm" type="text"  name="annee_choisie">
                                    </div>
                                
                                </div> 
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                              <button type="submit" class="btn btn-primary">Ok</button>
                            </div>
                            </form>
                          </div>
                          <!-- /.modal-content -->
                        </div>
                      <!-- /.modal-dialog -->
                  </div>

                  <div class="modal fade" id="fokontany">
                          <div class="modal-dialog">
                            <div class="modal-content">
                             
                              <div class="modal-header">
                                <h4 class="modal-title"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form method="GET" action="{{ url('/liste_inscrit/fokontany') }}">
                                  <div class="col-md-7">
                                      <div class="form-group">
                                      <p>Entrer l'adresse  :</p>
                                          <input placeholder="adresse" class="form-control form-control-sm" type="text"  name="fokontany">
                                      </div>
                                  
                                  </div> 
                              </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                <button type="submit" class="btn btn-primary">Ok</button>
                              </div>
                              </form>
                            </div>
                            <!-- /.modal-content -->
                          </div>
                        <!-- /.modal-dialog -->
                  </div>
             
                
            @if(Auth::guard('admin')->check())
            
              
             <section class="col">
             <div class="card">
              <div class="card-body">
              <h3 style="color:orange;text-align:center">Listes vehicules</h3><br>
              <form method="GET" action="{{url('/admin/vehicules/search')}}"  class="navbar-search navbar-search-light form-inline mr-3 d-none d-md-flex ml-lg-auto">
                      <div class="form-group mb-0">
                        <div class="input-group input-group-alternative">
                        
                          <input style="color:green;" class="form-control" placeholder="Search" type="text" name="title">
                          <div class="input-group-prepend">
                            <button class="btn bg-transparent" type="submit"><i class="fas fa-search"></i></button>
                          </div>
                        </div>
                      </div>
                </form><br>
                <div class="table-responsive">
                
                  @if(Session::has('success'))
                  <div class="col-md-6">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Succés! </button> {{Session::get('success')}}
                    </div>
                  </div>
                  @endif
                  <div class="row">
                
                  </div>
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                                <th>id personne</th>
                                <th>Nom personne</th>
                                <th>Numéro vehicule</th>
                                <th>Marque</th>
                                <th>Genre</th>
                                <th>Type</th>
                                <th>Puissance</th>
                                <th>Nombre de place</th>
                                <th>Charge</th>
                                <th>Poid</th>
                                <th>Date de circulation</th>
                                <th>A usage professionnel</th>
                                <th>Date de début</th>
                                <th>Exploitation</th>
                      
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>
                    
                    @foreach ($listes_v as $liste)
                  
                          <tr scope="row">
                            <td >{{ $liste->pers_phys_id }}</td>
                            <td >{{ $liste->nom_pers_phys }}</td>
                            <td >{{ $liste->num_veh }}</td>
                            <td >{{ $liste->marque }}</td>
                            <td >{{ $liste->genre }}</td>
                            <td >{{ $liste->type }}</td>
                            <td>{{ $liste->puissance }}</td>
                            <td>{{ $liste->nbr_place }}</td>
                            <td>{{ $liste->charge }}</td>
                            <td>{{ $liste->poid}}</td>
                            <td>{{ $liste->date_circ}}</td>
                            <td>{{ $liste->usage_proff}}</td>
                            <td>{{ $liste->date_debut}}</td>
                            <td>{{ $liste->exploitation}}</td>
                              
                            <td class="text-right">
                                <a class="btn btn-danger btn-sm"  href="{{route('veh.delete',['id'=>$liste->id]) }}"><i  class="fas fa-trash"></i></a>
                            </td>
                          </tr>

                    @endforeach
                      
                  
                        
                    
                    
                    </tbody>
                  </table>
              
                </div>
              
                <div class="card-footer py-4">
                  <div class="pagination">
                  {{ $listes_v->links() }}
                  </div>
                </div>
                </div>
              </div>
              </section>
            @endif  
            </div> 
            </div> 
           
          </div>
        </div>
      </section>
</div>
@endsection