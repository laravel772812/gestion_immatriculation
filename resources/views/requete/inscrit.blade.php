@extends('layouts.application')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"><br>
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>@foreach($pers_phys as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Personne physique</p>
              </div>
              <div class="icon">
                 <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>@foreach($pers_morals as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Personne morale</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>@foreach($users as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Utilisateur</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>@foreach($admins as $pers) {{ $pers->total}} @endforeach</h3>

                <p>Administrateur</p>
              </div>
              <div class="icon">
              <i class="fas fa-user-plus"></i>
              </div>
              <a href="#" class="small-box-footer">
                Plus detail <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
        </div>
   
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
    
        <!-- Main row -->
        <div class="row">
        
                <!-- center col -->
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-body">
                
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Listes personnes physiques</h3>
                    </div>
                    <div class="table-responsive">
                          <table class="table m-0">
                            <thead>
                                <tr>
                                <th scope="col">id</th>
                                <th scope="col">Sexe</th>
                                <th scope="col">Activité</th>
                                <th scope="col">Adresse</th>
                                <th scope="col">Date d'inscription</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                        
                                @foreach ($listes_p as $liste)
                                
                                        <tr scope="row">
                                            <td>{{ $liste->id }}</td>
                                            <td>{{ $liste->sexe }}</td>
                                            <td>{{ $liste->description }}</td>
                                            <td>{{ $liste->adrs}}</td>
                                            <td>{{ $liste->created_at}}</td>
                                            
                                            
                                        </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                       
                        <a href="{{route('liste_inscrit')}}" class="btn btn-sm btn-secondary float-right">Tout les personnes physiques</a>
                    </div> 
                
              </div> 
            </div>
          
        

          </section>
          <!-- /.center col -->
         <!-- right col -->
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-body">
                
                    <div class="card-header border-transparent">
                        <h3 class="card-title">Listes personnes morales</h3>
                    </div>
                    <div class="table-responsive">
                          <table class="table m-0">
                            <thead>
                                <tr>
                                <th scope="col">id</th>
                                <th scope="col">Raison social</th>
                                <th scope="col">Activité</th>
                                <th scope="col">Adresse</th>
                               
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                        
                                @foreach ($listes_m as $liste)
                                
                                        <tr scope="row">
                                            <td>{{ $liste->id }}</td>
                                            <td>{{ $liste->raison_social }}</td>
                                            <td>{{ $liste->description_m }}</td>
                                            <td>{{ $liste->adrs_m}}</td>
                                           
                                            
                                            
                                        </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix">
                       
                        <a href="{{route('pers_morals')}}" class="btn btn-sm btn-secondary float-right">Tout les personnes morales</a>
                    </div> 
                
              </div> 
            </div>
       

          </section>

          
         <!-- /.right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
       
    <!-- /.content -->
  </div>
 
  <!-- /.content-wrapper -->
@endsection
