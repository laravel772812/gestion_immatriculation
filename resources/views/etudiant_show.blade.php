@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Affichage de l' etudiant N° {{ $etudiant->id}}</div>

                <div class="panel-body">
                   <form action="" method="post">
                   {{ csrf_field() }}
                      
                       <div class="form-group">
                           <label for="firstname">Nom</label>
                           <input type="text" name="firstname"  value="{{ $etudiant->firstname}}" id="firstname" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="lastname">Prenom</label>
                           <input type="text" name="lastname"  value="{{ $etudiant->lastname}}" id="lastname" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="email">Email</label>
                           <input type="text" name="email"  value="{{ $etudiant->email}}" id="email" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="phone">Téléphone</label>
                           <input type="text" name="phone"   value="{{ $etudiant->phone}}" id="phone" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="birthday">Date d naissance</label>
                           <input type="text" name="birthday"  value="{{ $etudiant->birthday}}" id="birthday" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="classe">Classe</label>
                           <select name="classes_id" id="classe" class="form-control">
                              @foreach($classes as $classes)
                                  @if($classes->id == $etudiant->classes_id)
                                   <option value="{{ $classes->id }}" selected>{{ $classes->name }}</option>
                                  @else
                                  <option value="{{ $classes->id }}">{{ $classes->name }}</option>
                                  @endif
                              @endforeach
                           </select>
                        </div>
                       <div class="form-group">
                           <center>
                             <a href="{{ route("etudiant") }}"  class="btn btn-success pull-center">Retour</a>
                           </center>
                       </div>
                   </form>
                </div>
                
                <div class="text-center panel-footer">
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection