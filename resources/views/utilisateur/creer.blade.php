@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Utilisateur</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Utilisateur</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
           <div class="col">
              <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Utilisateur</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                </div>
              </div>
              <div class="card-body">
                 <form action="{{ route('admins.creer')}}" method="post">
                  {{ csrf_field() }}
                    @if(Session::has('success'))
                      <div class="col-md-6">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Succés! </button> {{Session::get('success')}}
                        </div>
                      </div>
                    @endif

                   
                <div class="row">
                      
                       <div class="col-md-5">
                            <div class="form-group">
                                <p>Nom</p>
                                <input type="text" name="name"   class="form-control form-control-sm"  required>
                            
                            </div> 
                            @if($errors->has('name'))
                             <p class="text-danger">{{ $errors->first('name') }}</p>
                            @endif
                        </div>  
                        <div class="col-md-5">     
                            <div class="form-group">
                                <p>Email</p>
                                <input type="text" name="email" class="form-control form-control-sm"  required>
                            
                            </div>  
                            @if($errors->has('email'))
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                        @endif
                        </div>  
                        <div class="col-md-5"> 
                            <div class="form-group">
                                <p>Fonction</p>
                                <input type="text" name="job_title"   class="form-control form-control-sm"  required>
                            
                            </div>  
                            @if($errors->has('job_title'))
                        <p class="text-danger">{{ $errors->first('job_title') }}</p>
                        @endif
                        </div> 
                        <div class="col-md-5"> 
                            <div class="form-group">
                                <p>Mot de passe</p>
                                <input type="password" name="password"   class="form-control form-control-sm"  required>
                            
                            </div>  
                            @if($errors->has('password'))
                        <p class="text-danger">{{ $errors->first('password') }}</p>
                        @endif
                        </div> 
                        <div class="col-md-5"> 
                            <div class="form-group">
                                <p>Retaper mot de passe</p>
                                <input type="password" name="password_confirmation"   class="form-control form-control-sm" required>
                            
                            </div>  
                            @if($errors->has('password_confirmation'))
                        <p class="text-danger">{{ $errors->first('password_confirmation') }}</p>
                        @endif
                        </div> 
                       
                 </div>
                  
                    <div class="col-12" >
                      <a href="{{route('admin.liste')}}" class="btn btn-secondary">Retour</a>
                      <input type="submit" value="Valider" class="btn btn-success float-right" >
                    </div>              
                </div>
                
              </div>
            </div>
                         
           
            </form> 
            </form> 
        </div>  <br>
      </section>    
  
  </div>

@endsection