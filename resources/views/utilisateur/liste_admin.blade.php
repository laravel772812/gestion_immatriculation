@extends('layouts.application')

@section('content')
<div class="content-wrapper"><br>
        <section class="content">
          <div class="row">
              <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3>@foreach($pers_phys as $pers) {{ $pers->total}} @endforeach</h3>

                    <p>Personne physique</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user-plus"></i>
                  </div>
                  <a href="#" class="small-box-footer">
                    Plus detail <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3>@foreach($pers_morals as $pers) {{ $pers->total}} @endforeach</h3>

                    <p>Personne morale</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user-plus"></i>
                  </div>
                  <a href="#" class="small-box-footer">
                    Plus detail <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h3>@foreach($users as $pers) {{ $pers->total}} @endforeach</h3>

                    <p>Utilisateur</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-user-plus"></i>
                  </div>
                  <a href="#" class="small-box-footer">
                    Plus detail <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small card -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h3>@foreach($admins as $pers) {{ $pers->total}} @endforeach</h3>

                    <p>Administrateur</p>
                  </div>
                  <div class="icon">
                  <i class="fas fa-user-plus"></i>
                  </div>
                  <a href="#" class="small-box-footer">
                    Plus detail <i class="fas fa-arrow-circle-right"></i>
                  </a>
                </div>
              </div>
            <!-- ./col -->
          </div>
       

          @if(Session::has('success'))
                  <div class="col-md-6">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Succés! </button> {{Session::get('success')}}
                    </div>
                  </div>
                  @endif

          <div class="card card-solid">
            <div class="card-body pb-0">
              <div class="row d-flex align-items-stretch">

                
                 
                @foreach ($listes as $liste)
                  

                  <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                    
                    <div class="card bg-light">
                      <div class="card-header text-muted border-bottom-0">
                      {{ $liste->id }}
                      </div>
                      <div class="card-body pt-0">
                        <div class="row">
                          <div class="col-7">
                            <h2 class="lead"><b>{{ $liste->name}}</b></h2>
                            <p class="text-muted text-sm"><b>Fonction: </b>{{ $liste->job_title}} </p>
                            
                            <ul class="ml-4 mb-0 fa-ul text-muted">
                              <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Email:{{ $liste->email }}</li>
        
                            </ul>
                          </div>
                          <div class="col-5 text-center">
                            <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                          </div>
                        </div>
                      </div>
                      <div class="card-footer">
                      
                        <div class="text-right">
                         <a href="{{ route('admin.creer') }}" onclick="return(confirm('Voulez-vous creer un compte administrateur?'))" class="btn btn-sm bg-default">
                          <i  class="fas fa-pencil-alt"></i>
                          </a>
                          <a href="{{route('admin.delete',['id'=>$liste->id]) }}" onclick="return(confirm('Voulez-vous supprimer?'))" class="btn btn-sm bg-danger">
                          <i  class="fas fa-trash"></i>
                          </a>
                          <a href="{{route('admin.show',['id'=>$liste->id]) }}" class="btn btn-sm btn-primary">
                            <i class="fas fa-user"></i> Voir Profile
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                
                  <div class="pagination">
                  {{ $listes->links() }}
                  </div> 
                  
                 
                
              
                @endforeach
               
            
          
          
            </div> 
           
          </div>
        </div>
      </section>
</div>
@endsection