@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Confirmation</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">confirmation</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
           <div class="col">
              <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Confirmation</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                </div>
              </div>
              <div class="card-body">
                 <form action="{{ route('utilisateur.update',['id'=>$ref->id] )}}" method="post">
                  {{ csrf_field() }}
                    @if(Session::has('success'))
                      <div class="col-md-6">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Succés! </button> {{Session::get('success')}}
                        </div>
                      </div>
                    @endif

                   
                <div class="row">
                      
                       <div class="col-md-5">
                            <div class="form-group">
                                <p>Nom</p>
                                <input type="text" name="name"  value="{{ $ref->name }}"  class="form-control form-control-sm">
                            
                            </div> 
                        </div>  
                        <div class="col-md-5">     
                            <div class="form-group">
                                <p>Email</p>
                                <input type="text" name="email"  value="{{ $ref->email }}"  class="form-control form-control-sm">
                            
                            </div>  
                        </div>  
                        <div class="col-md-5"> 
                            <div class="form-group">
                                <p>Password</p>
                                <input type="text" name="password"  value="{{ $ref->password }}"  class="form-control form-control-sm">
                            
                            </div>  
                        </div> 
                 </div>
                  
                    <div class="col-12" >
                      <a href="{{route('references.liste')}}" class="btn btn-secondary">Cancel</a>
                      <input type="submit" value="Valider" class="btn btn-success float-right">
                    </div>              
                </div>
                
              </div>
            </div>
                         
           
            </form> 
            </form> 
        </div>  <br>
      </section>    
  
  </div>

@endsection