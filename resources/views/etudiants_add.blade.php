@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Ajout d' un etudiant</div>

                <div class="panel-body">
                   <form action="{{ route('etudiant.store') }}" method="post">
                   {{ csrf_field() }}
                       <div class="form-group">
                           <label for="firstname">Nom</label>
                           <input type="text" name="firstname" id="firstname" class="form-control">
                       </div>
                       
                             

                       
                       <div class="form-group">
                           <label for="lastname">Prenom</label>
                           <input type="text" name="lastname" id="lastname" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="email">Email</label>
                           <input type="text" name="email" id="email" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="phone">Téléphone</label>
                           <input type="phone" name="phone" id="phone" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="birthday">Date d naissance</label>
                           <input type="date" name="birthday" id="birthday" class="form-control">
                       </div>
                       <div class="form-group">
                           <label for="classe">Classe</label>
                           <select name="classes_id" id="classe" class="form-control">
                              @foreach($classes as $classes)
                              <option value="{{ $classes->id }}">{{ $classes->name }}</option>
                              @endforeach
                           </select>
                       </div>
                       <div class="form-group">
                           <center>
                             <button type="submit"  class="btn btn-success pull-center">Enregistrer</button>
                           </center>
                       </div>
                   </form>
                </div>
                  <div class=col-md-2>
                           
                          @if(Session::has('error'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                   <button type="button" class="close" data-dismiss="alert" aria-label="close">
                                      <span aria-hidden="true">&times; </span>
                                      <strong>Erreur!</strong> {{ Session::get('error') }}
                                   </button>  
                                </div>
                          @endif
                       </div>
                <div class="text-center panel-footer">
                
                </div>
            </div>
        </div>
        <div class=col-md-2>
            @if(count($errors) > 0)
                @foreach($errors->all()  as $error)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="close"> 
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong> {{ $error }} </strong>
                                    
                    </div>
                @endforeach
            @endif 
      
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                       <button type="button" class="close" data-dismiss="alert" aria-label="close"> 
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>  succées! {{ Session::get('success') }}</strong>
                    
                </div>
            @endif
        </div>
    </div>
</div>
@endsection