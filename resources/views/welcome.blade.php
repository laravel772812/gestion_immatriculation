@extends('layouts.app')

@section('content')
<div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-gradient-success pb-5">
              <div class="text-muted text-center text-white"><strong>IDENTIFIEZ-VOUS</strong></div>
                <div class="btn-wrapper text-center">
                    <a href="{{route('login')}}" class="btn btn-neutral btn-icon">
                    <span class="btn-inner--icon"><i class="ni ni-key-25 text-info"></i></span>
                    <span class="btn-inner--text">Utilisateur</span>
                    </a>
                    <a href="{{route('admin.login')}}" class="btn btn-neutral btn-icon">
                    <span class="btn-inner--icon"><i class="ni ni-circle-08 text-pink"></i></span>
                    <span class="btn-inner--text">Administrateur</span>
                    </a>
                </div>
            </div>
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Entrer votre email et votre mot de passe</small>
              </div>
                    <form class="form-horizontal" method="POST" action="#">
                        {{ csrf_field() }}
                        <div class="form-group mb-3{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input id="email" disabled="" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                 <input id="password" disabled="" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input disabled="" type="checkbox" id=" customCheckLogin" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }}> 
                            <label class="custom-control-label" for=" customCheckLogin">
                                <span class="text-muted">Remember me</span>
                            </label>
                        </div>
                        <div class="text-center">
                            <button disabled="" type="submit" class="btn btn-primary my-4">Connecter</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row mt-3">
           
          </div>
        </div>
      </div>
    </div>
                               
@endsection
