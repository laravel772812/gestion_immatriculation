@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-gradient-red">
              <h3 class="mb-0">Principaux renseignement sur l'interlocuteur au sein de la DGI</h3>
            </div>
            <div class="card-body">
                 <form action="{{ route('interlocuteurs.adds') }}" method="post" id="form" name="form">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group {{$errors->has('nom_inter') ? 'has-error' : ''}}">
                                      <p>Nom</p>
                                      <input onblur="controlnom(form)" class="form-control form-control-sm {{$errors->has('nom_inter') ? 'is-invalid' : ''}}" type="text" name="nom_inter">
                                   </div>
                                   @if($errors->has('nom_inter'))
                                      <p class="text-danger">{{ $errors->first('nom_inter') }}</p>
                                    @endif
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                       <p>Titre</p>
                                        <input class="form-control form-control-sm" type="text"  name="titre">
                                    </div>
                                  </div> 
                              </div>    
                                
                              <div class="row">    
                                  <div class="col-md-5">
                                    <div class="form-group {{$errors->has('adrs_inter') ? 'has-error' : ''}}">
                                        <p>Adresse</p>
                                        <input onblur="controladrs(form)" class="form-control form-control-sm {{$errors->has('adrs_inter') ? 'is-invalid' : ''}}" type="text"  name="adrs_inter">
                                    </div>
                                    @if($errors->has('adrs_inter'))
                                      <p class="text-danger">{{ $errors->first('adrs_inter') }}</p>
                                    @endif
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group {{$errors->has('tel_inter') ? 'has-error' : ''}}">
                                      <p>Telephone</p>
                                      <input onblur="controletelephone(form)" class="form-control form-control-sm {{$errors->has('tel_inter') ? 'is-invalid' : ''}}" type="text"   name="tel_inter">
                                    </div>
                                    @if($errors->has('tel_inter'))
                                      <p class="text-danger">{{ $errors->first('tel_inter') }}</p>
                                    @endif
                                    <p class="text-danger" id="tel_inter"></p>
                                  </div> 
                              </div>
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Email</p>
                                      <input class="form-control form-control-sm" type="email"  name="email_inter">
                                    </div>
                                
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Nom contribuable</p>
                                      <select class="form-control form-control-sm"  name="pers_phys_id">
                                         <option value="{{ $pers->id }}">{{ $pers->nom_pers_phys }}</option>
                                      </select>
                                    </div>
                                
                                  </div>  
                              </div>   
                              
                            
                              
                              </div> 
                             </div>
                           </div>
                          </div>
                         
                        </div>        
                         
                             
                         
                        <br>      
                        <div class="col-12">
                          
                          <input type="submit" value="Enregistrer" class="btn btn-outline-success float-right">
                        </div>
            </form> 
            </div> 
      </section>  <br>   
  
  </div>
  <script language="javascript">
      function controlnom(form){
          var test = document.form.nom_inter.value;
          var nb = test.length;
          if(nb>=1){
            var maj=test.toUpperCase();
            document.form.nom_inter.value=maj;
          }
        }

     function controletelephone(form){
        var phon=document.form.tel_inter.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs = phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.form.tel_inter.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('tel_inter').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('tel_inter').innerHTML="le numero telephone est invalide";
            
            }
        }else{
          document.getElementById('tel_inter').innerHTML="le numero telephone est invalide";
        
        }
      }
      
        function controladrs(form){
            var str=document.form.adrs_inter.value;
            var x=0;
            var y=1;
            var txt=(str.substring(x,y)).toUpperCase();
            var prnm=txt;
            for(var i=0;i<str.length;i++){
              x=y;
              if(str.charAt(i)==" "){
                
                prnm=prnm+str.substring(y,i)+" ";
                txt=(str.substring(i+1,i+2)).toUpperCase();
                y=i+2;
                prnm=prnm+txt;
              }       
                
            }
                prnm=prnm+str.substring(x,str.length)+" ";
            document.form.adrs_inter.value=prnm;
        }

      
  </script>
@endsection