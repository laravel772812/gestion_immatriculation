@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Associer</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
              <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur les vehicules du contribuable</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
                <div class="card-body">
             
                 <form action="{{ route('vehicules_m.adds') }}" method="post">
                              {{ csrf_field() }}
                            
                             
                              <div class="row">
                                    
                                  <div class="col-md-5">
                                    <div class="form-group {{$errors->has('num_veh_m') ? 'has-error' : ''}}">
                                      <p>Numero vehicule</p>
                                      <input class="form-control form-control-sm {{$errors->has('num_veh_m') ? 'is-invalid' : ''}}" type="text" name="num_veh_m">
                                   </div>
                                    @if($errors->has('num_veh_m'))
                                      <p class="text-danger">{{ $errors->first('num_veh_m') }}</p>
                                    @endif
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                       <p>marque</p>
                                        <input  class="form-control form-control-sm" type="text"  name="marque_m">
                                    </div>
                                  </div> 
                              </div>    
                                
                              <div class="row">    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Genre</p>
                                        <input class="form-control form-control-sm" type="text"  name="genre_m">
                                    </div>
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Type</p>
                                      <input class="form-control form-control-sm" type="text"   name="type_m">
                                    </div>
                                  </div> 
                              </div>
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Puissance</p>
                                      <input class="form-control form-control-sm" type="text"  name="puissance_m">
                                    </div>
                                
                                  </div>  
                                 
                                 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Nombre de place</p>
                                        <input class="form-control form-control-sm" type="text"  name="nbr_place_m">
                                      </div>
                                      @if($errors->has('nbr_place_m'))
                                      <p class="text-danger">{{ $errors->first('nbr_place_m') }}</p>
                                    @endif
                                    </div> 
                                 
                              </div>   
                              <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Charge</p>
                                        <input class="form-control form-control-sm" type="text"  name="charge_m">
                                      </div>
                                      @if($errors->has('charge_m'))
                                       <p class="text-danger">{{ $errors->first('charge_m') }}</p>
                                      @endif
                                  </div> 
                                    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Poid</p>
                                        <input class="form-control form-control-sm" type="text"  name="poid_m">
                                      </div>
                                      @if($errors->has('poid_m'))
                                        <p class="text-danger">{{ $errors->first('poid_m') }}</p>
                                      @endif
                                  </div> 
                              </div>
                              <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Date de mise en circulation</p>
                                        <input class="form-control form-control-sm" type="date"  name="date_circ_m">
                                      </div>
                                  </div> 
                                    
                                  <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                            A usage professionnel &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="usage_proff_m" checked id="radioSuccess1" value="Oui">
                                            <label for="radioSuccess1">Oui</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio"  name="usage_proff_m" id="radioSuccess2" value="Non">
                                            <label for="radioSuccess2">Non</label>
                                          </div>
                                        </div> 
                                  </div><br> 
                              </div>
                              <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Date de début</p>
                                        <input class="form-control form-control-sm" type="date"  name="date_debut">
                                      </div>
                                  </div> 
                                    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Type d'exploitation</p>
                                      <select class="form-control form-control-sm" style="width: 100%;" name="exploitation_m">
                                        <option selected="" value="">--Choisissez dans la liste--</option>
                                        <option>FLUVIAL</option>
                                        <option>DANS TOUTES L'ILE</option>
                                        <option>NATIONAL</option>
                                        <option>REGIONAL</option>
                                        <option>SUBURBAINE</option>
                                        <option>URBAINE</option>
                                      
                                      </select>
                                    </div>  
                              </div>
                              
                                <div class="col-md-5">
                                    <div class="form-group">
                                    
                                      <p>Nom proprietaire du vehicule</p>
                                      <select name="pers_moral_id"  id="pers_moral"  class="form-control form-control-sm">
                                       
                                          <option value="{{ $pers->id }}">{{ $pers->raison_social }} </option>
                                      
                                      </select>
                                      
                                    </div>
                              
                              </div> 
                              
                            </div> 
                          </div>
                        </div>
                         <br>      
                        <div class="col-12">
                          
                          <input type="submit" value="Enregistrer" class="btn btn-outline-success float-right">
                        </div>
            </form> 
          </div> 
      </section>  <br>   
  
  </div>

@endsection