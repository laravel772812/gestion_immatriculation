@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">Associer</li>
                <li class="breadcrumb-item active">Etablissement</li>
                <li class="breadcrumb-item active">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
                <li class="breadcrumb-item active">Interlocuteur</li>
                <li class="breadcrumb-item active">Reference</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Reference d' inscription</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
            <div class="card-body">
                <form action="{{ route('references_m.add') }}" method="post">
                {{ csrf_field() }}
              
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nom personne</p>
                            <select name="pers_moral_id"   id="pers_moral" class="form-control form-control-sm">
                           
                                <option value="{{ $pers->id }}">{{ $pers->raison_social }} </option>
                           
                            </select>
                        </div>      
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nom etablissement</p>
                            <select name="etablissement_m_id"    id="etablissement" class="form-control form-control-sm">
                           
                                <option value="{{ $etabl->id }}">{{ $etabl->nom_etabl_m }} </option>
                            
                            </select>
                        </div>      
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Date reference</p>
                            <input type="date" name="date_ref_m"   class="form-control form-control-sm">
                           
                        </div>      
                    </div>
                    @if(Auth::guard('admin')->check())
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nouveau NIF</p>
                            <input type="text" name="nouveau_nif_m"   class="form-control form-control-sm">
                           
                        </div>      
                    </div>
                    @endif
                  
                   
                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input type="checkbox" id=" customCheckLogin" class="custom-control-input" value="je certifie que ces renseignements sont complets et exacts" name="confirmation_m"> 
                            <label class="custom-control-label" for=" customCheckLogin">
                                <span class="text-muted">je certifie que ces renseignements sont complets et exacts</span>
                            </label>
                        </div>
                   
             
                                    
            </div>
          </div>
        </div>
                         
        <div class="col-12" >
 
            <input type="submit" value="Enregistrer" class="btn btn-success float-right">
        </div>
        </form> 
        </div> 
      </section>  <br>   
  
  </div>

@endsection