@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">associer</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Principaux renseignements sur les associés</small></h1></center>
                </div>
                   
                        <div class="table-responsive">
                            @if(Session::has('success'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('success')}}
                                </div>
                              </div>
                            @endif
                            @if(Session::has('successs'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('successs')}}
                                </div>
                              </div>
                            @endif
                            <table class="table align-items-center table-flush">
                              <thead class="thead-light">
                                <tr>
                                  <th><center>Type associer</center></th>
                                  @if($associer->type_ass == "personne physique")
                                  <th><center>Nom </center></th>
                                  
                                  <th ><center>Fonction</center></th>
                                  <th ><center>CIN</center></th>
                                  <th ><center>Adresse</center></th>
                                  <th ><center>Activite</center></th>
                                  <th ><center>NIF activite</center></th>
                                  <th ><center>Email</center></th>
                                  <th ><center>Tel</center></th>
                                  @endif
                                  @if($associer->type_ass == "Personne morale")
                                  <th ><center>NIF societé</center></th>
                                  @endif
                                  <th ><center>Association unique</center></th>
                                
                                  <th ><center>Action (%)</center></th>
                                 
                               
                                  <th ></th>
                                </tr>
                              </thead>
                              <tbody>
                              
                                  <tr>
                                      <td >{{ $associer->type_ass }}</td>
                                      @if($associer->type_ass == "personne physique")
                                        <td>{{ $associer->nom_ass }}</td>
                                        <td>{{ $associer->fonction_ass }}</td>
                                        <td>{{ $associer->cin_ass }}</td>
                                        <td>{{ $associer->adrs_ass }}</td>
                                        <td>{{ $associer->activite_ass }}</td>
                                        <td>{{ $associer->nif_activite_ass }}</td>
                                        <td>{{ $associer->email_ass }}</td>
                                        <td>{{ $associer->tel_ass }}</td>
                                      @endif 
                                      @if($associer->type_ass == "Personne morale") 
                                        <td>{{ $associer->nif }}</td>
                                      @endif  
                                        <td>{{ $associer->ass_unique }}</td>
                                        <td>{{ $associer->action }}</td>
                                      
                                       
                                      
                                  </tr>

                           
                                
                              
                                  
                              
                              
                              </tbody>
                            </table>
                            
                        </div>
                   
                  <div class="card-footer py-4">
                    <center><a href="{{ route('associer.add') }}" class="btn btn-outline-success">Plus des associers</a></center>
                  </div>
                  <div class="col-12">
                   
                    <a href="{{ route('inscription.etablissements_m') }}" class="btn btn-outline-success float-right">Suivant</a>
                  </div>
              </div>
            </div>
           
     
          </div>
        </div>
      </section>
</div>

@endsection