@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
                <li class="breadcrumb-item active">associer</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Associer de la personne morale</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
                <div class="card-body">
              <script language="javascript">
                   function controlnom(form){
                      var test = document.form.nom_ass.value;
                      var nb = test.length;
                      if(nb>=1){
                        var maj=test.toUpperCase();
                        document.form.nom_ass.value=maj;
                      }
                    }
                    function controlecin(form){
                        var val=document.form.cin_ass.value;
                        if(isNaN(val)==false){
                       
                            var n=val.length;
                            if (n==12){
                                var pc = val.substring(0,1);
                         
                                if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6){
                                    var cs=val.substring(5,6);
                                 
                                    if(cs==1 || cs==2 ){
                                        document.form.cin_ass.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                                    }else{
                                      document.getElementById('cin_asso').innerHTML="CIN invalide à Madagascar ";
                                    }
                                }else{
                                  document.getElementById('cin_asso').innerHTML="CIN invalide à Madagascar ";
                                  
                                }
                            }else{
                                document.getElementById('cin_asso').innerHTML ="votre CIN est incorrect";
                        
                            }
                        } else{
                                document.getElementById('cin_asso').innerHTML ="votre CIN est incorrect";
                        
                            }
                    }
                    function controletelephone1(form){
                      var phon=document.form.tel_ass.value;
                      if(isNaN(phon)==false){
                          var isa=phon.length;
                        
                          if(isa==10 ){
                              var cs=phon.substring(0,3);
                              
                              if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                                  document.form.tel_ass.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                              }else{
                                document.getElementById('tel_asso').innerHTML="Operateur inconnu à Madagascar";
                              }
                          }else{
                            
                            document.getElementById('tel_asso').innerHTML="le numero telephone est invalide";
                          
                          }
                      }else{
                        document.getElementById('tel_asso').innerHTML="le numero telephone est invalide";
                        
                      }
                    }   

                
              </script>
              <script>
                 function pers_phys(){
                   var p=document.getElementById('radioPrimary1');
                   var m=document.getElementById('radioPrimary2');

                   if(p.checked == true){
                    document.getElementById('nom_ass').style.display="block";
                    document.getElementById('fonction_ass').style.display="block";
                    document.getElementById('cin_ass').style.display="block";
                    document.getElementById('adrs_ass').style.display="block";
                    document.getElementById('act_ass').style.display="block";
                    document.getElementById('nif_act').style.display="block";
                    document.getElementById('tel_ass').style.display="block";
                    document.getElementById('nom_ass').style.display="block";
                   
                    document.getElementById('email_ass').style.display="block";
                    document.getElementById('nif').style.display="none";
                  
                   } 
                   if(m.checked == true){
                    document.getElementById('nom_ass').style.display="none";
                    document.getElementById('fonction_ass').style.display="none";
                    document.getElementById('cin_ass').style.display="none";
                    document.getElementById('adrs_ass').style.display="none";
                    document.getElementById('act_ass').style.display="none";
                    document.getElementById('nif_act').style.display="none";
                    document.getElementById('tel_ass').style.display="none";
                    document.getElementById('nom_ass').style.display="none";
                   
                    document.getElementById('email_ass').style.display="none";
                    document.getElementById('nif').style.display="block";
                  
                   } 
                 }
              </script>
                  <form action="{{ route('associer.adds') }}" method="post" id="form" name="form">
                  {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Type d'associer &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="type_ass" checked id="radioPrimary1" value="personne physique" onclick="pers_phys()">
                          <label for="radioPrimary1">Personne physique </label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="type_ass" id="radioPrimary2" value="Personne morale" onclick="pers_phys()">
                          <label for="radioPrimary2">Personne morale</label>
                        </div>
                      
                      </div>  
                  </div>
                  <div class="row">
                    <div class="col-md-5">
                        <div class="form-group {{ $errors->has('nom_ass') ? ' has-error' : '' }}" style="display:none" id="nom_ass">
                            <p>Nom associer *</p>
                            <input  onblur="controlnom()" class="form-control form-control-sm {{ $errors->has('nom_ass') ? ' is-invalid' : '' }}" type="text" name="nom_ass">
                        </div>
                            @if($errors->has('nom_ass'))
                             <p class="text-danger">{{ $errors->first('nom_ass') }}</p>
                            @endif
                      </div> 
                  
             
                 
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="fonction_ass">
                           <p>Fonction associer</p>
                           <select class="form-control"  name="fonction_ass">
							
                             
                                            
                              <option value="MAIRE">MAIRE</option>
                                            
                              <option value="ADJOINT AU MAIRE">ADJOINT AU MAIRE</option>
                                            
                              <option value="PDS">PDS</option>
                                            
                              <option value="CHEF DE REGION">CHEF DE REGION</option>
                                            
                              <option value="CHEF DE DISTRICT">CHEF DE DISTRICT</option>
                                            
                              <option value="PROPRIETAIRE">PROPRIETAIRE</option>
                                            
                              <option value="CHEF D'AGENCE">CHEF D'AGENCE</option>
                                            
                              <option value="GERANT">GERANT</option>
                                            
                              <option value="DG">DG</option>
                                            
                              <option value="PDG">PDG</option>
                                            
                              <option value="CO-GERANT">CO-GERANT</option>
                                            
                              <option value="AUTRE">AUTRE</option>
							
                            </select>
                        </div>
                    
                    </div> 
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="cin_ass">
                            <p>CIN associer</p>
                            <input class="form-control form-control-sm" type="text"  name="cin_ass" onblur="controlecin(form)">
                        </div>
                        <p id="cin_asso" class="text-danger"></p>
                    </div>  
                 
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="adrs_ass">
                            <p>Adresse associer</p>
                            <input class="form-control form-control-sm" type="text"  name="adrs_ass">
                        </div>
                    </div> 
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="act_ass">
                            <p>Activité associer</p>
                            <input class="form-control form-control-sm" type="text" name="activite_ass">
                        </div>
                          
                    </div>  
                
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="nif_act">
                            <p>NIF activité</p>
                            <input class="form-control form-control-sm" type="text" name="nif_activite_ass">
                        </div>
                    </div>  
                    <div class="col-md-5">
                        <div class="form-group" style="display:none" id="tel_ass">
                            <p>Tel associer</p>
                            <input onblur="controletelephone1(form)" class="form-control form-control-sm" type="text" name="tel_ass">
                        </div>
                        <p id="tel_asso" class="text-danger"></p>
                    </div>  
                 
                   
                   
                     
               
                    <div class="col-md-5">
                      <div class="form-group" style="display:none" id="email_ass">
                             <p>Email associer</p>
                              <input class="form-control form-control-sm" type="text" name="email_ass">
                      </div>
                    </div> 
                    
                    <div class="col-md-5">
                      <div class="form-group" style="display:none" id="nif">
                             <p>NIF societé</p>
                              <input class="form-control form-control-sm" type="text" name="nif">
                      </div>
                    </div>
                    <div class="col-md-5">
                      <div class="form-group" style="display:none" id="n">
                            
                      </div>
                    </div>
                    <div class="col-md-5">
                       <div class="form-group">
                              <p>% action</p>
                              <input class="form-control form-control-sm" type="text" name="action">
                        </div>
                    </div> 
                    <div class="col-md-5">
                      <div class="form-group" >
                            <p>Associer unique</p>
                            <select name="ass_unique" class="form-control form-control-sm" >
                                <option>Oui</option>
                                <option>Non</option>
                            </select>
                      </div>
                    </div> 
                  </div>

                </div>
              </div>
            </div>
            <div class="col-12">
                  <button type="reset" class="btn btn-secondary">Annuler</button>
                  <input type="submit" value="Enregistrer" class="btn btn-success float-right">
            </div>
          </form> 
          </div> 
      </section>    
  
  </div>

@endsection