@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
                <li class="breadcrumb-item active">Associer</li>
                <li class="breadcrumb-item active">Etablissement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur l'etablissement de la societé</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                  <form action="{{ route('etablissements_m.adds') }}" method="post" id="ajout" name="ajout">
                      {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Proprietaire etablissement</p>
                              <select name="pers_moral_id"    id="pers_moral" class="form-control form-control-sm">
                                <option value="{{ $pers->id }}">{{ $pers->raison_social }} </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('nom_etabl_m') ? 'has-error' : ''}}">
                              <p>Nom commercial</p>
                              <input  class="form-control form-control-sm  {{ $errors->has('nom_etabl_m') ? 'has-error' : ''}}" type="text" name="nom_etabl_m">
                            </div>
                            @if($errors->has('nom_etabl_m'))
                              <p class="text-danger">{{ $errors->first('nom_etabl_m') }}</p>
                            @endif
                          </div> 
                        </div> 
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group {{ $errors->has('date_ouvert_m') ? 'has-error' : ''}}">
                                <p>Date ouverture</p>
                                <input class="form-control form-control-sm {{ $errors->has('date_ouvert_m') ? 'is-invalid' : ''}}" type="date"  name="date_ouvert_m">
                            </div>
                            @if($errors->has('date_ouvert_m'))
                              <p class="text-danger">{{ $errors->first('date_ouvert_m') }}</p>
                            @endif
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('tel_m') ? 'has-error' : ''}}">
                                <p>Telephone 1</p>
                                <input onblur="controletelephone(ajout)" class="form-control form-control-sm  {{ $errors->has('tel_m') ? 'is-invalid' : ''}}" type="text"  name="tel_m">
                            </div>
                            @if($errors->has('tel_m'))
                              <p class="text-danger">{{ $errors->first('tel_m') }}</p>
                            @endif
                            <p class="text-danger" id="tel_m"></p>
                          </div> 
                        </div> 
                        <div class="row">  
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Autre telephone</p>
                              <input  onblur="controletelephone1(ajout)"  class="form-control form-control-sm" type="text"   name="autre_tel_m">
                            </div>
                            <div class="text-danger" id="autre_tel_m"></div>
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Fax</p>
                                <input class="form-control form-control-sm" type="text"  name="fax_m">
                            </div>
                          </div>  
                        </div> 
                        <div class="row"> 
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('email_m') ? 'has-error' : ''}}">
                              <p>Email</p>
                              <input class="form-control form-control-sm  {{ $errors->has('email_m') ? 'is-invalid' : ''}}" type="email_m"  name="email_m">
                            </div>
                              @if($errors->has('email_m'))
                                <p class="text-danger">{{ $errors->first('email_m') }}</p>
                              @endif
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Exportateur</p>
                              <select class="form-control form-control-sm"  name="exportateur_m">
                                <option>Oui</option>
                                <option>Non</opton>
                              </select>
                            </div>
                          </div> 
                        </div> 
                        <div class="row">           
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Importateur</p>
                              <select class="form-control form-control-sm"  name="importateur_m">
                                <option>Oui</option>
                                <option>Non</opton>
                              </select>
                            </div>
                          </div> 
                      </div>
              </div>
            </div>
            <div class="col">            
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Proprietaire d'etablissement</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <script>
                  function myFunction() {
                      // Get the checkbox
                      var checkBox = document.getElementById("radioSuccess1");
                      var checkBox1 = document.getElementById("radioSuccess2");
                      // Get the output text
                      var text = document.getElementById("soc");

                      // If the checkbox is checked, display the output text
                      if (checkBox.checked == true){
                        par.style.display = "none";
                        soc.style.display = "block";
                      }  
                      if (checkBox1.checked == true){
                        par.style.display = "block";
                        soc.style.display = "none";
                      }  
                    }
                </script>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline" >
                          Type proprietaire  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input  type="radio" onclick="myFunction()" name="type_prop_m" id="radioSuccess1" value="Societé">
                          <label for="radioSuccess1">Societé</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="type_prop_m" checked  id="radioSuccess2" value="Particulier" onclick="myFunction()">
                          <label for="radioSuccess2">Particulier</label>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-5">
                      <div class="form-group " id="soc" style="display:block;" >
                        <p>NIF proprietaire(Societé)</p>
                        <input class="form-control form-control-sm"  type="text" name="nif_prop_m">
                      </div>
                    </div> 
                    </div> 
                    <div id="par" style="display:block" >
                     
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Nom proprietaire</p>
                            <input onBlur="controlnom(ajout)" class="form-control form-control-sm" type="text"  name="nom_prop_m">
                          </div>
                        </div> 
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>CIN proprietaire</p>
                            <input onblur="controlecin_prop(ajout)" class="form-control form-control-sm" type="text"  name="cin_prop_m">
                          </div>
                          <p class="text-danger"id="cin_prop_m"></p>
                        </div> 
                      </div>
                      <div class="row">  
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Adresse proprietaire</p>
                            <input class="form-control form-control-sm" type="text"  name="adrs_prop_m">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Telephone proprietaire</p>
                            <input onBlur="controletelephone_prop(ajout)"class="form-control form-control-sm" type="text"  name="tel_prop_m">
                            <p  class="text-danger" id=tel_prop_m></p>
                          </div>
                        </div>
                      </div> 
                    </div> 
                </div>
              </div>
            </div>
            <div class="col-12">
           
              <input type="submit" value="Enregistrer" class="btn btn-success float-right">
          </div>
          </form> 
         </div> 
        </div> 
      </section><br> 
</div>
  <script>
      function controletelephone(ajout){
        var phon=document.ajout.tel_m.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs=phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.ajout.tel_m.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('tel_m').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('tel_m').innerHTML="le numero telephone est invalide";
            
            }
        }else{
          document.getElementById('tel_m').innerHTML="le numero telephone est invalide";
          
        }
      }
      function controletelephone1(ajout){
        var phon=document.ajout.autre_tel_m.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs=phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.ajout.autre_tel_m.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('autre_tel_m').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('autre_tel_m').innerHTML="le numero telephone est invalide";
            
            }
        }else{
          document.getElementById('autre_tel_m').innerHTML="le numero telephone est invalide";
          
        }
      }
      function controletelephone_prop(ajout){
        var phon=document.ajout.tel_prop_m.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs=phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.ajout.tel_prop_m.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('tel_prop_m').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('tel_prop_m').innerHTML="le numero telephone est invalide";
            
            }
        }else{
          document.getElementById('tel_prop_m').innerHTML="le numero telephone est invalide";
          
        }
      }
      function controlnom(ajout){
      var test = document.ajout.nom_prop_m.value;
      var nb = test.length;
      if(nb>=1){
          var maj=test.toUpperCase();
          document.ajout.nom_prop_m.value=maj;
      }
    }
    function controlecin_prop(ajout){
          var val=document.ajout.cin_prop_m.value;
          if(isNaN(val)==false){
         
              var n=val.length;
              if (n==12){
                  var pc = val.substring(0,1);
                 
                  if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6){
                      var cs=val.substring(5,6);
                 
                      if(cs==1 || cs==2 ){
                          document.ajout.cin_prop_m.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                      }else{
                        document.getElementById('cin_prop_m').innerHTML="votre CIN est invalide";
                      }
                  }else{
                    
                    document.getElementById('cin_prop_m').innerHTML="votre CIN est invalide";
                   
                    
                  }
              }else{
                  document.getElementById('cin_prop_m').innerHTML ="votre CIN  est incorrect";
          
              }
          }else{
            document.getElementById('cin_prop_m').innerHTML="CIN doit être en chiffre ";
          }
      }
    
  </script>
@endsection