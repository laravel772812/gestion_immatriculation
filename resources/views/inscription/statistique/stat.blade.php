@extends('layouts.application')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"><br>
    
  <div  id="carousel">
        <div class="container">
         
          <div class="row justify-content-center">
            <div class="col">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block" src="{{asset('dist/img/car1.jfif')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                   
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car2.jfif')}}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car3.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car4.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
            
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div><br>
   
   
    <section class="content"> 

        <!-- Main content -->
 
      <div class="container-fluid">
        
    
        <!-- Main row -->
        <div class="row">
          <!-- Donut Chart -->
          <div class="col-md-6">
            <div class="card card-dark">
                <div class="card-header">
                    <h3 class="card-title">Statistique des personnes morales par Mois</h3>

                    <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                  
                    </div>
                </div>
                <div class="card-body">
                    <canvas id="chartMorales" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                
              <!-- /.card-body -->
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-dark">
              <div class="card-header">
                <h3 class="card-title">Statistique des personnes physique par Mois</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="myAreaChart"style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
          </div>

          <div class="col-md-6">
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">Statistique d'inscription</h3>

                    <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                  
                    </div>
                </div>
                <div class="card-body">
                    <canvas id="chartMoral" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <div class="mt-4 text-center small">
                  <span class="mr-2">
                    <i class="fas fa-circle text-primary"></i> Personne morale
                  </span>
                  <span class="mr-2">
                    <i class="fas fa-circle text-success"></i> Personne physique
                  </span>
                  <span class="mr-2">
                    <i class="fas fa-circle text-info"></i> Administrateur
                  </span>
                  <span class="mr-2">
                    <i class="fas fa-circle text-orange"></i> Utilisateur
                  </span>
                </div>
              <!-- /.card-body -->
            </div>
          </div>
          
         <!-- /.right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
       
    <!-- /.content -->
  </div>
    <script src="{{url( 'vendor1/jquery.min.js' )}}"></script>

    <script src="{{url( 'vendor1/Chart.min.js' )}}"></script>

    <script src="{{url( 'vendor1/create-charts.js' )}}"></script>
    <script src="{{url( 'vendor1/charts-morals.js' )}}"></script>
    <script src="{{url( 'vendor1/morals.js' )}}"></script>
  <!-- /.content-wrapper -->
@endsection
