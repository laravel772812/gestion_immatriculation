@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item active">Dirigeant</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Renseignement sur les dirigeans de la societé</small></h1></center>
                  </div>
              @if(Auth::guard('admin')->check())
                <div class="table-responsive">
                
                  <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th >Nom</th>
                        <th >Fonction</th>
                        <th >CIN</th>
                        <th >Adresse</th>
                        <th >Email</th>
                        <th >Tel</th>
                        <th >Activité</th>
                        <th >NIF activité</th>
                        <th >Id Etablissement</th>
                        <th ></th>
                      </tr>
                    </thead>
                    <tbody>
                    
                  @foreach ($dirigeants as $dirigeant)
                
                        <tr scope="row">
                              <td >{{ $dirigeant->nom_dir_m }}</td>
                              <td >{{ $dirigeant->fonction_m }}</td>
                              <td >{{ $dirigeant->cin_dir_m }}</td>
                              <td >{{ $dirigeant->adrs_dir_m }}</td>
                              <td >{{ $dirigeant->email_dir_m }}</td>
                              <td >{{ $dirigeant->tel_dir_m }}</td>
                              <td >{{ $dirigeant->activite_dir_m }}</td>
                              <td >{{ $dirigeant->nif_activite_dir_m}}</td>
                              <td >{{ $dirigeant->etablissement_m_id}}</td>
                              <td class="text-right">
                                <a class="btn btn-danger btn-sm"  href="{{route('dirigeants_m.delete',['id'=>$dirigeant->id]) }}"><i  class="fas fa-trash"></i></a>
                              </td>
                        </tr>

                  @endforeach
                      
                    
                        
                        
                  </tbody>
                </table>
                
              </div>
              @endif
              <div class="card-footer py-4">
                  <center><a href="{{ route('dirigeants_m.add') }}" class="btn btn-outline-success">Plus des dirigeants</a></center>
              </div>
              </div>
            </div>
          </div>
        </div>
      </section>
</div>

@endsection