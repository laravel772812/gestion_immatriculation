@extends('layouts.dash')

@section('content')
 <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Icons</a>
      
       
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-white pb-4 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 300px; background-image: url({{asset('plug/img/theme/republik.jpg')}}); background-repeat:no-repeat; background-position: center top;">
       

      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                  <div class="card card-stats mb-4 mb-xl-0 bg-gradient-success" >
                    <div class="card-body">
                      <div class="row">
                        <div class="col">
                          <div class="typography-line">
                            <h1><span>D</span><small style="color:#056839;">irection</small> <span>G</span><small style="color:#056839;">énérale des</small> <span>I</span><small style="color:#056839;">mpôts</small> </h1>
                            <small>Vers une administration fiscale innovante, transparente et pilier de l'emergence</small>
                          </div>
                    
                        </div>  
                      </div>    
                    </div> 
                    </div>  </div>   
                  </div> <br>
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-md-12">
              <div class="card card-stats mb-4 mb-xl-0 bg-coral" >
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                       
                    <ul class="nav nav-tabs">
                      <li class="nav-item">
                        <a class="nav-link " href="#" >Principaux renseignement</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active"  href="#" >Activité</a>
                      </li>
                    
                      <li class="nav-item">
                        <a class="nav-link " href="#etablissement">Etablissement</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#dirigeant">Dirigeant</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#vehicule">Vehicule</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#interlocuteur">Interlocuteur</a>
                      </li>
                    </ul>
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div><br><br><br>

    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Icons</h3>
            </div>
            <div class="card-body">
                         <form action="{{ route('inscription.activites_adds') }}" method="post">
                            {{ csrf_field() }}
                              <div class="row">
                   
                                  <div class="col-md-5">
                                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                      <p>Description d' activités</p>
                                      <textarea  class="form-control input-sm {{ $errors->has('description') ? 'is-invalid' : '' }}"  name="description" data-toggle="tooltip" data-original-title="Lister par ordre d'importance toutes vos activités"></textarea>
                                    </div>
                                    @if($errors->has('description'))
                                      <p class="text-danger">{{ $errors->first('description') }}</p>
                                    @endif
                                  </div>  
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Precision d'activité</p>
                                      <textarea class="form-control input-sm"  name="precision" data-toggle="tooltip" data-placement="right" data-original-title="Si Transporteur, lister ici les numéros des moyens de transport. Si Importateur, Exportateur, Collecteur (ou autres), énumérer les principaux produits. Pour les titulaires de licence de boissons alcooliques, mentionner la catégorie, la référence et la date de décision."></textarea>
                                    </div>
                                   
                                  </div> 
                              </div>    
                                
                               <div class="row">    
                               
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                    <p>Registre du commerce</p>
                                      <input type="text" class="form-control form-control-sm"  name="reg_comm">
                                    </div>
                                 </div>  
                                 <div class="col-md-5">
                                    <div class="form-group">
                                    <p>Date de registre du commerce</p>
                                      <input type="date" class="form-control form-control-sm"  name="date_reg_comm">
                                    </div>
                                 </div>  
                                </div> 
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('debut') ? ' has-error' : '' }}">
                                        <p>Début de l'exercice comptable</p>
                                          <input type="text" class="form-control form-control-sm {{ $errors->has('debut') ? 'is-invalid' : '' }}"  name="debut" value="01/01"  data-toggle="tooltip"  data-original-title="Changer cette valeur par défaut, si votre exercice ne débute pas le 1er Janvier.">
                                        </div>
                                        @if($errors->has('debut'))
                                          <p class="text-danger">{{ $errors->first('debut') }}</p>
                                        @endif
                                    </div>  
                                    <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('cloture') ? 'has-error' : '' }}">
                                        <p>Clôture de l'exercice comptable</p>
                                          <input type="text" class="form-control form-control-sm {{ $errors->has('cloture') ? 'is-invalid' : '' }}"  name="cloture" value="31/12">
                                        </div>
                                        @if($errors->has('cloture'))
                                          <p class="text-danger">{{ $errors->first('cloture') }}</p>
                                        @endif
                                    </div> 
                                 </div>  
                                  <div class="row">  
                                      <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                             Importateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="import" checked id="radioSuccess1" value="Oui">
                                            <label for="radioSuccess1">Oui</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio" name="import" id="radioSuccess2" value="Non">
                                            <label for="radioSuccess2">Non</label>
                                          </div>
                                          @if($errors->has('import'))
                                            <p class="text-danger">{{ $errors->first('import') }}</p>
                                          @endif
                                        </div> 
                                       </div><br> 
                                       <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                             Exportateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="export" checked id="radioSuccess1" value="Oui">
                                            <label for="radioSuccess1">Oui</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio" name="export" id="radioSuccess2" value="Non">
                                            <label for="radioSuccess2">Non</label>
                                          </div>
                                          @if($errors->has('exportateur'))
                                            <p class="text-danger">{{ $errors->first('exportateur') }}</p>
                                          @endif
                                        </div> 
                                       </div>
                                  </div>     <br> 
                                  <div class="row">
                                        <div class="col-md-5">
                                          <div class="form-group  {{ $errors->has('nbr_salarier') ? ' has-error' : '' }}">
                                            <p>Nombre de salarié</p>
                                            <input type="text" class="form-control form-control-sm  {{ $errors->has('nbr_salarier') ? ' is-invalid' : '' }}" value="{{old('nbr_salarier')}}"  name="nbr_salarier" >
                                          </div>
                                          @if($errors->has('nbr_salarier'))
                                            <p class="text-danger">{{ $errors->first('nbr_salarier') }}</p>
                                          @endif
                                        </div>
                                  
                                  </div>
                              </div>
                          </div><br>
                         
                          <div class="col-12">
                                <a href="{{ route('inscription.pers') }}" class="btn btn-secondary">Cancel</a>
                                <input type="submit" value="Enregistrer" class="btn btn-success float-right">
                          </div>
                      </form> 
            
      <!-- Footer -->
     
     @include('layouts/footer')
    </div>
  </div>

@endsection