@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">Etablissement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Principaux renseignements sur l'établissement</small></h1></center>
                  </div>
                          @if(Session::has('success'))
                            <div class="col-md-6">
                              <div class="alert alert-success alert-dismissible fade show" role="alert">
                                
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                                  <strong>Succés! </button> {{Session::get('success')}}
                              </div>
                            </div>
                          @endif
                @if(Auth::guard('admin')->check())
                          
                  <div class="table-responsive">
                    @if(Session::has('success'))
                      <div class="col-md-6">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Succés! </button> {{Session::get('success')}}
                        </div>
                      </div>
                    @endif
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                        <tr>
                          <th style="padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;white-space: nowrap;"><center>id personne</center></th>
                          <th style="padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;" ><center>Nom commercial</center></th>
                          
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Date ouverture</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Telephone 1</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Autre telephone</center></th>
                          <th style="    padding-top: 0.75rem; padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Fax</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Email</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Exportateur</center></th>
                          <th style="    padding-top: 0.75rem; padding-bottom: 0.75rem; font-size: 0.65rem; text-transform: uppercase;letter-spacing: 1px; border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Importateur</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Type proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>NIF proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Nom proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>CIN proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Adresse proprietaire</center></th>
                          <th style="    padding-top: 0.75rem; padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Adresse proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center>Telephone proprietaire</center></th>
                          <th style="    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;    white-space: nowrap;"><center></th>
                        </tr>
                      </thead>
                      <tbody>
                      
                    @foreach ($etablissements as $etablissement)
                  
                          <tr>
                          <td >{{ $etablissement->pers_moral_id }}</td>
                                <td>{{ $etablissement->nom_etabl_m }}</td>
                                
                                <td >{{ $etablissement->date_ouvert_m }}</td>
                                <td >{{ $etablissement->tel_m }}</td>
                                <td >{{ $etablissement->autre_tel_m }}</td>
                                <td >{{ $etablissement->fax_m }}</td>
                                <td >{{ $etablissement->email_m }}</td>
                                <td >{{ $etablissement->exportateur_m }}</td>
                                <td >{{ $etablissement->importateur_m }}</td>
                                <td >{{ $etablissement->type_prop_m }}</td>
                                <td >{{ $etablissement->nif_prop_m }}</td>
                                <td >{{ $etablissement->nom_prop_m }}</td>
                                <td >{{ $etablissement->cin_prop_m }}</td>
                                <td >{{ $etablissement->adrs_prop_m }}</td>
                                <td >{{ $etablissement->tel_prop_m }}</td>
                              
                              
                                <td class="text-right">
                                  <a class="btn btn-danger btn-sm"  href="{{route('etablissements_m.delete',['id'=>$etablissement->id]) }}"><i  class="fas fa-trash"></i></a>
                                </td>
                              
                          </tr>

                    @endforeach
                        
                      
                          
                      
                      
                      </tbody>
                    </table>
                    
                  </div>
                @endif  
                <div class="card-footer py-4">
                  <center><a href="{{ route('etablissements_m.add') }}" class="btn btn-outline-success">Ajouter des etablissements</a></center>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
</div>

@endsection