@extends('layouts.dash')

@section('content')
 <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Icons</a>
      
       
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-white pb-4 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 300px; background-image: url({{asset('plug/img/theme/republik.jpg')}}); background-repeat:no-repeat; background-position: center top;">
       

      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                  <div class="card card-stats mb-4 mb-xl-0 bg-gradient-success" >
                    <div class="card-body">
                      <div class="row">
                        <div class="col">
                          <div class="typography-line">
                            <h1><span>D</span><small style="color:#056839;">irection</small> <span>G</span><small style="color:#056839;">énérale des</small> <span>I</span><small style="color:#056839;">mpôts</small> </h1>
                            <small>Vers une administration fiscale innovante, transparente et pilier de l'emergence</small>
                          </div>
                    
                        </div>  
                      </div>    
                    </div> 
                    </div>  </div>   
                  </div> <br>
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-md-12">
              <div class="card card-stats mb-4 mb-xl-0 bg-coral" >
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                       
                    <ul class="nav nav-tabs">
                      <li class="nav-item">
                        <a class="nav-link" href="#" >Principaux renseignement</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link active"  href="#" >Activité</a>
                      </li>
                    
                      <li class="nav-item">
                        <a class="nav-link " href="#etablissement">Etablissement</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#dirigeant">Dirigeant</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#vehicule">Vehicule</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " href="#interlocuteur">Interlocuteur</a>
                      </li>
                    </ul>
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>

    <!-- Header -->
   
      <div class="container-fluid">
        
          <div class="row">
            <div class="col">
             
            
             
  
          <div class="card shadow">
            <div class="card-header border-0">
            <div class="card-header bg-gradient-grey ">
                <h1 class="mb-0" style="color:green;"><small>Les activités  du contribuable</small></h1>
            </div>
            </div>
            @if(Auth::guard('admin')->check())
              <div class="table-responsive">
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">Activite</th>
                      <th scope="col">Precision sur les activités</th>
                      <th scope="col">Registre de commerce</th>
                      <th scope="col">Date de registre de commerce</th>
                      <th scope="col">Début de l'exercice comptable</th>
                      <th scope="col">Clôture de l'exercice comptable</th>
                      <th scope="col">Importateur</th>
                      <th scope="col">exportateur</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                  
                @foreach ($activites as $activite)
              
                      <tr scope="row">
                            <td>{{ $activite->description }}</td>
                            <td>{{ $activite->precision }}</td>
                            <td>{{ $activite->reg_comm }}</td>
                            <td>{{ $activite->date_reg_comm }}</td>
                            <td>{{ $activite->debut }}</td>
                            <td>{{ $activite->cloture }}</td>
                            <td>{{ $activite->import }}</td>
                            <td>{{ $activite->export}}</td>
                            
                            <td class="text-right">
                              <div class="dropdown">
                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="fas fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(32px, 32px, 0px);">
                                  <a class="dropdown-item" href="#"> <i class="fas fa-pencil-alt"></i>Edit</a>
                                  <a class="dropdown-item" href="#"><i class="fas fa-trash"></i>Delete</a>
                                
                                </div>
                              </div>
                            </td>
                      </tr>

                @endforeach
                    
                  
                      
                  
                  
                  </tbody>
                </table>
                
              </div>
            @endif  
            <div class="card-footer py-4">
              <center><a href="{{ route('activites.add') }}" class="btn btn-outline-success">Ajouter des activites</a></center>
            </div>
          </div>
        </div>
      </div><br>
      <div class="col-12">
            <a href="{{ route('inscription.persPhys') }}" class="btn btn-secondary">Cancel</a>
            <a href="{{ route('inscription.etablissements') }}" class="btn btn-outline-success float-right">Suivant</a>
      </div>
      <!-- Footer -->
     @include('layouts/footer')
    </div>
  </div>

@endsection