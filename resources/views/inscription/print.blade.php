<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Espace contribuable</title>
  <link href="{{asset('dist/img/logo-mfb.png')}}" rel="icon" type="image/png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 5 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,500,500i,700" rel="stylesheet">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section  class="col-lg-2 connectedSortable"></section>
          <section class="col-lg-9 connectedSortable">
           <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <center> <h3 class="mb-0" ><small>REPOBLIKAN'I MADAGASIKARA</small></h3>
                <h5 class="mb-0" ><small>Fitiavana - Tanindrazana - Fandrosoana</small></h5> 
                <h5 class="mb-0" ><small>----------------</small></h5></center>

                <h5 class="mb-0" ><small>MINISTERE DES FINANCES ET DU BUDGET</small></h5>
                <h5 class="mb-0" ><small>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp----------------</small></h5>
                <h5 class="mb-0" ><small>&nbsp&nbsp&nbsp&nbsp DIRECTION GENERALE DES IMPOTS</small></h5> 
                <h5 class="mb-0" ><small> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp----------------</small></h5>
                    
                <br> <br>  <center><h1class="mb-0" ><strong>REFERENCE DE DEMANDE D'IMMATRICULATION FISCALE</strong></h1> </center><br> <br>
            <div style="margin-left:100px;"> 
                 <!-- Table row -->
              @foreach ($listes as $liste)
                <p>Id reference  : {{Request::get('a') }}</p>
                <p>Nom  : {{ $liste->nom_pers_phys }}</p>
                <p>Prenom  : {{ $liste->prenom_pers_phys }}</p>
                <p>Situation matrimonial  : {{ $liste->sit_mat}}</p>
                <p>Sexe  : {{ $liste->sexe }}</p>
                <p>CIN : {{ $liste->cin }}</p>
               
            
                <p>Date de naissance  : {{ $liste->date_naiss }}</p>
                <p>Lieu de naissance  : {{ $liste->lieu_naiss }}</p>
                <p>Adresse actuel  : {{ $liste->adrs }}</p>
                <p>NIF  : En attente</p>
                <p class="text-right">{{ $liste->confirmation }}</p>
               
              @endforeach
            </div> 
             <!-- /.row -->

              <div class="row">
              
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              
            </div>
            <!-- /.invoice -->
          </section><!-- /.col -->
          <section  class="col-lg-2 connectedSortable"></section>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>
