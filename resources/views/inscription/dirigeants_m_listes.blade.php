@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">Associer</li>
                <li class="breadcrumb-item active">Etablissement</li>
                <li class="breadcrumb-item active">Dirigeant</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Principaux renseignements sur les dirigeants de la societé</small></h1></center>
                </div>
     
              <div class="table-responsive">
                            @if(Session::has('successs'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('successs')}}
                                </div>
                              </div>
                            @endif
                <table class="table align-items-center table-flush">
                    <thead class="thead-light">
                      <tr>
                        <th >Nom</th>
                        <th >Fonction</th>
                        <th >CIN</th>
                        <th >Adresse</th>
                        <th >Email</th>
                        <th >Tel</th>
                        <th >Autre activité</th>
                        @if($dirigeant->autre_act == "Oui")
                        <th >Activité</th>
                     
                        <th >NIF activité</th>
                        @endif
                        <th >Id Etablissement</th>
                        <th ></th>
                      </tr>
                    </thead>
                    <tbody>
                    
                  
                
                        <tr scope="row">
                              <td>{{ $dirigeant->nom_dir_m }}</td>
                              <td>{{ $dirigeant->fonction_m }}</td>
                              <td>{{ $dirigeant->cin_dir_m }}</td>
                              <td>{{ $dirigeant->adrs_dir_m }}</td>
                              <td>{{ $dirigeant->email_dir_m }}</td>
                              <td>{{ $dirigeant->tel_m }}</td>
                              <td>{{ $dirigeant->autre_act }}</td>
                              @if($dirigeant->autre_act == "Oui")
                              <td>{{ $dirigeant->activite_dir_m }}</td>
                              <td>{{ $dirigeant->nif_activite_dir_m}}</td>
                              @endif
                              <td>{{ $dirigeant->etablissement_m_id}}</td>
                            
                        </tr>

                   
                      
                    
                        
                        
                  </tbody>
                </table>
                
              </div>
            
              <div class="card-footer py-4">
                  <center><a href="{{ route('dirigeants_m.add') }}" class="btn btn-outline-success">Plus des dirigeants</a></center>
              </div>
              <div class="col-12">
                
                  <a href="{{ route('inscription.vehicules_m') }}" class="btn btn-outline-success float-right">Suivant</a>
              </div>
            </div> 
          </div>
       
     </div>
  </section>
</div>

@endsection