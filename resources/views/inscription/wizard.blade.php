@extends('layouts.appl')

@section('content')


<!-- page content -->
<div class="right_col" role="main">
<div class="">
    <div class="page-title">
    <div class="title_left">
        <h3>Form Wizards</h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
        </div>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
        <div class="x_title">
            <h2>Form Wizards <small>Sessions</small></h2>
            <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                <li><a href="#">Settings 1</a>
                </li>
                <li><a href="#">Settings 2</a>
                </li>
                </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">



           <!--      Wizard container        -->
           <div class="wizard-container">

<div class="card wizard-card" data-color="orange" id="wizardProfile">
    <script language="javascript">
          function controlnom(form){
            var test = document.form.nom_pers_phys.value;
            var nb = test.length;
            if(nb>=1){
              var maj=test.toUpperCase();
              document.form.nom_pers_phys.value=maj;
            }
          }

          function controlprenom(){
              var str=document.form.prenom_pers_phys.value;
              var x=0;
              var y=1;
              var txt=(str.substring(x,y)).toUpperCase();
              var prnm=txt;
              for(var i=0;i<str.length;i++){
                x=y;
                if(str.charAt(i)==" "){
                  
                  prnm=prnm+str.substring(y,i)+" ";
                  txt=(str.substring(i+1,i+2)).toUpperCase();
                  y=i+2;
                  prnm=prnm+txt;
                }       
                  
              }
                  prnm=prnm+str.substring(x,str.length)+" ";
              document.form.prenom_pers_phys.value=prnm;
          }
          
          function controlLieu_naiss(){
              var str=document.form.lieu_naiss.value;
              var x=0;
              var y=1;
              var txt=(str.substring(x,y)).toUpperCase();
              var prnm=txt;
              for(var i=0;i<str.length;i++){
                x=y;
                if(str.charAt(i)==" "){
                  
                  prnm=prnm+str.substring(y,i)+" ";
                  txt=(str.substring(i+1,i+2)).toUpperCase();
                  y=i+2;
                  prnm=prnm+txt;
                }       
                  
              }
                  prnm=prnm+str.substring(x,str.length)+" ";
              document.form.lieu_naiss.value=prnm;
          }

          function controlecin(form){
              var val=document.form.cin.value;
              if(isNaN(val)==false){
                document.getElementById('cin').innerHTML="";
                  var n=val.length;
                  if (n==12 || n==""){
                      var pc = val.substring(0,1);
                      document.getElementById('cin').innerHTML="";
                      if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6 || pc==""){
                          var cs=val.substring(5,6);
                          document.getElementById('cin').innerHTML="";
                          if(cs==1 || cs==2 ){
                              document.form.cin.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                          }else{
                            document.getElementById('cin').innerHTML="";
                          }
                      }else{
                        document.getElementById('cin').innerHTML="CIN invalide à Madagascar ";
                        
                      }
                  }else{
                      document.getElementById('cin').innerHTML ="votre CIN est incorrect";
              
                  }
              }else{
                document.getElementById('cin').innerHTML=" ";
              }
          }
        
          function controleNum_stat(form){
              var val=document.form.num_stat.value;
              if(isNaN(val)==false){
                document.getElementById('num_stat').innerHTML="";
                  var n=val.length;
                  if (n!=17){
                    document.getElementById('num_stat').innerHTML="Numéro statistique invalide";
                  } else {
                      document.form.num_stat.value=val.substring(0,5) + " " + val.substring(5,7) + " " + val.substring(7,11) + " " + val.substring(11,12)  + " " + val.substring(12,17);
                    
                  }  
              }
          }
          
          function controlAdresse(){
              var str=document.form.adrs.value;
              var x=0;
              var y=1;
              var txt=(str.substring(x,y)).toUpperCase();
              var prnm=txt;
              for(var i=0;i<str.length;i++){
                x=y;
                if(str.charAt(i)==" "){
                  
                  prnm=prnm+str.substring(y,i)+" ";
                  txt=(str.substring(i+1,i+2)).toUpperCase();
                  y=i+2;
                  prnm=prnm+txt;
                }       
                  
              }
                  prnm=prnm+str.substring(x,str.length)+" ";
              document.form.adrs.value=prnm;
          }

          function controleRib(form){
              var val=document.form.rib.value;
              if(isNaN(val)==false){
                
                  var n=val.length;
                  if (n==23){
                      document.form.rib.value=val.substring(0,5) + "-" + val.substring(5,10) + "-" + val.substring(10,21) + "-" + val.substring(21,23);
                    
                  }else {
                    document.form.rib.value=""
                  }
              }else{
                document.getElementById('rib').innerHTML ="RIB doit être en chiffre";
              
              }
              
          }
    </script>
    <form action="{{ route('inscription.persPhys') }}" method="post" id="form" name="form">
    {{ csrf_field() }}
<!--        You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red"          -->

        <div class="wizard-header">
            <h3>
               <b>BUILD</b> YOUR PROFILE <br>
               <small>This information will let us know more about you.</small>
            </h3>
        </div>

        <div class="wizard-navigation">
            <ul>
                <li><a href="#about" data-toggle="tab">Principaux renseignement</a></li>
                <li><a href="#account" data-toggle="tab">Activité</a></li>
                <li><a href="#address" data-toggle="tab">siège</a></li>
            
               
            </ul>

        </div>

        <div class="tab-content">
       
            <div class="tab-pane" id="about">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group {{ $errors->has('nom_pers_phys') ? ' has-error' : '' }}">
                                <p>Nom *</p>
                                <input onBlur="controlnom(form)" class="form-control form-control-sm {{ $errors->has('nom_pers_phys') ? ' is-invalid' : '' }}" value="" type="text" name="nom_pers_phys">
                            </div>
                            @if($errors->has('nom_pers_phys'))
                                <p class="text-danger">{{ $errors->first('nom_pers_phys') }}</p>
                            @endif
                        </div>  
                            
                        <div class="col-md-5">
                            <div class="form-group">
                             <p>Prenom *</p>
                              <input  onBlur="controlprenom()" class="form-control form-control-sm" type="text"  name="prenom_pers_phys">
                            </div>
                        
                        </div> 
                    </div> 

                    <div class="row">    
                      <div class="col-md-5">
                        <div class="form-group  {{ $errors->has('nom_pers_phys') ? ' has-error' : '' }}">
                          <p>Situation matrimoniale *</p>
                          <select class="form-control form-control-sm  {{ $errors->has('nom_pers_phys') ? 'is-invalid' : '' }}" style="width: 100%;" name="sit_mat">
                            <option class="select_first_option" value="" selected="">
                                     -- Choisissez dans la liste --
                             </option>
                            <option>MARIE(E)</option>
                            <option>CELIBATAIRE</option>
                            <option>DIVORCE(E)</option>
                            <option>VEUF(VE)</option>
                            <option>INCONNU</option>
                          
                          </select>
                        </div>  
                        @if($errors->has('sit_mat'))
                         <p class="text-danger">{{ $errors->first('sit_mat') }}</p>
                        @endif
                      </div>   
                      <div class="col-md-5">
                        <div class="form-group {{ $errors->has('date_naiss') ? ' has-error' : '' }}">
                          <p>Date de naissance *</p>
                          <input class="form-control form-control-sm {{ $errors->has('date_naiss') ? ' is-invalid' : '' }}" value="{{ old('date_naiss') }}" type="date" id="date_naiss"  name="date_naiss">
                        </div>
                        @if($errors->has('date_naiss'))
                          <p class="text-danger">{{ $errors->first('date_naiss') }}</p>
                        @endif
                      </div>   
                    </div>   

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group {{ $errors->has('lieu_naiss') ? ' has-error' : '' }}">
                            <p>Lieu de naissance *</p>
                            <input onBlur="controlLieu_naiss()" class="form-control form-control-sm {{ $errors->has('lieu_naiss') ? ' is-invalid' : '' }}" value="{{ old('lieu_naiss') }}" type="text" id="lieu_naiss" name="lieu_naiss">
                            </div>
                            @if($errors->has('lieu_naiss'))
                            <p class="text-danger">{{ $errors->first('lieu_naiss') }}</p>
                            @endif
                        </div>  
                    
                        
                        <div class="col-md-5">
                            <div class="form-group clearfix"> <br> 
                                
                                    Sexe *  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="sexe" checked id="radioSuccess1" value="Masculin">
                                    <label for="radioSuccess1">Masculin</label>
                               
                                    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp 
                                    <input type="radio" name="sexe" id="radioSuccess2" value="Feminin">
                                    <label for="radioSuccess2">Feminin</label>
                               
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group {{ $errors->has('cin') ? ' has-error' : '' }}">
                                <p>CIN *</p>
                                <input onBlur="controlecin(form)" class="form-control form-control-sm {{ $errors->has('cin') ? ' is-invalid' : '' }}" value="{{ old('cin') }}" type="text" name="cin">
                            </div>
                            @if($errors->has('cin'))
                            <p class="text-danger">{{ $errors->first('cin') }}</p>
                            @endif
                            <p class="text-danger" id="cin"></p>
                        </div> 
                        <div class="col-md-5">
                            <div class="form-group {{ $errors->has('date_del_cin') ? ' has-error' : '' }}">
                                <p>Date de delivrance cin *</p>
                                <input class="form-control form-control-sm {{ $errors->has('date_del_cin') ? ' is-invalid' : '' }}" value="{{ old('date_del_cin') }}" type="date" name="date_del_cin">
                            </div>
                            @if($errors->has('date_del_cin'))
                            <p class="text-danger">{{ $errors->first('date_del_cin') }}</p>
                            @endif 
                        </div> 
                    </div> 
                    <div class="row"> 
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('lieu_del_cin') ? ' has-error' : '' }}">
                      <p>Lieu de delivrance cin *</p>
                      <input class="form-control form-control-sm {{ $errors->has('lieu_del_cin') ? ' is-invalid' : '' }}" value="{{ old('lieu_del_cin') }}" type="text"  name="lieu_del_cin">
                    </div>
                    @if($errors->has('lieu_del_cin'))
                      <p class="text-danger">{{ $errors->first('lieu_del_cin') }}</p>
                    @endif 
                  </div> 
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('num_stat') ? ' has-error' : '' }}">
                    <p>Numero statistique *</p>
                    <input onBlur="controleNum_stat(form)" type="text" class="form-control form-control-sm {{ $errors->has('num_stat') ? ' is-invalid' : '' }}"  name="num_stat" value="55555 55 5555 5 55555" data-toggle="tooltip"  data-original-title="Si votre statistique est en attente, veuillez mettre 555555555555555555."  >
                    </div>  
                    @if($errors->has('num_stat'))
                      <p class="text-danger">{{ $errors->first('num_stat') }}</p>
                    @endif
                    <p class="text-danger" id="num_stat"></p>
                  </div>  
                  </div>  

                  <div class="row">   
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Date de delivrance statistique</p>
                            <input type="date" class="form-control form-control-sm"  name="date_deliv_stat">
                        </div> 
                    </div> 
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Regime fiscal *</p>
                                <select class="form-control form-control-sm" style="width: 100%;" name="reg_fisc">
                                    <option class="select_first_option" value="" selected="">
                                            -- Choisissez dans la liste --
                                    </option>
                                    <option>DROIT COMMUN</option>

                                    <option>ZONE FRANCHE</option>

                                    <option>CODE DES INVESTISSEMENTS</option>

                                    <option>LGIM</option>

                                    <option>CONVENTION D'ETABLISSEMENT</option>

                                    <option>AUTRES</option>
                                
                                </select>
                        </div>
                    </div> 
                    </div> 
                    <div class="row"> 
                        <div class="col-md-5">
                            <div class="form-group">
                                <p>date agrément</p>
                                <input class="form-control form-control-sm" type="date" id="date_agre" name="date_agre">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <p>Ref agrément</p>
                                <input class="form-control form-control-sm" type="text" id="ref_agre" name="ref_agre">
                            </div>
                        </div>
                  </div> 
                  <div class="row" >
                        <div class="col-md-5">
                            <div class="form-group">
                                <p>Periode de grace *</p>
                                <input class="form-control form-control-sm" value="0" type="text" id="peri_grace" name="peri_grace">
                            </div>
                        </div> 
                        <div class="col-md-5">
                            <div class="form-group">
                                <p>Date de creation *</p>
                                <input class="form-control form-control-sm" type="date" id="date_creation" name="date_creation">
                            </div>
                        </div> 
                </div> 
                <div class="row"> 
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Capital en Ar *</p>
                            <input class="form-control form-control-sm" type="text" id="capital" name="capital">
                        </div>
                    </div>
                     <div class="col-md-5">
                        <div class="form-group  {{ $errors->has('rib') ? ' has-error' : '' }}">
                            <p>RIB *</p>
                            
                            <input onBlur="controleRib(form)"  data-toggle="tooltip" data-placement="bottom" data-original-title="Numéro de compte bancaire. Obligatoire pour les personnes morales ou les assujettis à la TVA." class="form-control form-control-sm  {{ $errors->has('rib') ? ' is-invalid' : '' }}" type="text"  name="rib">
                        </div>
                    
                        <p class="text-danger" id="rib"></p>
                    
                    </div> 
                </div> 

                  
            </div>
            <div class="tab-pane" id="account">
               
               <div class="row">
                    <div class="col-md-5">
                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                            <p>Description d' activités</p>
                            <textarea  class="form-control input-sm {{ $errors->has('description') ? 'is-invalid' : '' }}"  name="description" data-toggle="tooltip" data-original-title="Lister par ordre d'importance toutes vos activités"></textarea>
                        </div>
                        @if($errors->has('description'))
                            <p class="text-danger">{{ $errors->first('description') }}</p>
                        @endif
                    </div>  
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Precision d'activité</p>
                            <textarea class="form-control input-sm"  name="precision" data-toggle="tooltip" data-placement="right" data-original-title="Si Transporteur, lister ici les numéros des moyens de transport. Si Importateur, Exportateur, Collecteur (ou autres), énumérer les principaux produits. Pour les titulaires de licence de boissons alcooliques, mentionner la catégorie, la référence et la date de décision."></textarea>
                        </div>
                    
                    </div> 
                </div> 

                  <div class="row">    
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Registre du commerce</p>
                        <input type="text" class="form-control form-control-sm"  name="reg_comm">
                      </div>
                        
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Date de registre du commerce</p>
                        <input type="date" class="form-control form-control-sm"  name="date_reg_comm">
                      </div>
                    </div> 
                  </div> 
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('debut') ? ' has-error' : '' }}">
                        <p>Début de l'exercice comptable</p>
                        <input type="text" class="form-control form-control-sm {{ $errors->has('debut') ? 'is-invalid' : '' }}"  name="debut" value="01/01"  data-toggle="tooltip"  data-original-title="Changer cette valeur par défaut, si votre exercice ne débute pas le 1er Janvier.">
                      </div>
                      @if($errors->has('debut'))
                        <p class="text-danger">{{ $errors->first('debut') }}</p>
                      @endif
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('cloture') ? 'has-error' : '' }}">
                        <p>Clôture de l'exercice comptable</p>
                        <input type="text" class="form-control form-control-sm {{ $errors->has('cloture') ? 'is-invalid' : '' }}"  name="cloture" value="31/12">
                      </div>
                      @if($errors->has('cloture'))
                        <p class="text-danger">{{ $errors->first('cloture') }}</p>
                      @endif
                    </div>  
                  </div>  <br> 
                  <div class="row">  
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        
                            Importateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="import" checked id="radioDanger1" value="Oui">
                          <label for="radioDanger1">Oui</label>
                        &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                       
                          <input type="radio" name="import" id="radioDanger2" value="Non">
                          <label for="radioDanger2">Non</label>
                       
                        @if($errors->has('import'))
                          <p class="text-danger">{{ $errors->first('import') }}</p>
                        @endif
                      </div> 
                    </div><br> 
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                      
                            Exportateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="export" checked id="radioPrimary1"value="Oui">
                          <label for="radioPrimary1">Oui</label>
                    
                      
                          <input type="radio" name="export" id="radioPrimary2" value="Non">
                          <label for="radioPrimary2">Non</label>
                       
                        @if($errors->has('exportateur'))
                          <p class="text-danger">{{ $errors->first('exportateur') }}</p>
                        @endif
                      </div> 
                    </div> 
                  </div> 
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('nbr_salarier') ? ' has-error' : '' }}">
                        <p>Nombre de salarié</p>
                        <input type="text" class="form-control form-control-sm  {{ $errors->has('nbr_salarier') ? ' is-invalid' : '' }}" value="{{old('nbr_salarier')}}"  name="nbr_salarier" >
                      </div>
                      @if($errors->has('nbr_salarier'))
                        <p class="text-danger">{{ $errors->first('nbr_salarier') }}</p>
                      @endif
                    </div>
                  </div> 
               
            </div>
            <div class="tab-pane" id="address">
            <div class="row">
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('adrs') ? ' has-error' : '' }}">
                      <p>Adresse actuel *</p>
                      <input onBlur="controlAdresse()" class="form-control form-control-sm {{ $errors->has('adrs') ? ' is-invalid' : '' }}" type="text" name="adrs">
                    </div>
                    @if($errors->has('adrs'))
                      <p class="text-danger">{{ $errors->first('adrs') }}</p>
                    @endif
                  </div>  
                
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Fokontany</p>
                      <input class="form-control form-control-sm" type="text"  name="fokontany">
                    </div>
                  
                  </div> 
              </div>    
                
              <div class="row">    
                  <div class="col-md-5">
                        <div class="form-group">
                          <p>Commune</p>
                          <input class="form-control form-control-sm" type="text"  name="commune">
                        </div>
                      
                  </div>  
                  
              
                  <div class="col-md-5">
                      <div class="form-group">
                        <p>District</p>
                        <input class="form-control form-control-sm" type="text"  name="district">
                      </div>
                    </div> 
                </div> 
                  
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Region</p>
                      <input class="form-control form-control-sm" type="text" name="region">
                    </div>
                  
                  </div>  
                
                
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Province</p>
                    <input class="form-control form-control-sm" type="text" name="province">
                    </div>
                  </div>  
              </div>  
            </div>
            
          
        </div>
        <div class="wizard-footer height-wizard">
            <div class="pull-right">
                <input type='button'  class='btn btn-next btn-fill btn-success btn-wd btn-sm' name='next' value='Suivant' />
                <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />

            </div>

            <div class="pull-left">
                <input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
            </div>
            <div class="clearfix"></div>
        </div>

    </form>
</div>
</div> <!-- wizard container -->

        </div>
        </div>
    </div>
    </div>
</div>
</div>
<!-- /page content -->
@endsection