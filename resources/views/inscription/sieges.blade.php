@extends('layouts.dash')

@section('content')
 <div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Icons</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="../assets/img/theme/team-4-800x800.jpg">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">Jessica Jones</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-md-12">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                       
                          <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item active"><a href="{{route('inscription.pers')}}">Principaux renseignement</a></li>
                            <li class="breadcrumb-item"><a href="{{route('inscription.persPhys')}}">Activité</a></li>
                            <li class="breadcrumb-item"><a href="#">Siege</a></li>
                            <li class="breadcrumb-item">Etablissement</a></li>
                            <li class="breadcrumb-item">Dirigeant</a></li>
                            <li class="breadcrumb-item">Véhicule</a></li>
                            <li class="breadcrumb-item">Interlocuteur</li>
                          </ol>
                        
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent">
              <h3 class="mb-0">Icons</h3>
            </div>
            <div class="card-body">
                         <form action="{{ route('inscription.sieges') }}" method="post">
                              {{ csrf_field() }}
                                      <div class="row">
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <p>Adresse actuel</p>
                                              <input class="form-control form-control-sm" type="text" name="adrs">
                                            </div>
                                          
                                          </div>  
                                        
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <p>Fokontany</p>
                                              <input class="form-control form-control-sm" type="text"  name="fokontany">
                                            </div>
                                          
                                          </div> 
                                      </div>    
                                        
                                      <div class="row">    
                                          <div class="col-md-5">
                                                <div class="form-group">
                                                  <p>Commune</p>
                                                  <input class="form-control form-control-sm" type="text"  name="commune">
                                                </div>
                                              
                                          </div>  
                                          
                                      
                                          <div class="col-md-5">
                                              <div class="form-group">
                                                <p>District</p>
                                                <input class="form-control form-control-sm" type="text"  name="district">
                                              </div>
                                            </div> 
                                        </div> 
                                          
                                        <div class="row">
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <p>Region</p>
                                              <input class="form-control form-control-sm" type="text" name="region">
                                            </div>
                                          
                                          </div>  
                                        
                                        
                                          <div class="col-md-5">
                                            <div class="form-group">
                                              <p>Province</p>
                                            <input class="form-control form-control-sm" type="text" name="province">
                                            </div>
                                          </div>
                                          
                               

                  </div>
                </div>
              </div>
                                
              <div class="row">
                  <div class="col-sm-3">
                      <button type="reset" class="btn btn-danger btn-block">
                        <i class="mdi  mdi-refresh"></i>
                          Annuler
                      </button>
                  </div>
                  <div class="col-sm-3">
                      <button type="submit" class="btn btn-success btn-block">
                        <i class="mdi mdi-file-document"></i>
                          Enregister
                      </button>
                  </div>
                              </form>    
                         
               
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <!-- Footer -->
      <footer class="footer">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
              &copy; 2018 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
          </div>
          <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
              <li class="nav-item">
                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
              </li>
              <li class="nav-item">
                <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="https://github.com/creativetimofficial/argon-dashboard/blob/master/LICENSE.md" class="nav-link" target="_blank">MIT License</a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    </div>
  </div>

@endsection