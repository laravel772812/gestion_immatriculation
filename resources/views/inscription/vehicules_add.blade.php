@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
              <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur les vehicules du contribuable</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
                <div class="card-body">
             
                 <form action="{{ route('vehicules.adds') }}" method="post">
                              {{ csrf_field() }}
                            
                             
                              <div class="row">
                                    
                                  <div class="col-md-5">
                                    <div class="form-group {{$errors->has('num_veh') ? 'has-error' : ''}}">
                                      <p>Numero vehicule</p>
                                      <input class="form-control form-control-sm {{$errors->has('num_veh') ? 'is-invalid' : ''}}" type="text" name="num_veh">
                                   </div>
                                   @if($errors->has('num_veh'))
                                      <p class="text-danger">{{ $errors->first('num_veh') }}</p>
                                    @endif
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                       <p>marque</p>
                                        <input  class="form-control form-control-sm" type="text"  name="marque">
                                    </div>
                                  </div> 
                              </div>    
                                
                              <div class="row">    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Genre</p>
                                        <input class="form-control form-control-sm" type="text"  name="genre">
                                    </div>
                                  </div>  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Type</p>
                                      <input class="form-control form-control-sm" type="text"   name="type">
                                    </div>
                                  </div> 
                              </div>
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Puissance</p>
                                      <input class="form-control form-control-sm" type="text"  name="puissance">
                                    </div>
                                
                                  </div>  
                                 
                                 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Nombre de place</p>
                                        <input class="form-control form-control-sm" type="text"  name="nbr_place" value="{{ old('nbr_place')}}">
                                    </div>
                                    @if($errors->has('nbr_place'))
                                      <p class="text-danger">{{ $errors->first('nbr_place') }}</p>
                                    @endif
                                  </div> 
                                 
                              </div>   
                              <div class="row">  
                                  <div class="col-md-5">
                                      <div class="form-group">
                                        <p>Charge</p>
                                        <input class="form-control form-control-sm" type="text"  name="charge"  value="{{ old('charge')}}">
                                      </div>
                                      @if($errors->has('charge'))
                                        <p class="text-danger">{{ $errors->first('charge') }}</p>
                                      @endif
                                  </div> 
                                    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Poid</p>
                                        <input class="form-control form-control-sm" type="text"  name="poid" value="{{ old('poid')}}">
                                      </div>
                                      @if($errors->has('poid'))
                                        <p class="text-danger">{{ $errors->first('poid') }}</p>
                                      @endif
                                  </div> 
                              </div>
                              <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Date de mise en circulation</p>
                                        <input class="form-control form-control-sm" type="date"  name="date_circ">
                                      </div>
                                  </div> 
                                    
                                  <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                            A usage professionnel &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="usage_proff" checked id="radioSuccess1" value="Oui">
                                            <label for="radioSuccess1">Oui</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio"  name="usage_proff" id="radioSuccess2" value="Non">
                                            <label for="radioSuccess2">Non</label>
                                          </div>
                                        </div> 
                                  </div><br> 
                              </div>
                              <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                        <p>Date de début</p>
                                        <input class="form-control form-control-sm" type="date"  name="date_debut">
                                      </div>
                                  </div> 
                                    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Type d'exploitation</p>
                                      <select class="form-control form-control-sm" style="width: 100%;" name="exploitation">
                                        <option selected="selected" value="">--Choisissez dans la liste--</option>
                                        <option>FLUVIAL</option>
                                        <option>DANS TOUTES L'ILE</option>
                                        <option>NATIONAL</option>
                                        <option>REGIONAL</option>
                                        <option>SUBURBAINE</option>
                                        <option>URBAINE</option>
                                      
                                      </select>
                                    </div>  
                              </div>
                              
                                <div class="col-md-5">
                                    <div class="form-group">
                                    
                                      <p>Nom proprietaire du vehicule</p>
                                      <select name="pers_phys_id"  id="pers_phys"  class="form-control form-control-sm">
                                       
                                          <option value="{{ $pers->id }}">{{ $pers->nom_pers_phys }} </option>
                                      
                                      </select>
                                      
                                    </div>
                              
                              </div> 
                              
                            </div> 
                          </div>
                        </div>
                         <br>      
                        <div class="col-12">
                          <button type="reset" class="btn btn-secondary">Annuler</button>
                          <input type="submit" value="Enregistrer" class="btn btn-outline-success float-right">
                        </div>
            </form> 
          </div> 
      </section>  <br>   
  
  </div>

@endsection