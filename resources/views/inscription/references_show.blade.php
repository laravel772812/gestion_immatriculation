@extends('layouts.application')

@section('content')
<div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">vehicule</li>
                <li class="breadcrumb-item active">Interlocuteur</li>
              
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>References de demande d'immatriculation fiscal</small></h1></center>
                </div>
             
                  <div class="table-responsive">
                
                       
                          <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                              <tr>

                                <th ><center>Numéro reference</center></th>
                                <th ><center>Id personne</center></th>
                                <th ><center>Id etablissement</center></th>

                                <th ><center>Confirmation</center></th>

                                <th ></th>
                              </tr>
                            </thead>
                            <tbody>
                            
                          
                        
                                <tr scope="row">
                                      <td >{{ $listes->id }}</td>
                                      <td >{{ $listes->pers_phys_id }}</td>
                                      <td >{{ $listes->etablissement_id }}</td>
                                       <td >{{ $listes->confirmation }}</td>
                                      
                                    
                                </tr>
                            </tbody>
                          </table>
                         
                          <form action="{{route('references.print')}}">  
                                <select name="a"   id="a" class="form-control form-control-sm">
                              
                                    <option value="{{ $ref->id }}">{{ $ref->id }} </option>
                              
                                </select>
                             
                          
                  </div>
             
                <div class="card-footer py-4" >
                  <center><button type="submit"  class="btn btn-default" target="_blank" ><i class="fas fa-print"></i> Imprimer</button></center>
                </div>
                </form>   
              </div> 
            </div> 
            
          </div>
        </div>
      </section>
</div>
@endsection