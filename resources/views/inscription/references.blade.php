@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item ">Vehicules</li>
                <li class="breadcrumb-item">Interlocuteur</li>
                <li class="breadcrumb-item active">Reference</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Reference d' inscription</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
            <div class="card-body">
                <form action="{{ route('references.add') }}" method="post">
                {{ csrf_field() }}
              
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nom personne</p>
                            <select name="pers_phys_id"   id="pers_phys" class="form-control form-control-sm">
                           
                                <option value="{{ $pers->id }}">{{ $pers->nom_pers_phys }} </option>
                           
                            </select>
                        </div>      
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nom etablissement</p>
                            <select name="etablissement_id"    id="etablissement" class="form-control form-control-sm">
                           
                                <option value="{{ $etabl->id }}">{{ $etabl->nom_etabl }} </option>
                            
                            </select>
                        </div>      
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Date reference</p>
                            <input type="date" name="date_ref"   class="form-control form-control-sm" required>
                           
                        </div>      
                    </div>
                    @if(Auth::guard('admin')->check())
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Nouveau NIF</p>
                            <input type="text" name="nouveau_nif"   class="form-control form-control-sm" required>
                           
                        </div>      
                    </div>
                    @endif
                  
                   
                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input type="checkbox" id=" customCheckLogin" class="custom-control-input" value="je certifie que ces renseignements sont complets et exacts" name="confirmation" required> 
                            <label class="custom-control-label" for=" customCheckLogin">
                                <span class="text-muted">je certifie que ces renseignements sont complets et exacts</span>
                            </label>
                        </div>
                   
             
                                    
            </div>
          </div>
        </div>
                         
        <div class="col-12" >
            
            <input type="submit" value="Enregistrer" class="btn btn-success float-right">
        </div>
        </form> 
        </div> 
      </section>  <br>   
  
  </div>

@endsection