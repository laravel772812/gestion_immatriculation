@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">Etablissement</li>
                <li class="breadcrumb-item active">Associer</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Renseignement sur les associers</small></h1></center>
                </div>
                @if(Session::has('success'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('success')}}
                                </div>
                              </div>
                            @endif
                   @if(Auth::guard('admin')->check())
                        <div class="table-responsive">
                            @if(Session::has('success'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('success')}}
                                </div>
                              </div>
                            @endif
                            @if(Session::has('successs'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('successs')}}
                                </div>
                              </div>
                            @endif
                <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                    <tr>
                      
                      <th scope="col">Type associer</th>
                      
                      <th scope="col">Nom associer</th>
                      <th scope="col">Fonction associer</th>
                      <th scope="col">CIN associer</th>
                      <th scope="col">Adresse associer</th>
                      <th scope="col">Activité associer</th>
                      <th scope="col">NIF activité</th>
                      <th scope="col">Email associer</th>
                      <th scope="col">Tel associer</th>
                      <th scope="col">Associer unique</th>
                      <th scope="col">% action</th>
                      
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                  
                @foreach ($associes as $associe)
              
                      <tr scope="row">
                           <td>{{ $associe->type_ass }}</td>
                            <td>{{ $associe->nom_ass }}</td>
                            <td>{{ $associe->fonction_ass }}</td>
                            <td>{{ $associe->cin_ass }}</td>
                            <td>{{ $associe->adrs_ass }}</td>
                            <td>{{ $associe->activite_ass }}</td>
                            <td>{{ $associe->nif_activite_ass }}</td>
                            <td>{{ $associe->email_ass }}</td>
                            <td>{{ $associe->tel_ass }}</td>
                            <td>{{ $associe->ass_unique }}</td>
                            <td>{{ $associe->action }}</td>
                           
                            <td class="text-right">
                              <a class="btn btn-danger btn-sm"  href="{{route('associer.delete',['id'=>$associer->id]) }}"><i  class="fas fa-trash"></i></a>
                            </td>
                      </tr>

                @endforeach
                    
                  
                      
                  
                  
                  </tbody>
                </table>
                
              </div>
            @endif  
            <div class="card-footer py-4">
              <center><a href="{{ route('associer.add') }}" class="btn btn-outline-success">Plus des associers</a></center>
            </div>
            </div>
            </div>
           
     
          </div>
        </div>
      </section>
</div>

@endsection