@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">Renseignement</li>
                <li class="breadcrumb-item active">Etablissement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Principaux renseignements sur l'établissement</small></h1></center>
                </div>
                   
                        <div class="table-responsive">
                            @if(Session::has('success'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('success')}}
                                </div>
                              </div>
                            @endif
                            @if(Session::has('successs'))
                              <div class="col-md-6">
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                  
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>Succés! </button> {{Session::get('successs')}}
                                </div>
                              </div>
                            @endif
                            <table class="table align-items-center table-flush">
                              <thead class="thead-light">
                                <tr>
                                  <th><center>id personne</center></th>
                                  <th><center>Nom commercial</center></th>
                                  
                                  <th ><center>Date ouverture</center></th>
                                  <th ><center>Telephone 1</center></th>
                                  <th ><center>Autre telephone</center></th>
                                  <th ><center>Fax</center></th>
                                  <th ><center>Email</center></th>
                                  <th ><center>Exportateur</center></th>
                                  <th ><center>Importateur</center></th>
                                  <th ><center>Type proprietaire</center></th>
                                  @if($etablissement->type_prop == "Societé")
                                  <th ><center>NIF proprietaire</center></th>
                                  @else
                                  <th ><center>Nom proprietaire</center></th>
                                  <th ><center>CIN proprietaire</center></th>
                                  <th ><center>Adresse proprietaire</center></th>
                               
                                  <th ><center>Telephone proprietaire</center></th>
                                  @endif
                                  <th ><center></th>
                                </tr>
                              </thead>
                              <tbody>
                              
                                  <tr>
                                      <td >{{ $etablissement->pers_phys_id }}</td>
                                        <td>{{ $etablissement->nom_etabl }}</td>
                                        
                                        <td>{{ $etablissement->date_ouvert }}</td>
                                        <td>{{ $etablissement->tel }}</td>
                                        <td>{{ $etablissement->autre_tel }}</td>
                                        <td>{{ $etablissement->fax }}</td>
                                        <td>{{ $etablissement->email }}</td>
                                        <td>{{ $etablissement->exportateur }}</td>
                                        <td>{{ $etablissement->importateur }}</td>
                                        <td>{{ $etablissement->type_prop }}</td>
                                        @if($etablissement->type_prop == "Societé")
                                        <td>{{ $etablissement->nif_prop }}</td>
                                        @else
                                        <td>{{ $etablissement->nom_prop }}</td>
                                        <td>{{ $etablissement->cin_prop }}</td>
                                        <td>{{ $etablissement->adrs_prop }}</td>
                                        <td>{{ $etablissement->tel_prop }}</td>
                                        @endif
                                       
                                      
                                  </tr>

                           
                                
                              
                                  
                              
                              
                              </tbody>
                            </table>
                            
                        </div>
                   
                  <div class="card-footer py-4">
                    <center><a href="{{ route('etablissements.add') }}" class="btn btn-outline-success">Ajouter des etablissements</a></center>
                  </div>
                  <div class="col-12">
                    <button type="reset" class="btn btn-secondary">Annuler</button>
                    <a href="{{ route('inscription.dirigeants') }}" class="btn btn-outline-success float-right">Suivant</a>
                  </div>
              </div>
            </div>
           
     
          </div>
        </div>
      </section>
</div>

@endsection