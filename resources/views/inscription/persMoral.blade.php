@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                    <h3 class="card-title">Principaux renseignement</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                     
                    </div>
                </div>
                <div class="card-body">
                    <script language="javascript">
                        function controlnom(form){
                            var test = document.form.raison_social.value;
                            var nb = test.length;
                            if(nb>=1){
                              var maj=test.toUpperCase();
                              document.form.raison_social.value=maj;
                            }
                          }

                          function controlprenom(){
                              var str=document.form.preraison_social.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.preraison_social.value=prnm;
                          }
                          
                          function controlLieu_naiss(){
                              var str=document.form.lieu_naiss.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.lieu_naiss.value=prnm;
                          }

                          function controlecin(form){
                              var val=document.form.cin.value;
                              if(isNaN(val)==false){
                                document.getElementById('cin').innerHTML="";
                                  var n=val.length;
                                  if (n==12 || n==""){
                                      var pc = val.substring(0,1);
                                      document.getElementById('cin').innerHTML="";
                                      if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6 || pc==""){
                                          var cs=val.substring(5,6);
                                          document.getElementById('cin').innerHTML="";
                                          if(cs==1 || cs==2 ){
                                              document.form.cin.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                                          }else{
                                            document.getElementById('cin').innerHTML="";
                                          }
                                      }else{
                                        document.getElementById('cin').innerHTML="CIN invalide à Madagascar ";
                                        
                                      }
                                  }else{
                                      document.getElementById('cin').innerHTML ="votre CIN est incorrect";
                              
                                  }
                              }else{
                                document.getElementById('cin').innerHTML=" ";
                              }
                          }
                        
                          function controleNum_stat(form){
                              var val=document.form.num_stat.value;
                              if(isNaN(val)==false){
                                document.getElementById('num_stat').innerHTML="";
                                  var n=val.length;
                                  if (n!=17){
                                    document.getElementById('num_stat').innerHTML="Numéro statistique invalide";
                                  } else {
                                    document.form.num_stat.value=val.substring(0,5) + " " + val.substring(5,7) + " " + val.substring(7,11) + " " + val.substring(11,12)  + " " + val.substring(12,17);
                                    
                                  }  
                              }
                          }
                        
                          function controlAdresse(){
                              var str=document.form.adrs_m.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.adrs_m.value=prnm;
                          }

                          function controlerib(form){
                              var val=document.form.rib_m.value;
                              if(isNaN(val)==false){
                                
                                  var n=val.length;
                                  if (n==23){
                                    document.form.rib_m.value=val.substring(0,5) + "-" + val.substring(5,10) + "-" + val.substring(10,21) + "-" + val.substring(21,23);
                                  
                                  }else {
                                    document.getElementById('rib_m').innerHTML ="RIB incorrecte";
                                  }
                              }else{
                                document.getElementById('rib_m').innerHTML ="RIB doit être en chiffre";
                              
                              }
                            
                          }
                    </script>
                      <form action="{{ route('inscription.persMoral') }}" method="post" id="form" name="form">
                      {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-5">
                           <div class="form-group {{ $errors->has('raison_social') ? ' has-error' : '' }}">
                            <p>Raison social *</p>
                            <input onBlur="controlnom(form)" class="form-control form-control-sm {{ $errors->has('raison_social') ? ' is-invalid' : '' }}" value="{{ old('raison_social') }}" type="text" name="raison_social">
                          </div>
                          @if($errors->has('raison_social'))
                            <p class="text-danger">{{ $errors->first('raison_social') }}</p>
                          @endif
                        </div>  
                        <div class="col-md-5">
                          <div class="form-group  {{ $errors->has('form_jurid') ? ' has-error' : '' }}">
                            <p>forme juridique *</p>
                            <select class="form-control form-control-sm  {{ $errors->has('form_jurid') ? ' is-invalid' : '' }}" id="form_jurid" data-id="" name="form_jurid" >

                                  <option class="select_first_option" value="" selected="">
                                      Choisissez dans la liste --
                                  </option>
                                  <option >commune_m RURALE</option>

                                  <option >commune_m URBAINE</option>

                                  <option >REGION</option>

                                  <option >DISTRICT</option>

                                  <option >PROVINCE</option>

                                  <option >STE EN PARTICIPATION</option>

                                  <option >INCONNU</option>

                                  <option >ENTREPRISE INDIVIDUELLE</option>

                                  <option >STE EN NOM COLLECTIF</option>

                                  <option>STE EN COMMANDITE SIMPLE</option>

                                  <option>STE EN COMPTE PAR ACTION</option>

                                  <option >SA (SOCIETE ANONYME)</option>

                                  <option >SARL (STE A RESPTE LIMITEE)</option>

                                  <option >STE D'ECONOMIE MIXTE</option>

                                  <option >STE D'INTERET NATIONAL</option>

                                  <option >STE ANONYME D'ETAT</option>

                                  <option >ENTREPRISE SOCIALISTE</option>

                                  <option >EPIC (ETS PUBLIC A CAR IND &amp; CO) </option>

                                  <option >STE CIVILE IMMOBILIERE</option>

                                  <option >COOPERATIVE</option>

                                  <option >ASSOCIATION</option>

                                  <option >ONG (ORGANISME NON GOUVERNEMENTAL)</option>

                                  <option >ETS PUBLIC SCIENT.TECHNI</option>

                                  <option >PROJET</option>

                                  <option >SARLU (STE A RESP LIMIT UNI)</option>

                                  <option >OIG (ORGANISME INTER GOUVERNEMENTAL)</option>

                                  <option >EPA (ETS PUBLIC A CARACTERE ADMINISTRATIF)</option>

                                  <option>STE ANONYME UNIPERSONNELE</option>

                            </select>
                          </div>
                            @if($errors->has('form_jurid'))
                                  <p class="text-danger">{{ $errors->first('form_jurid') }}</p>
                                @endif
                        </div> 
                    </div>    
                  
              </div>
            </div> 
           
          <div class="col">  
            <div class="card shadow">
              <div class="card-header">
                  <h3 class="card-title">Siège de la societé</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    
                  </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('adrs_m') ? ' has-error' : '' }}">
                      <p>Adresse actuel *</p>
                      <input onBlur="controlAdresse()" class="form-control form-control-sm {{ $errors->has('adrs_m') ? ' is-invalid' : '' }}" type="text" name="adrs_m">
                    </div>
                    @if($errors->has('adrs_m'))
                      <p class="text-danger">{{ $errors->first('adrs_m') }}</p>
                    @endif
                  </div>  
                
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Fokontany</p>
                      <input class="form-control form-control-sm" type="text"  name="fokontany">
                     </div>
                  </div> 
                </div>    
              </div>  
            </div>
          </div>
          <div class="col"> 
            <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Activité de la societé</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                </div>
              </div>
              <div class="card-body">
                  
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('description_m') ? ' has-error' : '' }}">
                      <p>Description d' activités</p>
                      <textarea  class="form-control input-sm {{ $errors->has('description_m') ? 'is-invalid' : '' }}"  name="description_m" data-toggle="tooltip" data-original-title="Lister par ordre d'importance toutes vos activités"></textarea>
                    </div>
                    @if($errors->has('description_m'))
                      <p class="text-danger">{{ $errors->first('description_m') }}</p>
                    @endif
                  </div>  
                
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Precision d'activité</p>
                      <textarea class="form-control input-sm"  name="precision_m" data-toggle="tooltip" data-placement="right" data-original-title="Si Transporteur, lister ici les numéros des moyens de transport. Si Importateur, Exportateur, Collecteur (ou autres), énumérer les principaux produits. Pour les titulaires de licence de boissons alcooliques, mentionner la catégorie, la référence et la date de décision."></textarea>
                    </div>
                  
                  </div> 
                </div>    
                            
                <div class="row">    
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Registre du commerce</p>
                        <input type="text" class="form-control form-control-sm"  name="reg_comm_m">
                      </div>
                        
                    </div>  
                    
                
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Date de registre du commerce</p>
                        <input type="date" class="form-control form-control-sm"  name="date_reg_comm_m">
                      </div>
                    </div> 
                  </div> 
                    
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('debut') ? ' has-error' : '' }}">
                        <p>Début de l'exercice comptable</p>
                        <input type="text" class="form-control form-control-sm {{ $errors->has('debut') ? 'is-invalid' : '' }}"  name="debut_m" value="01/01"  data-toggle="tooltip"  data-original-title="Changer cette valeur par défaut, si votre exercice ne débute pas le 1er Janvier.">
                      </div>
                      @if($errors->has('debut'))
                        <p class="text-danger">{{ $errors->first('debut') }}</p>
                      @endif
                    </div>  
                  
                  
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('cloture') ? 'has-error' : '' }}">
                        <p>Clôture de l'exercice comptable</p>
                          <input type="text" class="form-control form-control-sm {{ $errors->has('cloture') ? 'is-invalid' : '' }}"  name="cloture_m" value="31/12">
                        </div>
                        @if($errors->has('cloture'))
                          <p class="text-danger">{{ $errors->first('cloture') }}</p>
                        @endif
                      </div>  
                  </div> 
                  <div class="row">  
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Importateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="import_m" checked id="radioDanger1" value="Oui">
                          <label for="radioDanger1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="import_m" id="radioDanger2" value="Non">
                          <label for="radioDanger2">Non</label>
                        </div>
                        @if($errors->has('import'))
                          <p class="text-danger">{{ $errors->first('import') }}</p>
                        @endif
                      </div> 
                      </div><br> 
                      <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Exportateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="export_m" checked id="radioPrimary1"value="Oui">
                          <label for="radioPrimary1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="export_m" id="radioPrimary2" value="Non">
                          <label for="radioPrimary2">Non</label>
                        </div>
                        @if($errors->has('exportateur'))
                          <p class="text-danger">{{ $errors->first('exportateur') }}</p>
                        @endif
                      </div> 
                      </div>
                </div>     <br> 
                          
                <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('nbr_salarier') ? ' has-error' : '' }}">
                        <p>Nombre de salarié</p>
                        <input type="text" class="form-control form-control-sm  {{ $errors->has('nbr_salarier') ? ' is-invalid' : '' }}" value="{{old('nbr_salarier')}}"  name="nbr_salarier_m" >
                      </div>
                      @if($errors->has('nbr_salarier'))
                        <p class="text-danger">{{ $errors->first('nbr_salarier') }}</p>
                      @endif
                    </div>
                   <div class="col-md-5">
                    <div class="form-group">
                      <p>Regime fiscal *</p>
                          <select class="form-control form-control-sm" style="width: 100%;" name="reg_fisc_m">
                            <option selected="selected">--Choisissez dans la liste--</option>
                            <option>DROIT COMMUN</option>

                            <option>ZONE FRANCHE</option>

                            <option>CODE DES INVESTISSEMENTS</option>

                            <option>LGIM</option>

                            <option>CONVENTION D'ETABLISSEMENT</option>

                            <option>AUTRES</option>
                          
                          </select>
                    </div>
                  </div>  
                </div>      
                <div class="row"> 
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Periode de grace *</p>
                      <input class="form-control form-control-sm" value="0" type="text" id="peri_grace" name="peri_grace_m">
                    </div>
                  </div> 
               
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Capital en Ar *</p>
                      <input class="form-control form-control-sm" type="text" id="capital" name="capital_m">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group  {{ $errors->has('rib_m') ? ' has-error' : '' }}">
                      <p>RIB *</p>
                    
                      <input onBlur="controlerib(form)" placeholder="11111-11111-11111111111-11"  data-toggle="tooltip" data-placement="bottom" data-original-title="Numéro de compte bancaire. Obligatoire pour les personnes morales ou les assujettis à la TVA." class="form-control form-control-sm  {{ $errors->has('rib_m') ? ' is-invalid' : '' }}" type="text"  name="rib_m">
                    </div>
                    <p class="text-danger" id="rib_m"></p>
                  </div> 
                </div> 
              </div>
            </div>
          </div>
          
        <div class="col-12">
              <button type="reset" class="btn btn-secondary">Annuler</button>
              <input type="submit" value="Enregistrer" class="btn btn-success float-right">
        </div>
      </form> 
      </div> 
      </section>   <br>  
  
  </div>

@endsection