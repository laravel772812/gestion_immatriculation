@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                    <h3 class="card-title">Principaux renseignement</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                     
                    </div>
                </div>
                <div class="card-body">
                    <script language="javascript">
                        function controlnom(form){
                            var test = document.form.raison_social.value;
                            var nb = test.length;
                            if(nb>=1){
                              var maj=test.toUpperCase();
                              document.form.raison_social.value=maj;
                            }
                          }

                          function controlprenom(){
                              var str=document.form.preraison_social.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.preraison_social.value=prnm;
                          }
                          
                          function controlLieu_naiss(){
                              var str=document.form.lieu_naiss.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.lieu_naiss.value=prnm;
                          }

                          function controlecin(form){
                              var val=document.form.cin.value;
                              if(isNaN(val)==false){
                                document.getElementById('cin').innerHTML="";
                                  var n=val.length;
                                  if (n==12 || n==""){
                                      var pc = val.substring(0,1);
                                      document.getElementById('cin').innerHTML="";
                                      if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6 || pc==""){
                                          var cs=val.substring(5,6);
                                          document.getElementById('cin').innerHTML="";
                                          if(cs==1 || cs==2 ){
                                              document.form.cin.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                                          }else{
                                            document.getElementById('cin').innerHTML="";
                                          }
                                      }else{
                                        document.getElementById('cin').innerHTML="CIN invalide à Madagascar ";
                                        
                                      }
                                  }else{
                                      document.getElementById('cin').innerHTML ="votre CIN est incorrect";
                              
                                  }
                              }else{
                                document.getElementById('cin').innerHTML=" ";
                              }
                          }
                        
                          function controleNum_stat(form){
                              var val=document.form.num_stat.value;
                              if(isNaN(val)==false){
                                document.getElementById('num_stat').innerHTML="";
                                  var n=val.length;
                                  if (n!=17){
                                    document.getElementById('num_stat').innerHTML="Numéro statistique invalide";
                                  } else {
                                    document.form.num_stat.value=val.substring(0,5) + " " + val.substring(5,7) + " " + val.substring(7,11) + " " + val.substring(11,12)  + " " + val.substring(12,17);
                                    
                                  }  
                              }
                          }
                        
                          function controlAdresse(){
                              var str=document.form.adrs_m.value;
                              var x=0;
                              var y=1;
                              var txt=(str.substring(x,y)).toUpperCase();
                              var prnm=txt;
                              for(var i=0;i<str.length;i++){
                                x=y;
                                if(str.charAt(i)==" "){
                                  
                                  prnm=prnm+str.substring(y,i)+" ";
                                  txt=(str.substring(i+1,i+2)).toUpperCase();
                                  y=i+2;
                                  prnm=prnm+txt;
                                }       
                                  
                              }
                                  prnm=prnm+str.substring(x,str.length)+" ";
                              document.form.adrs_m.value=prnm;
                          }

                          function controlerib(form){
                              var val=document.form.rib_m.value;
                              if(isNaN(val)==false){
                                
                                  var n=val.length;
                                  if (n==23){
                                    document.form.rib_m.value=val.substring(0,5) + "-" + val.substring(5,10) + "-" + val.substring(10,21) + "-" + val.substring(21,23);
                                  
                                  }else {
                                    document.getElementById('rib_m').innerHTML ="RIB incorrecte";
                                  }
                              }else{
                                document.getElementById('rib_m').innerHTML ="RIB doit être en chiffre";
                              
                              }
                            
                          }
                    </script>
                      <form action="{{ route('pers_moral.edit',['id' =>$pers->id]) }}" method="GET" id="form" name="form">
                      {{ csrf_field() }}
                      <div class="row">
               
               <div class="col">
             @if(Session::has('success'))
               <div class="col-md-6">
                 <div class="alert alert-success alert-dismissible fade show" role="alert">
                   
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                     <strong>Succés! </button> {{Session::get('success')}}
                 </div>
               </div>
             @endif
             </div>
             </div>
                    <div class="row">
                        <div class="col-md-5">
                           <div class="form-group {{ $errors->has('raison_social') ? ' has-error' : '' }}">
                            <p>Raison social *</p>
                            <input onBlur="controlnom(form)" class="form-control form-control-sm {{ $errors->has('raison_social') ? ' is-invalid' : '' }}" value="{{ $pers->raison_social }}" type="text" name="raison_social" required>
                          </div>
                          @if($errors->has('raison_social'))
                            <p class="text-danger">{{ $errors->first('raison_social') }}</p>
                          @endif
                        </div>  
                        <div class="col-md-5">
                          <div class="form-group  {{ $errors->has('form_jurid') ? ' has-error' : '' }}">
                            <p>forme juridique *</p>
                            <input class="form-control form-control-sm " value="{{ $pers->form_jurid }}" type="text" name="form_jurid">
                          </div>
                            @if($errors->has('form_jurid'))
                                  <p class="text-danger">{{ $errors->first('form_jurid') }}</p>
                            @endif
                        </div> 
                    </div>    
                  
              </div>
            </div> 
           
          <div class="col">  
            <div class="card shadow">
              <div class="card-header">
                  <h3 class="card-title">Siège de la societé</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    
                  </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('adrs_m') ? ' has-error' : '' }}">
                      <p>Adresse actuel *</p>
                      <input onBlur="controlAdresse()" class="form-control form-control-sm {{ $errors->has('adrs_m') ? ' is-invalid' : '' }}" type="text" name="adrs_m"  value="{{ $pers->adrs_m }}" required>
                    </div>
                    @if($errors->has('adrs_m'))
                      <p class="text-danger">{{ $errors->first('adrs_m') }}</p>
                    @endif
                  </div>  
                
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('Fokontany') ? ' has-error' : '' }}">
                      <p>Fokontany</p>
                      <input class="form-control form-control-sm  {{ $errors->has('Fokontany') ? ' is-invalid' : '' }}" type="text"  name="Fokontany"   value="{{ $pers->Fokontany }}" required>
                     </div>
                     @if($errors->has('Fokontany'))
                      <p class="text-danger">{{ $errors->first('Fokontany') }}</p>
                    @endif
                  </div> 
                </div>    
              </div>  
            </div>
          </div>
          <div class="col"> 
            <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Activité de la societé</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                </div>
              </div>
              <div class="card-body">
                  
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group {{ $errors->has('description_m') ? ' has-error' : '' }}">
                      <p>Description d' activités</p>
                      <input class="form-control input-sm {{ $errors->has('description_m') ? 'is-invalid' : '' }}"  name="description_m" data-toggle="tooltip" data-original-title="Lister par ordre d'importance toutes vos activités"  value="{{ $pers->description_m}}">
                    </div>
                    @if($errors->has('description_m'))
                      <p class="text-danger">{{ $errors->first('description_m') }}</p>
                    @endif
                  </div>  
                
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Precision d'activité</p>
                      <input class="form-control input-sm"  name="precision_m" data-toggle="tooltip" data-placement="right" data-original-title="Si Transporteur, lister ici les numéros des moyens de transport. Si Importateur, Exportateur, Collecteur (ou autres), énumérer les principaux produits. Pour les titulaires de licence de boissons alcooliques, mentionner la catégorie, la référence et la date de décision."  value="{{ $pers->precision_m }}">
                    </div>
                  
                  </div> 
                </div>    
                            
                <div class="row">    
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Registre du commerce</p>
                        <input type="text" class="form-control form-control-sm"  value="{{ $pers->reg_comm_m }}" name="reg_comm_m">
                      </div>
                        
                    </div>  
                    
                
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Date de registre du commerce</p>
                        <input type="date" class="form-control form-control-sm"  value="{{ $pers->date_reg_comm_m }}"  name="date_reg_comm_m">
                      </div>
                    </div> 
                  </div> 
                    
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('debut') ? ' has-error' : '' }}">
                        <p>Début de l'exercice comptable</p>
                        <input type="text" class="form-control form-control-sm {{ $errors->has('debut') ? 'is-invalid' : '' }}"  name="debut_m"  value="{{ $pers->debut_m }}"  data-toggle="tooltip"  data-original-title="Changer cette valeur par défaut, si votre exercice ne débute pas le 1er Janvier." required>
                      </div>
                      @if($errors->has('debut'))
                        <p class="text-danger">{{ $errors->first('debut') }}</p>
                      @endif
                    </div>  
                  
                  
                    <div class="col-md-5">
                      <div class="form-group {{ $errors->has('cloture') ? 'has-error' : '' }}">
                        <p>Clôture de l'exercice comptable</p>
                          <input type="text" class="form-control form-control-sm {{ $errors->has('cloture') ? 'is-invalid' : '' }}"  name="cloture_m"  value="{{ $pers->cloture_m}}" required>
                        </div>
                        @if($errors->has('cloture'))
                          <p class="text-danger">{{ $errors->first('cloture') }}</p>
                        @endif
                      </div>  
                  </div> 
                  <div class="row">  
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Importateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="import_m" checked id="radioDanger1" value="Oui">
                          <label for="radioDanger1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="import_m" id="radioDanger2" value="Non">
                          <label for="radioDanger2">Non</label>
                        </div>
                        @if($errors->has('import'))
                          <p class="text-danger">{{ $errors->first('import') }}</p>
                        @endif
                      </div> 
                      </div><br> 
                      <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Exportateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="export_m" checked id="radioPrimary1"value="Oui">
                          <label for="radioPrimary1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="export_m" id="radioPrimary2" value="Non">
                          <label for="radioPrimary2">Non</label>
                        </div>
                        @if($errors->has('exportateur'))
                          <p class="text-danger">{{ $errors->first('exportateur') }}</p>
                        @endif
                      </div> 
                      </div>
                </div>     <br> 
                          
                <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('nbr_salarier') ? ' has-error' : '' }}">
                        <p>Nombre de salarié</p>
                        <input type="text" class="form-control form-control-sm  {{ $errors->has('nbr_salarier') ? ' is-invalid' : '' }}"  value="{{ $pers->nbr_salarier_m }}"  name="nbr_salarier_m" >
                      </div>
                      @if($errors->has('nbr_salarier'))
                        <p class="text-danger">{{ $errors->first('nbr_salarier') }}</p>
                      @endif
                    </div>
                   <div class="col-md-5">
                    <div class="form-group">
                      <p>Regime fiscal *</p>
                      <input type="text" class="form-control form-control-sm"  value="{{ $pers->reg_fisc_m }}"  name="reg_fisc_m" >
                    </div>
                  </div>  
                </div>      
                <div class="row"> 
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Periode de grace *</p>
                      <input class="form-control form-control-sm" value="{{ $pers->peri_grace_m }}"  type="text" id="peri_grace" name="peri_grace_m" required>
                    </div>
                  </div> 
               
                  <div class="col-md-5">
                    <div class="form-group">
                      <p>Capital en Ar *</p>
                      <input class="form-control form-control-sm" type="text"  value="{{ $pers->capital_m}}"  id="capital" name="capital_m">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group  {{ $errors->has('rib_m') ? ' has-error' : '' }}">
                      <p>RIB *</p>
                    
                      <input onBlur="controlerib(form)" placeholder="11111-11111-11111111111-11"  data-toggle="tooltip" data-placement="bottom" data-original-title="Numéro de compte bancaire. Obligatoire pour les personnes morales ou les assujettis à la TVA." class="form-control form-control-sm  {{ $errors->has('rib_m') ? ' is-invalid' : '' }}" type="text"  value="{{ $pers->rib_m }}"   name="rib_m">
                    </div>
                    <p class="text-danger" id="rib_m"></p>
                  </div> 
                </div> 
              </div>
              <div class="col-12">
              <a href="{{route('pers_morals')}}" class="btn btn-secondary">retour</a>
              <input type="submit" value="modifier" class="btn btn-success float-right" onclick="return(confirm('Voulez-vous modifier?'))">
        </div>
            </div>
          </div>
          
      
      </form> 
      </div> 
      </section>   <br>  
  
  </div>

@endsection