@extends('layouts.application')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div  id="carousel">
        <div class="container">
         
          <div class="row justify-content-center">
            <div class="col">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block" src="{{asset('dist/img/car1.jfif')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                   
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car2.jfif')}}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car3.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car4.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
            
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div><br>
   
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
    
        <!-- Main row -->
        <div class="row">
        
                <!-- center col -->
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-body">
                <div class="tab-content p-0">
                  <div class="col-lg-8 col-6">
                      <!-- small card -->
                      <div class="small-box bg-success">
                        
                          <h4 style="text-align:center;"><small>PERSONNE PHYSIQUE</small></h4>
                        <div class="inner bg-white">
                          <p>@if(count($totalp) != 0 ) 
                               @foreach($totalp as $tot) 
                                  <strong style="color:red;">{{$tot->totalp}} nouveaux</strong> 
                               @endforeach 
                                contribuables
                             @else 
                               Toutes les demandes sont confirmer
                             @endif</p>
                        </div>
                      
                        <a href="{{route('references.liste')}}" class="small-box-footer bg-light">
                          <span style="color:orange">Confirmer  <span><i class="fas fa-arrow-circle-right"></i>
                        </a>
                      </div>
                  </div> 
                 </div> 
                </div> 
              </div>
              
        

          </section>
          <!-- /.center col -->
         <!-- right col -->
          <section class="col-lg-6 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-body">
                <div class="tab-content p-0">
                <div class="col-lg-8 col-6">
                      <!-- small card -->
                      <div class="small-box bg-success">
                        
                          <h4 style="text-align:center;"><small>PERSONNE MORALE</small></h4>
                        <div class="inner bg-white">
                          <p>@if(count($totalf) != 0) 
                               @foreach($totalf as $tot) 
                                  <strong style="color:red;">{{$tot->totalf}} nouveaux</strong> 
                               @endforeach 
                               nouveaux contribuables 
                             @else
                               Toutes les demandes sont confirmer 
                             @endif</p>
                        </div>
                      
                        <a href="{{route('references_m.liste')}}" class="small-box-footer bg-light">
                          <span style="color:orange">Veuillez confirmer  <span><i class="fas fa-arrow-circle-right"></i>
                        </a>
                      </div>
                  </div> 
              </div> 
            </div>
       

          </section>
         <!-- /.right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
       
    <!-- /.content -->
  </div>
 
  <!-- /.content-wrapper -->
@endsection
