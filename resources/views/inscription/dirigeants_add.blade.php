@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur les dirigeants de la societé</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
                <div class="card-body">
            
                    <form action="{{ route('dirigeants.adds') }}" method="post" id="ajout" name="ajout">
                    {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('nom_dir') ? 'has-error' : ''}}">
                        <p>Nom et Prenom</p>
                        <input  onBlur="controlenom(ajout)" class="form-control form-control-sm  {{ $errors->has('nom_dir') ? 'is-invalid' : ''}}" type="text" name="nom_dir">
                      </div>
                      @if($errors->has('nom_dir'))
                        <p class="text-danger">{{ $errors->first('nom_dir') }}</p>
                      @endif
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('fonction') ? 'has-error' : ''}}">
                          <p>Fonction</p>
                          <input class="form-control form-control-sm  {{ $errors->has('fonction') ? 'is-invalid' : ''}}" type="text"  name="fonction">
                      </div>
                      @if($errors->has('fonction'))
                        <p class="text-danger">{{ $errors->first('fonction') }}</p>
                      @endif
                    </div> 
                </div>    
                  
                <div class="row">    
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('cin_dir') ? 'has-error' : ''}}">
                          <p>CIN</p>
                          <input  onBlur="controlecin_dir(ajout)" class="form-control form-control-sm  {{ $errors->has('cin_dir') ? 'is-invalid' : ''}}" type="text"  name="cin_dir">
                      </div>
                      @if($errors->has('cin_dir'))
                        <p class="text-danger">{{ $errors->first('cin_dir') }}</p>
                      @endif
                      <p id="cin_dir" class="text-danger"></p>
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('adrs_dir') ? 'has-error' : ''}}">
                        <p>Adresse</p>
                        <input class="form-control form-control-sm  {{ $errors->has('adrs_dir') ? 'is-invalid' : ''}}" type="text"   name="adrs_dir">
                      </div>
                      @if($errors->has('adrs_dir'))
                        <p class="text-danger">{{ $errors->first('adrs_dir') }}</p>
                      @endif
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('email_dir') ? 'has-error' : ''}}">
                        <p>Email</p>
                        <input class="form-control form-control-sm  {{ $errors->has('email_dir') ? 'is-invalid' : ''}}" type="email"  name="email_dir">
                      </div>
                      @if($errors->has('email_dir'))
                        <p class="text-danger">{{ $errors->first('email_dir') }}</p>
                      @endif
                    </div>  
                    
                    
                    <div class="col-md-5">
                        <div class="form-group  {{ $errors->has('tel_dir') ? 'has-error' : ''}}">
                          <p>Tel</p>
                          <input onBlur="controletelephone_dir(ajout)" class="form-control form-control-sm  {{ $errors->has('tel_dir') ? 'is-invalid' : ''}}" type="text"  name="tel_dir">
                        </div>
                        @if($errors->has('tel_dir'))
                        <p class="text-danger">{{ $errors->first('tel_dir') }}</p>
                        @endif
                      <p class="text-danger" id="tel_dir"></p>
                    </div> 
                    
                </div>  
                <script>
                  function myFunction() {
                      // Get the checkbox
                      var checkBox = document.getElementById("radioDanger1");
                      var checkBox1 = document.getElementById("radioDanger2");
                      // Get the output text
                      var text = document.getElementById("act");

                      // If the checkbox is checked, display the output text
                      if (checkBox.checked == true){
                      
                        act.style.display = "block";
                      } 
                      if (checkBox1.checked == true){
                      
                      act.style.display = "none";
                    }   
                      
                    }
                </script> 
                <div class="row">  
                    <div class="col-md-5">
                 
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Autre activité &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" onclick="myFunction()" name="import"  id="radioDanger1" value="Oui">
                          <label for="radioDanger1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" onclick="myFunction()" name="import" checked id="radioDanger2" value="Non">
                          <label for="radioDanger2">Non</label>
                        </div>
                       
                      </div> 
                    </div><br> 
                    
                </div>
                <div class="row" id="act" style="display:none">  
                    <div class="col-md-5">
                      <div class="form-group">
                          <p>Description activité</p>
                          <input class="form-control form-control-sm" type="text"  name="activite_dir">
                        </div>
                    </div> 
                      
                    <div class="col-md-5">
                      <div class="form-group">
                          <p>NIF autre activité</p>
                          <input class="form-control form-control-sm" type="text"  name="nif_activite_dir">
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Nom etablissement</p>
                        <select name="etablissement_id"    id="etablissement" class="form-control form-control-sm">
                        
                            <option value="{{ $etablissements->id }}">{{ $etablissements->nom_etabl }} </option>
                          
                        </select>
                      
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12">
            <button type="reset" class="btn btn-secondary">Annuler</button>
            <input type="submit" value="Enregistrer" class="btn btn-success float-right">
          </div>
      </form> 
    </div>  <br>
  </section>    
</div>
<script>
  function controletelephone(ajout){
    var phon=document.ajout.tel.value;
    if(isNaN(phon)==false){
        var isa=phon.length;
      
        if(isa==10 ){
            var cs=phon.substring(0,3);
            
            if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                document.ajout.tel.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
            }else{
              document.getElementById('tel').innerHTML="Operateur inconnu à Madagascar";
            }
        }else{
          
          document.getElementById('tel').innerHTML="";
          document.ajout.tel.value="";
        }
    }else{
      document.getElementById('tel').innerHTML="le numero telephone est invalide";
        document.ajout.tel.value="";
    }
  }
  function controlenom(ajout){
  var test = document.ajout.nom_dir.value;
  var nb = test.length;
  if(nb>=1){
      var maj=test.toUpperCase();
      document.ajout.nom_dir.value=maj;
  }
 }
 function controlecin_dir(ajout){
      var val=document.ajout.cin_dir.value;
      if(isNaN(val)==false){
        document.getElementById('cin_dir').innerHTML="";
          var n=val.length;
          if (n==12 || n==""){
              var pc = val.substring(0,1);
              document.getElementById('cin_dir').innerHTML="";
              if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6){
                  var cs=val.substring(5,6);
                  document.getElementById('cin_dir').innerHTML="";
                  if(cs==1 || cs==2 ){
                      document.ajout.cin_dir.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                  }else{
                    document.getElementById('cin_dir').innerHTML="votre CIN est invalide";
                  }
              }else{
                
                document.getElementById('cin_dir').innerHTML="";
                document.ajout.cin_dir.value="";
                
              }
          }else{
              document.getElementById('cin_dir').innerHTML ="votre CIN  est incorrect";
      
          }
      }else{
        document.getElementById('cin_dir').innerHTML="CIN doit être en chiffre ";
      }
  }
  function controletelephone_dir(ajout){
    var phon=document.ajout.tel_dir.value;
    if(isNaN(phon)==false){
        var isa=phon.length;
      
        if(isa==10 ){
            var cs=phon.substring(0,3);
            
            if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                document.ajout.tel_dir.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
            }else{
              document.getElementById('tel_dir').innerHTML="Operateur inconnu à Madagascar";
            }
        }else{
          
          document.getElementById('tel_dir').innerHTML="";
          document.ajout.tel_dir.value="";
        }
    }else{
      document.getElementById('tel_dir').innerHTML="le numero telephone est invalide";
        
    }
  }
</script>
@endsection