@extends('layouts.application')

@section('content')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <div  id="carousel">
        <div class="container">
         
          <div class="row justify-content-center">
            <div class="col">
              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner" role="listbox">
                  <div class="carousel-item active">
                    <img class="d-block" src="{{asset('dist/img/car1.jfif')}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                   
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car2.jfif')}}" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car3.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car4.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                    
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="d-block"src="{{asset('dist/img/car.jfif')}}"alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
            
                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <i class="now-ui-icons arrows-1_minimal-left"></i>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <i class="now-ui-icons arrows-1_minimal-right"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div><br>
   
  

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
    
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          
          
          <!-- /.Left col -->
                <!-- center col -->
          <section class="col-lg-9 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
            
              <div class="card-body">
                <div class="tab-content p-0">
                  <h3>
                    <center> IMMATRICULATION FISCAL <center>
                  </h3>  
                  <p style="color:gray; " >
                      Ce site regroupe en un seul espace les différentes fonctionnalités (Immatriculation, inscription, Consultation de situation fiscale).
                     
                  </p> <br>
                 
                  <div class="card-header">
                    <h5 class="" style="color:orange">
                      <i class="nav-icon far fa-circle text-warning"></i>
                     Nouvelle demande d'immatriculation au Numéro d'Identification Fiscale (NIF)
                    </h5>
                  </div>
                <p style="color:gray;font-size:14px;">
                 <strong>Veuillez suivre la procédure suivante : </strong> <br>
                    <ul>
                      <li style="color:gray">Compléter les pièces nécessaires requises</li>
                      <li style="color:gray">Remplir le Formulaire d'inscription en ligne</li>
                      <li style="color:gray"> Se présenter au Bureau fiscal territorialement compétent avec :</li>
                      <li style="color:gray"> L'original des pièces requises</li>
                      <li style="color:gray"> Le référence d'inscription</li>
                    </ul>  
                  
                  </p><br>
                  <div class="card-header">
                    <h5 class="" style="color:orange">
                      <i class="nav-icon far fa-circle text-warning"></i>
                      Pièces à présenter pour l'obtention du NIF :
                    </h5>
                  </div>
                <p style="color:gray;font-size:14px;" >
                <strong >Pour les Personnes Physiques :: </strong> <br>
                    <ul>
                      <li style="color:gray">Originale et copie de la CIN</li>
                      <li style="color:gray">Carte de résident pour les étrangers</li>
                      <li style="color:gray">Certificat de résidence et/ou Facture de la JIRAMA moins de trois mois</li>
                      <li style="color:gray">Plan de répérage</li>
                      <li style="color:gray">Récépissé et bordereau de versement de l'IS ou de l'IR</li>
                      <li style="color:gray">Carte statistique</li>
                      <li style="color:gray">Titre de propriété du local (pour le contrat de bail ou contrat de domiciliation ou lettre d'occupation ou mise à disposition)</li>
                      <li style="color:gray">Autres pièces originales (autorisation ministérielle, carte grise, licence, ...)</li>
                      <li style="color:gray">Procuration légalisée si représentant</li>
                      <li style="color:gray">Référence de la demande</li>
                    </ul> 
                    <a href="{{route('inscription.pers')}}" style="color:green"><i class="nav-icon fas fa-edit"></i>S'incrire pour la personne physique</a>
                  
                  </p>
               
                <strong >Pour les Personnes Morales : </strong> <br>
                    <ul>
                      <li style="color:gray">Statuts de la société</li>
                      <li style="color:gray">Originale et copie de la CIN du premier responsable</li>
                      <li style="color:gray">Carte de résident pour les étrangers</li>
                      <li style="color:gray">Certificat de résidence du premier responsable et/ou Facture de la JIRAMA moins de trois mois</li>
                      <li style="color:gray">Récépissé et bordereau de versement de l'IS ou de l'IR</li>
                      <li style="color:gray">Plan de répérage</li>
                      <li style="color:gray">Certificat d'existence</li>
                      <li style="color:gray">Carte statistique</li>
                      <li style="color:gray">Titre de propriété du local (pour le contrat de bail ou contrat de domiciliation ou lettre d'occupation ou mise à disposition)</li>
                      <li style="color:gray">Référence de la demAutres pièces originales (autorisation ministérielle, carte grise, licence, ...)ande</li>
                      <li style="color:gray">Procuration légalisée si représentant</li>
                      <li style="color:gray">Référence de la demande</li>
                    </ul> 
                    <a href="{{route('inscription.persMoral')}}" style="color:green"><i class="nav-icon fas fa-edit"></i>S'incrire pour la personne Morale</a>
                  
                  </p>
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  
                </div>
                <!-- /.card-body -->
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  <br>
                </div>
                <!-- /.card-body -->
              </div>
              
            <!-- /.card -->

          </section>
          <!-- /.center col -->
         <!-- right col -->
         <section class="col-lg-3 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="fas fa-map mr-1"></i>
                 Adresse
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                   <p style="color:orange">DGI</p>

                   <span style="font-size:13px; color:gray;">  Immeuble MFB, Antaninarenina
                    Antananarivo, 101, Madagascar
                    Tél: (020) xx-xxx-xx <br>
                    E-mali: dgimpots@moov.mg </span><br>
                    <div class="card-header">
                    </div>
                    <span style="color:orange">DRI Haute  Matsiatra</span><br>

                   <span style="font-size:13px; color:gray;"> Immeuble des domaines TSIANOLONDROA , Fianarantsoa <br>
                    code postal:301, Madagascar
                    <br>Tél: 75 503 34, 032 12 010 59
                     E-mali: dri.hautematsiatra@gmali.com </span>
                </div>
              </div>
              
              <!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header bg-gradient-success">
                <h3 class="card-title">
                  <i class="nav-icon far fa-calendar-alt"></i>
                 Calendrier
                </h3>
              
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
               
               
              
                  <div class="card-body pt-0">
                    <!--The calendar -->
                   
                    <div id="calendar" style="width: 100%;"></div>
                  </div>
                  <!-- /.card-body -->
               
              </div>
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

         </section>
         <!-- /.right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
       
    <!-- /.content -->
  </div>
  <div id="sparkline-1"></div>
          <div id="sparkline-2"></div>
      
          <div id="sparkline-3"></div>
  <!-- /.content-wrapper -->
@endsection
