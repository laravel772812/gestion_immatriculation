@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
         
            <div class="row">
              <div class="col">
                <div class="card shadow">
                  <div class="card-header">
                    <h3 class="card-title">Principaux renseignement</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                     
                    </div>
                  </div>
                   
                    <div class="card-body">
                        <form action="{{ route('inscription.persPhys') }}" method="post" id="form" name="form">
                        {{ csrf_field() }}
                         
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group {{ $errors->has('nom_pers_phys') ? ' has-error' : '' }}">
                              <p>Nom *</p>
                              <input onBlur="controlnom(form)" class="form-control form-control-sm {{ $errors->has('nom_pers_phys') ? ' is-invalid' : '' }}" value="{{ $pers->nom_pers_phys }}" type="text" name="nom_pers_phys" required>
                            </div>
                            @if($errors->has('nom_pers_phys'))
                              <p class="text-danger">{{ $errors->first('nom_pers_phys') }}</p>
                            @endif
                          </div>  
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Prenom *</p>
                              <input  onBlur="controlprenom()" class="form-control form-control-sm" type="text"  name="prenom_pers_phys" value="{{ $pers->prenom_pers_phys }}">
                            </div>
                          </div> 
                        </div>    
                        <div class="row">    
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('nom_pers_phys') ? ' has-error' : '' }}">
                              <p>Situation matrimoniale *</p>
                                <select class="form-control form-control-sm  {{ $errors->has('nom_pers_phys') ? 'is-invalid' : '' }}" value="{{ $pers->sit_mat }}" style="width: 100%;" name="sit_mat" required>
                                  <option class="select_first_option" value="" selected="">
                                          -- Choisissez dans la liste --
                                  </option>
                                  <option>MARIE(E)</option>
                                  <option>CELIBATAIRE</option>
                                  <option>DIVORCE(E)</option>
                                  <option>VEUF(VE)</option>
                                  <option>INCONNU</option>
                                
                                </select>
                            </div>  
                              @if($errors->has('sit_mat'))
                              <p class="text-danger">{{ $errors->first('sit_mat') }}</p>
                              @endif
                          </div>   
                          <div class="col-md-5">
                              <div class="form-group {{ $errors->has('date_naiss') ? ' has-error' : '' }}">
                                <p>Date de naissance *</p>
                                <input class="form-control form-control-sm {{ $errors->has('date_naiss') ? ' is-invalid' : '' }}" value="{{ $pers->date_naiss }}"type="date" id="date_naiss"  name="date_naiss" required>
                              </div>
                              @if($errors->has('date_naiss'))
                                <p class="text-danger">{{ $errors->first('date_naiss') }}</p>
                              @endif
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-5">
                            <div class="form-group {{ $errors->has('lieu_naiss') ? ' has-error' : '' }}">
                                <p>Lieu de naissance *</p>
                                <input onBlur="controlLieu_naiss()" class="form-control form-control-sm {{ $errors->has('lieu_naiss') ? ' is-invalid' : '' }}" value="{{ $pers->lieu_naiss }}" type="text" id="lieu_naiss" name="lieu_naiss">
                            </div>
                              @if($errors->has('lieu_naiss'))
                                <p class="text-danger">{{ $errors->first('lieu_naiss') }}</p>
                              @endif
                          </div>  
                          <div class="col-md-5">
                            <div class="form-group clearfix"> <br> 
                              <div class="icheck-success d-inline">
                                Sexe *  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="sexe" checked id="radioSuccess1" value="Masculin">
                                <label for="radioSuccess1">Masculin</label>
                              </div>
                              <div class="icheck-success d-inline">
                                <input type="radio" name="sexe" id="radioSuccess2" value="Feminin">
                                <label for="radioSuccess2">Feminin</label>
                              </div>
                            </div> 
                      </div>
                    </div>
                  </div>
              </div> 
                  <div class="col">
                    <div class="card shadow">
                      <div class="card-header">
                        <h3 class="card-title">Identité du contribuable</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                         
                        </div>
                      </div>
                      <div class="card-body">
                      
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group {{ $errors->has('cin') ? ' has-error' : '' }}">
                              <p>CIN *</p>
                              <input onBlur="controlecin(form)" class="form-control form-control-sm {{ $errors->has('cin') ? ' is-invalid' : '' }}" value="{{ $pers->cin }}" type="text" name="cin" required>
                            </div>
                            @if($errors->has('cin'))
                              <p class="text-danger">{{ $errors->first('cin') }}</p>
                            @endif
                            <p class="text-danger" id="cin"></p>
                          </div> 
                          <div class="col-md-6">
                            <div class="form-group {{ $errors->has('date_del_cin') ? ' has-error' : '' }}">
                              <p>Date de delivrance cin *</p>
                              <input class="form-control form-control-sm {{ $errors->has('date_del_cin') ? ' is-invalid' : '' }}" value="{{ $pers->date_del_cin }}" type="date" name="date_del_cin">
                            </div>
                            @if($errors->has('date_del_cin'))
                              <p class="text-danger">{{ $errors->first('date_del_cin') }}</p>
                            @endif 
                          </div> 
                        </div> 
                        <div class="row"> 
                          <div class="col-md-6">
                            <div class="form-group {{ $errors->has('lieu_del_cin') ? ' has-error' : '' }}">
                              <p>Lieu de delivrance cin *</p>
                              <input class="form-control form-control-sm {{ $errors->has('lieu_del_cin') ? ' is-invalid' : '' }}" value="{{ $pers->lieu_del_cin }}" type="text"  name="lieu_del_cin" >
                            </div>
                            @if($errors->has('lieu_del_cin'))
                              <p class="text-danger">{{ $errors->first('lieu_del_cin') }}</p>
                            @endif 
                          </div> 
                          <div class="col-md-6">
                            <div class="form-group {{ $errors->has('num_stat') ? ' has-error' : '' }}">
                            <p>Numero statistique *</p>
                            <input onBlur="controleNum_stat(form)" type="text" class="form-control form-control-sm {{ $errors->has('num_stat') ? ' is-invalid' : '' }}"  name="num_stat" value="{{$pers->num_stat}}" data-toggle="tooltip"  data-original-title="Si votre statistique est en attente, veuillez mettre 555555555555555555."  >
                            </div>  
                            @if($errors->has('num_stat'))
                              <p class="text-danger">{{ $errors->first('num_stat') }}</p>
                            @endif
                            <p class="text-danger" id="num_stat"></p>
                          </div>  
                        </div>  
                        <div class="row">   
                          <div class="col-md-6">
                            <div class="form-group">
                              <p>Date de delivrance statistique</p>
                              <input type="date" class="form-control form-control-sm"  value="{{ $pers->date_deliv_stat }}" name="date_deliv_stat">
                            </div> 
                          </div> 
                        </div>  
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card shadow">
                      <div class="card-header">
                        <h3 class="card-title">Siège du contribuable</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                         
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group {{ $errors->has('adrs') ? ' has-error' : '' }}">
                              <p>Adresse actuel *</p>
                              <input onBlur="controlAdresse()" class="form-control form-control-sm {{ $errors->has('adrs') ? ' is-invalid' : '' }}" type="text" name="adrs" value="{{$pers->adrs}}">
                            </div>
                            @if($errors->has('adrs'))
                              <p class="text-danger">{{ $errors->first('adrs') }}</p>
                            @endif
                          </div>  
                        </div>    
                      </div>  
                    </div>  
                  </div> 
                  <div class="col">
                    <div class="card shadow">
                      <div class="card-header">
                        <h3 class="card-title">Activité du contribuable</h3>

                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                         
                        </div>
                      </div>
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-5">
                              <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                <p>Description d' activités</p>
                                <textarea  class="form-control input-sm {{ $errors->has('description') ? 'is-invalid' : '' }}" value="{{ $pers->description }}"  name="description" data-toggle="tooltip" data-original-title="Lister par ordre d'importance toutes vos activités"></textarea>
                              </div>
                              @if($errors->has('description'))
                                <p class="text-danger">{{ $errors->first('description') }}</p>
                              @endif
                            </div>  
                            <div class="col-md-5">
                              <div class="form-group">
                                <p>Precision d'activité</p>
                                <textarea class="form-control input-sm"  name="precision" data-toggle="tooltip" value="{{ $pers->precision }}" data-placement="right" data-original-title="Si Transporteur, lister ici les numéros des moyens de transport. Si Importateur, Exportateur, Collecteur (ou autres), énumérer les principaux produits. Pour les titulaires de licence de boissons alcooliques, mentionner la catégorie, la référence et la date de décision."></textarea>
                              </div>
                            </div> 
                          </div>    
                          <div class="row">    
                            <div class="col-md-5">
                              <div class="form-group">
                                <p>Registre du commerce</p>
                                <input type="text" class="form-control form-control-sm"  name="reg_comm" value="{{ $pers->reg_comm }}">
                              </div>
                            </div>  
                            <div class="col-md-5">
                              <div class="form-group">
                                <p>Date de registre du commerce</p>
                                <input type="date" class="form-control form-control-sm"  name="date_reg_comm" value="{{ $pers->date_reg_comm }}">
                              </div>
                            </div> 
                          </div> 
                                
                          <div class="row">
                            <div class="col-md-5">
                              <div class="form-group {{ $errors->has('debut') ? ' has-error' : '' }}">
                                <p>Début de l'exercice comptable</p>
                                <input type="text" class="form-control form-control-sm {{ $errors->has('debut') ? 'is-invalid' : '' }}"  name="debut" value="{{ $pers->debut }}"  data-toggle="tooltip"  data-original-title="Changer cette valeur par défaut, si votre exercice ne débute pas le 1er Janvier.">
                                (JJ/MM)(JJ/MM)
                              </div>
                              @if($errors->has('debut'))
                                <p class="text-danger">{{ $errors->first('debut') }}</p>
                              @endif
                            </div>  
                            <div class="col-md-5">
                                <div class="form-group {{ $errors->has('cloture') ? 'has-error' : '' }}">
                                  <p>Clôture de l'exercice comptable</p>
                                  <input type="text" class="form-control form-control-sm {{ $errors->has('cloture') ? 'is-invalid' : '' }}"  name="cloture" value="{{ $pers->cloture }}">
                                  (JJ/MM)(JJ/MM)
                                </div>
                                @if($errors->has('cloture'))
                                  <p class="text-danger">{{ $errors->first('cloture') }}</p>
                                @endif
                            </div>  
                          </div> 
                          <div class="row">  
                            <div class="col-md-5">
                              <div class="form-group clearfix"> <br> 
                                <div class="icheck-success d-inline">
                                    Importateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="import" checked id="radioDanger1" value="Oui">
                                  <label for="radioDanger1">Oui</label>
                                </div>
                                <div class="icheck-success d-inline">
                                  <input type="radio" name="import" id="radioDanger2" value="Non">
                                  <label for="radioDanger2">Non</label>
                                </div>
                                @if($errors->has('import'))
                                  <p class="text-danger">{{ $errors->first('import') }}</p>
                                @endif
                              </div> 
                            </div><br> 
                            <div class="col-md-5">
                              <div class="form-group clearfix"> <br> 
                                <div class="icheck-success d-inline">
                                    Exportateur &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="export" checked id="radioPrimary1"value="Oui">
                                  <label for="radioPrimary1">Oui</label>
                                </div>
                                <div class="icheck-success d-inline">
                                  <input type="radio" name="export" id="radioPrimary2" value="Non">
                                  <label for="radioPrimary2">Non</label>
                                </div>
                                @if($errors->has('exportateur'))
                                  <p class="text-danger">{{ $errors->first('exportateur') }}</p>
                                @endif
                              </div> 
                            </div>
                          </div><br> 
                            
                          <div class="row">
                              <div class="col-md-5">
                                <div class="form-group  {{ $errors->has('nbr_salarier') ? ' has-error' : '' }}">
                                  <p>Nombre de salarié</p>
                                  <input type="text" class="form-control form-control-sm  {{ $errors->has('nbr_salarier') ? ' is-invalid' : '' }}" value="{{ $pers->nbr_salarier }}"  name="nbr_salarier" >
                                </div>
                                @if($errors->has('nbr_salarier'))
                                  <p class="text-danger">{{ $errors->first('nbr_salarier') }}</p>
                                @endif
                              </div>
                           
                            <div class="col-md-5">
                              <div class="form-group">
                                <p>Regime fiscal *</p>
                                    <select class="form-control form-control-sm" style="width: 100%;" name="reg_fisc" value="{{ $pers->reg_fisc }}">
                                      <option class="select_first_option" value="" selected="">
                                                -- Choisissez dans la liste --
                                      </option>
                                      <option>DROIT COMMUN</option>

                                      <option>ZONE FRANCHE</option>

                                      <option>CODE DES INVESTISSEMENTS</option>

                                      <option>LGIM</option>

                                      <option>CONVENTION D'ETABLISSEMENT</option>

                                      <option>AUTRES</option>
                                    
                                    </select>
                              </div>
                            </div> 
                          </div> 
                          <div class="row">    
                            <div class="col-md-5">
                              <div class="form-group">
                                <p>Capital en Ar *</p>
                                <input class="form-control form-control-sm" type="text" id="capital" value="{{ $pers->capital }}" name="capital">
                              </div>
                            </div>
                            <script>
                              function myFunction() {
                                  // Get the checkbox
                                  var checkBox = document.getElementById("dispo");
                                  var checkBox1 = document.getElementById("indispo");
                                  // Get the output text
                                  var text = document.getElementById("soc");

                                  // If the checkbox is checked, display the output text
                                  if (checkBox.checked == true){
                                    document.getElementById("ribc").value = "";
                                    checkBox1.checked == false;
                                  }  
                                  if (checkBox1.checked == true){
                                    document.getElementById("ribc").value = "11111-11111-11111111111-11"
                                    checkBox.checked = false;
                                  }  
                                }
                            </script>
                            <div class="col-md-5">
                              <div class="form-group  {{ $errors->has('rib') ? ' has-error' : '' }}">
                               RIB * &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp
                                <label for="radios-0">
                                  <input type="checkbox" onclick="myFunction()" class="yes" style="margin-right:5px"  id="dispo" value="3">Disponible
                                </label>
                                <label  for="radios-0">
                                  <input type="checkbox" onclick="myFunction()" class="yes" style="margin-right:5px"  id="indispo" value="3">Pas encore disponible
                                </label>
                                <input onBlur="controleRib(form)" id="ribc"  data-toggle="tooltip" data-placement="bottom" data-original-title="Numéro de compte bancaire. Obligatoire pour les personnes morales ou les assujettis à la TVA." class="form-control form-control-sm  {{ $errors->has('rib') ? ' is-invalid' : '' }}" type="text"  name="rib" value="{{ $pers->rib }}">
                              </div>
                              <p class="text-danger" id="rib"></p>
                        
                            </div> 
                          </div>
                        </div>
                      </div>
                    </div>
              </div>  
              <div class="col-12">
                    <button type="reset" class="btn btn-secondary">Annuler</button>
                    <input type="submit" value="Enregistrer" class="btn btn-outline-success float-right">
              </div> 
              </form> 
        </div>  <br>
      </section>    
  
  </div>
  <script language="javascript">
      function controlnom(form){
          var test = document.form.nom_pers_phys.value;
          var nb = test.length;
          if(nb>=1){
            var maj=test.toUpperCase();
            document.form.nom_pers_phys.value=maj;
          }
        }

        function controlprenom(){
            var str=document.form.prenom_pers_phys.value;
            var x=0;
            var y=1;
            var txt=(str.substring(x,y)).toUpperCase();
            var prnm=txt;
            for(var i=0;i<str.length;i++){
              x=y;
              if(str.charAt(i)==" "){
                
                prnm=prnm+str.substring(y,i)+" ";
                txt=(str.substring(i+1,i+2)).toUpperCase();
                y=i+2;
                prnm=prnm+txt;
              }       
                
            }
                prnm=prnm+str.substring(x,str.length)+" ";
            document.form.prenom_pers_phys.value=prnm;
        }
        
        function controlLieu_naiss(){
            var str=document.form.lieu_naiss.value;
            var x=0;
            var y=1;
            var txt=(str.substring(x,y)).toUpperCase();
            var prnm=txt;
            for(var i=0;i<str.length;i++){
              x=y;
              if(str.charAt(i)==" "){
                
                prnm=prnm+str.substring(y,i)+" ";
                txt=(str.substring(i+1,i+2)).toUpperCase();
                y=i+2;
                prnm=prnm+txt;
              }       
                
            }
                prnm=prnm+str.substring(x,str.length)+" ";
            document.form.lieu_naiss.value=prnm;
        }

        function controlecin(form){
            var val=document.form.cin.value;
            if(isNaN(val)==false){
              document.getElementById('cin').innerHTML="";
                var n=val.length;
                if (n==12 || n==""){
                    var pc = val.substring(0,1);
                    document.getElementById('cin').innerHTML="";
                    if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6 || pc==""){
                        var cs=val.substring(5,6);
                        document.getElementById('cin').innerHTML="";
                        if(cs==1 || cs==2 ){
                            document.form.cin.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                        }else{
                          document.getElementById('cin').innerHTML="";
                        }
                    }else{
                      document.getElementById('cin').innerHTML="CIN invalide à Madagascar ";
                      
                    }
                }else{
                    document.getElementById('cin').innerHTML ="votre CIN est incorrect";
            
                }
            }else{
              document.getElementById('cin').innerHTML=" ";
            }
        }
      
        function controleNum_stat(form){
            var val=document.form.num_stat.value;
            if(isNaN(val)==false){
              document.getElementById('num_stat').innerHTML="";
                var n=val.length;
                if (n!=17){
                  document.getElementById('num_stat').innerHTML="Numéro statistique invalide";
                } else {
                  document.form.num_stat.value=val.substring(0,5) + " " + val.substring(5,7) + " " + val.substring(7,11) + " " + val.substring(11,12)  + " " + val.substring(12,17);
                  
                }  
            }
        }
      
        function controlAdresse(){
            var str=document.form.adrs.value;
            var x=0;
            var y=1;
            var txt=(str.substring(x,y)).toUpperCase();
            var prnm=txt;
            for(var i=0;i<str.length;i++){
              x=y;
              if(str.charAt(i)==" "){
                
                prnm=prnm+str.substring(y,i)+" ";
                txt=(str.substring(i+1,i+2)).toUpperCase();
                y=i+2;
                prnm=prnm+txt;
              }       
                
            }
                prnm=prnm+str.substring(x,str.length)+" ";
            document.form.adrs.value=prnm;
        }

        function controleRib(form){
            var val=document.form.rib.value;
            if(isNaN(val)==false){
              
                var n=val.length;
                if (n==23){
                  document.form.rib.value=val.substring(0,5) + "-" + val.substring(5,10) + "-" + val.substring(10,21) + "-" + val.substring(21,23);
                
                }else {
                  document.form.rib.value=""
                }
            }else{
              document.getElementById('rib').innerHTML ="RIB doit être en chiffre";
            
            }
          
        }
  </script>
@endsection