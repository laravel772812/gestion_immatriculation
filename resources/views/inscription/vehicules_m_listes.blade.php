@extends('layouts.application')

@section('content')
<div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Associer</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Renseignement sur les vehicules du contribuables</small></h1></center>
                </div>
              
                  <div class="table-responsive">
                
                          @if(Session::has('success'))
                                <div class="col-md-6">
                                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <strong>Succés! </button> {{Session::get('success')}}
                                  </div>
                                </div>
                          @endif
                          
                          @if(Session::has('successs'))
                                <div class="col-md-6">
                                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <strong>Succés! </button> {{Session::get('successs')}}
                                  </div>
                                </div>
                          @endif
                          <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                              <tr>
                              <th>Id personne</th>
                                <th>Numéro vehicule</th>
                                <th>Marque</th>
                                <th>Genre</th>
                                <th>Type</th>
                                <th>Puissance</th>
                                <th>Nombre de place</th>
                                <th>Charge</th>
                                <th>Poid</th>
                                <th>Date de circulation</th>
                                <th>A usage professionnel</th>
                                <th>Date de début</th>
                                <th>Exploitation</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            
                       
                        
                                <tr scope="row">
                                      <td >{{ $vehicule->pers_moral_id }}</td>
                                      <td >{{ $vehicule->num_veh_m }}</td>
                                      <td >{{ $vehicule->marque_m }}</td>
                                      <td >{{ $vehicule->genre_m }}</td>
                                      <td >{{ $vehicule->type_m }}</td>
                                      <td>{{ $vehicule->puissance_m }}</td>
                                      <td>{{ $vehicule->nbr_place_m }}</td>
                                      <td>{{ $vehicule->charge_m }}</td>
                                      <td>{{ $vehicule->poid_m}}</td>
                                      <td>{{ $vehicule->date_circ_m}}</td>
                                      <td>{{ $vehicule->usage_proff_m}}</td>
                                      <td>{{ $vehicule->date_debut}}</td>
                                      <td>{{ $vehicule->exploitation_m}}</td>
                                      
                                     
                                </tr>

                            
                              
                            
                                
                            
                            
                              </tbody>
                          </table>
                      
                  </div>
            
                <div class="card-footer py-4">
                  <center><a href="{{ route('vehicules_m.add') }}" class="btn btn-outline-success">Plus des vehicules</a></center>
                </div>
                <div class="col-12">
              
                  <a href="{{ route('interlocuteurs_m.add') }}" class="btn btn-outline-success float-right">Suivant</a>
                </div>
              </div> 
            </div> 
           
          </div>
        </div>
      </section>
</div>
@endsection