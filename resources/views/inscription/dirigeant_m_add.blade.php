@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
                <li class="breadcrumb-item active">associer</li>
                <li class="breadcrumb-item active">etablissement</li>
                <li class="breadcrumb-item active">dirigeant</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur les dirigeants de la societé</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                  </div>
                </div>
                <div class="card-body">
            
                    <form action="{{ route('dirigeants_m.adds') }}" method="post" id="ajout" name="ajout">
                    {{ csrf_field() }}
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <p>Nom commercial</p>
                        <select name="etablissement_m_id"    id="etablissement" class="form-control form-control-sm">
                        
                            <option value="{{ $etablissements->id }}">{{ $etablissements->nom_etabl_m }} </option>
                          
                        </select>
                      
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('nom_dir_m') ? 'has-error' : ''}}">
                        <p>Nom et Prenom</p>
                        <input  onblur="controlenom(ajout)" class="form-control form-control-sm  {{ $errors->has('nom_dir_m') ? 'is-invalid' : ''}}" type="text" name="nom_dir_m">
                      </div>
                      @if($errors->has('nom_dir_m'))
                        <p class="text-danger">{{ $errors->first('nom_dir_m') }}</p>
                      @endif
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('fonction_m') ? 'has-error' : ''}}">
                          <p>Fonction</p>
                          <input class="form-control form-control-sm  {{ $errors->has('fonction_m') ? 'is-invalid' : ''}}" type="text"  name="fonction_m">
                      </div>
                      @if($errors->has('fonction_m'))
                        <p class="text-danger">{{ $errors->first('fonction_m') }}</p>
                      @endif
                    </div> 
                </div>    
                  
                <div class="row">    
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('cin_dir_m') ? 'has-error' : ''}}">
                          <p>CIN</p>
                          <input  onblur="controlecin(ajout)" class="form-control form-control-sm  {{ $errors->has('cin_dir_m') ? 'is-invalid' : ''}}" type="text"  name="cin_dir_m">
                      </div>
                      @if($errors->has('cin_dir_m'))
                        <p class="text-danger">{{ $errors->first('cin_dir_m') }}</p>
                      @endif
                      <p id="cin_dir_m" class="text-danger"></p>
                    </div>  
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('adrs_dir_m') ? 'has-error' : ''}}">
                        <p>Adresse</p>
                        <input class="form-control form-control-sm  {{ $errors->has('adrs_dir_m') ? 'is-invalid' : ''}}" type="text"   name="adrs_dir_m">
                      </div>
                      @if($errors->has('adrs_dir_m'))
                        <p class="text-danger">{{ $errors->first('adrs_dir_m') }}</p>
                      @endif
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-5">
                      <div class="form-group  {{ $errors->has('email_dir_m') ? 'has-error' : ''}}">
                        <p>Email</p>
                        <input class="form-control form-control-sm  {{ $errors->has('email_dir_m') ? 'is-invalid' : ''}}" type="email"  name="email_dir_m">
                      </div>
                      @if($errors->has('email_dir_m'))
                        <p class="text-danger">{{ $errors->first('email_dir_m') }}</p>
                      @endif
                    </div>  
                    
                    
                    <div class="col-md-5">
                        <div class="form-group  {{ $errors->has('tel_m') ? 'has-error' : ''}}">
                          <p>Tel</p>
                          <input onblur="controletel(ajout)" class="form-control form-control-sm  {{ $errors->has('tel_m') ? 'is-invalid' : ''}}" type="text"  name="tel_m">
                        </div>
                        @if($errors->has('tel_m'))
                        <p class="text-danger">{{ $errors->first('tel_m') }}</p>
                        @endif
                      <p class="text-danger" id="tel_m"></p>
                    </div> 
                    
                </div>  
                <script>
                  function myFunction() {
                      // Get the checkbox
                      var checkBox = document.getElementById("radioDanger1");
                      var checkBox1 = document.getElementById("radioDanger2");
                      // Get the output text
                      var text = document.getElementById("act");

                      // If the checkbox is checked, display the output text
                      if (checkBox.checked == true){
                      
                        act.style.display = "block";
                      } 
                      if (checkBox1.checked == true){
                      
                      act.style.display = "none";
                    }   
                      
                    }
                </script> 
                <div class="row">  
                    <div class="col-md-5">
                 
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline">
                            Autre activité &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" onclick="myFunction()" name="autre_act"  id="radioDanger1" value="Oui">
                          <label for="radioDanger1">Oui</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" onclick="myFunction()" name="autre_act" checked id="radioDanger2" value="Non">
                          <label for="radioDanger2">Non</label>
                        </div>
                       
                      </div> 
                    </div><br> 
                    
                </div>
                <div class="row" id="act" style="display:none">  
                    <div class="col-md-5">
                      <div class="form-group">
                          <p>Description activité</p>
                          <input class="form-control form-control-sm" type="text"  name="activite_dir_m">
                        </div>
                    </div> 
                      
                    <div class="col-md-5">
                      <div class="form-group">
                          <p>NIF autre activité</p>
                          <input class="form-control form-control-sm" type="text"  name="nif_activite_dir_m">
                        </div>
                    </div> 
                </div>
               
              </div>
            </div>
          </div>
          <div class="col-12">
            
            <input type="submit" value="Enregistrer" class="btn btn-success float-right">
          </div>
      </form> 
    </div>  <br>
  </section>    
</div>
<script>
 
  function controlenom(ajout){
  var test = document.ajout.nom_dir_m.value;
  var nb = test.length;
  if(nb>=1){
      var maj=test.toUpperCase();
      document.ajout.nom_dir_m.value=maj;
  }
 }
 function controlecin(ajout){
      var val=document.ajout.cin_dir_m.value;
      if(isNaN(val)==false){
      
          var n=val.length;
          if (n==12){
              var pc = val.substring(0,1);
           
              if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6){
                  var cs=val.substring(5,6);
               
                  if(cs==1 || cs==2 ){
                      document.ajout.cin_dir_m.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                  }else{
                    document.getElementById('cin_dir_m').innerHTML="votre CIN est invalide";
                  }
              }else{
                
                document.geElementById('cin_dir_m').innerHTML="votre CIN  est incorrect";
           
                
              }
          }else{
              document.getElementById('cin_dir_m').innerHTML ="votre CIN  est incorrect";
      
          }
      }else{
        document.getElementById('cin_dir_m').innerHTML="CIN doit être en chiffre ";
      }
  }
  function controletel(ajout){
    var phon=document.ajout.tel_m.value;
    if(isNaN(phon)==false){
        var isa=phon.length;
      
        if(isa==10 ){
            var cs=phon.substring(0,3);
            
            if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                document.ajout.tel_m.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
            }else{
              document.getElementById('tel_m').innerHTML="Operateur inconnu à Madagascar";
            }
        }else{
          
          document.getElementById('tel_m').innerHTML="le numero telephone est invalide";
      
        }
    }else{
      document.getElementById('tel_m').innerHTML="le numero telephone est invalide";
        
    }
  }
</script>
@endsection