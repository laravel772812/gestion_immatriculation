@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">renseignement</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Principaux renseignement sur l'etablissement</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                  <form action="{{ route('etablissements.adds') }}" method="post" id="ajout" name="ajout">
                      {{ csrf_field() }}
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Proprietaire etablissement</p>
                              <select name="pers_phys_id"    id="pers_phys" class="form-control form-control-sm">
                                <option value="{{ $pers->id }}">{{ $pers->nom_pers_phys }} </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Nom commercial</p>
                              <input  class="form-control form-control-sm" type="text" name="nom_etabl">
                            </div>
                          </div> 
                        </div> 
                        <div class="row">
                          <div class="col-md-5">
                            <div class="form-group {{ $errors->has('date_ouvert') ? 'has-error' : ''}}">
                                <p>Date ouverture</p>
                                <input class="form-control form-control-sm {{ $errors->has('date_ouvert') ? 'is-invalid' : ''}}" type="date"  name="date_ouvert">
                            </div>
                            @if($errors->has('date_ouvert'))
                              <p class="text-danger">{{ $errors->first('date_ouvert') }}</p>
                            @endif
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('tel') ? 'has-error' : ''}}">
                                <p>Telephone 1</p>
                                <input onBlur="controletelephone(ajout)" class="form-control form-control-sm  {{ $errors->has('tel') ? 'is-invalid' : ''}}" type="text"  name="tel">
                            </div>
                            @if($errors->has('tel'))
                              <p class="text-danger">{{ $errors->first('tel') }}</p>
                            @endif
                            <p class="text-danger" id="tel"></p>
                          </div> 
                        </div> 
                        <div class="row">  
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Autre telephone</p>
                              <input class="form-control form-control-sm" type="text"   name="autre_tel">
                            </div>
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Fax</p>
                                <input class="form-control form-control-sm" type="text"  name="fax">
                            </div>
                          </div>  
                        </div> 
                        <div class="row"> 
                          <div class="col-md-5">
                            <div class="form-group  {{ $errors->has('email') ? 'has-error' : ''}}">
                              <p>Email</p>
                              <input class="form-control form-control-sm  {{ $errors->has('email') ? 'is-invalid' : ''}}" type="email"  name="email">
                            </div>
                              @if($errors->has('email'))
                                <p class="text-danger">{{ $errors->first('email') }}</p>
                              @endif
                          </div> 
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Exportateur</p>
                              <select class="form-control form-control-sm"  name="exportateur">
                                <option>Oui</option>
                                <option>Non</opton>
                              </select>
                            </div>
                          </div> 
                        </div> 
                        <div class="row">           
                          <div class="col-md-5">
                            <div class="form-group">
                              <p>Importateur</p>
                              <select class="form-control form-control-sm"  name="importateur">
                                <option>Oui</option>
                                <option>Non</opton>
                              </select>
                            </div>
                          </div> 
                      </div>
              </div>
            </div>
            <div class="col">            
              <div class="card shadow">
                <div class="card-header">
                  <h3 class="card-title">Proprietaire d'etablissement</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <script>
                  function myFunction() {
                      // Get the checkbox
                      var checkBox = document.getElementById("radioSuccess1");
                      var checkBox1 = document.getElementById("radioSuccess2");
                      // Get the output text
                      var text = document.getElementById("soc");

                      // If the checkbox is checked, display the output text
                      if (checkBox.checked == true){
                        par.style.display = "none";
                        soc.style.display = "block";
                      }  
                      if (checkBox1.checked == true){
                        par.style.display = "block";
                        soc.style.display = "none";
                      }  
                    }
                </script>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group clearfix"> <br> 
                        <div class="icheck-success d-inline" >
                          Type proprietaire  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input  type="radio" onclick="myFunction()" name="type_prop" id="radioSuccess1" value="Societé">
                          <label for="radioSuccess1">Societé</label>
                        </div>
                        <div class="icheck-success d-inline">
                          <input type="radio" name="type_prop" checked  id="radioSuccess2" value="Particulier" onclick="myFunction()">
                          <label for="radioSuccess2">Particulier</label>
                        </div>
                      </div> 
                    </div>
                    <div class="col-md-5">
                      <div class="form-group " id="soc" style="display:block;" >
                        <p>NIF proprietaire(Societé)</p>
                        <input class="form-control form-control-sm"  type="text" name="nif_prop">
                      </div>
                    </div> 
                    </div> 
                    <div id="par" style="display:block" >
                     
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Nom proprietaire</p>
                            <input onBlur="controlnom(ajout)" class="form-control form-control-sm" type="text"  name="nom_prop">
                          </div>
                        </div> 
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>CIN proprietaire</p>
                            <input onBlur="controlecin_prop(ajout)" class="form-control form-control-sm" type="text"  name="cin_prop">
                          </div>
                          <p class="text-danger"id="cin_prop"></p>
                        </div> 
                      </div>
                      <div class="row">  
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Adresse proprietaire</p>
                            <input class="form-control form-control-sm" type="text"  name="adrs_prop">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <p>Telephone proprietaire</p>
                            <input onBlur="controletelephone_prop(ajout)"class="form-control form-control-sm" type="text"  name="tel_prop">
                            <p  class="text-danger" id=tel_prop></p>
                          </div>
                        </div>
                      </div> 
                    </div> 
                </div>
              </div>
            </div>
            <div class="col-12">
             
              <input type="submit" value="Enregistrer" class="btn btn-success float-right">
          </div>
          </form> 
         </div> 
        </div> 
      </section><br> 
</div>
  <script>
      function controletelephone(ajout){
        var phon=document.ajout.tel.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs=phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.ajout.tel.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('tel').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('tel').innerHTML="";
              document.ajout.tel.value="";
            }
        }else{
          document.getElementById('tel').innerHTML="le numero telephone est invalide";
            document.ajout.tel.value="";
        }
      }
      function controlnom(ajout){
      var test = document.ajout.nom_prop.value;
      var nb = test.length;
      if(nb>=1){
          var maj=test.toUpperCase();
          document.ajout.nom_prop.value=maj;
      }
    }
    function controlecin_prop(ajout){
          var val=document.ajout.cin_prop.value;
          if(isNaN(val)==false){
            document.getElementById('cin_prop').innerHTML="";
              var n=val.length;
              if (n==12 || n==""){
                  var pc = val.substring(0,1);
                  document.getElementById('cin_prop').innerHTML="";
                  if(pc==1 || pc==2 || pc==3 || pc==4 || pc==5 || pc==6){
                      var cs=val.substring(5,6);
                      document.getElementById('cin_prop').innerHTML="";
                      if(cs==1 || cs==2 ){
                          document.ajout.cin_prop.value=val.substring(0,3) + " " + val.substring(3,6) + " " + val.substring(6,9) + " " + val.substring(9,12);
                      }else{
                        document.getElementById('cin_prop').innerHTML="votre CIN est invalide";
                      }
                  }else{
                    
                    document.getElementById('cin_prop').innerHTML="";
                    document.ajout.cin_prop.value="";
                    
                  }
              }else{
                  document.getElementById('cin_prop').innerHTML ="votre CIN  est incorrect";
          
              }
          }else{
            document.getElementById('cin_prop').innerHTML="CIN doit être en chiffre ";
          }
    }
      function controletelephone_prop(ajout){
        var phon=document.ajout.tel_prop.value;
        if(isNaN(phon)==false){
            var isa=phon.length;
          
            if(isa==10 ){
                var cs=phon.substring(0,3);
                
                if( cs=="032" || cs=="033" || cs=="034" || cs=="039" ){
                    document.ajout.tel_prop.value= phon.substring(0,3) +" "+ phon.substring(3,5) +" "+ phon.substring(5,8) +" "+ phon.substring(8,10);
                }else{
                  document.getElementById('tel_prop').innerHTML="Operateur inconnu à Madagascar";
                }
            }else{
              
              document.getElementById('tel_prop').innerHTML="";
              document.ajout.tel_prop.value="";
            }
        }else{
          document.getElementById('tel_prop').innerHTML="le numero telephone est invalide";
            
        }
      }
  </script>
@endsection