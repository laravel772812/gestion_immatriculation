@extends('layouts.application')

@section('content')
<div class="content-wrapper">
     <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Confirmation</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active">confirmation</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
           <div class="col">
              <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Confirmation</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  
                </div>
              </div>
              <div class="card-body">
                 <form action="{{ route('references_m.update',['id'=>$ref->id] )}}" method="post">
                  {{ csrf_field() }}
                    @if(Session::has('success'))
                      <div class="col-md-6">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                          
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>Succés! </button> {{Session::get('success')}}
                        </div>
                      </div>
                    @endif

                   
                    @if(Auth::guard('admin')->check())
                    <div class="col-md-5">
                        <div class="form-group">
                            <p>Entrer nouveau NIF pour la reference n° : {{ $ref->id }}</p>
                            <input type="text" name="nouveau_nif_m"  value="{{ $ref->nouveau_nif_m }}"  class="form-control form-control-sm">
                           
                        </div>  
                        @if($errors->has('nouveau_nif_m'))
                          <p class="text-danger">{{ $errors->first('nouveau_nif_m') }}</p>
                        @endif    
                    </div>
                    @endif
                    <div class="col-12" >
                      <a href="{{route('references_m.liste')}}" class="btn btn-secondary">Retour</a>
                      <input type="submit" value="Valider" class="btn btn-success float-right">
                    </div>              
                </div>
              </div>
            </div>
                         
           
            </form> 
            </form> 
        </div>  <br>
      </section>    
  
  </div>

@endsection