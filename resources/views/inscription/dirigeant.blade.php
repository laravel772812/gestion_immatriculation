@extends('layouts.dash')

@section('content')
<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Tables</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{asset('plug/img/theme/team-4-800x800.jpg')}}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">Jessica Jones</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                      <span class="h2 font-weight-bold mb-0">2,356</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 3.48%</span>
                    <span class="text-nowrap">Since last week</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                      <span class="h2 font-weight-bold mb-0">924</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 1.10%</span>
                    <span class="text-nowrap">Since yesterday</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                      <span class="h2 font-weight-bold mb-0">49,65%</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
    
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Procedure de demande d'immatriculation fiscal</h3>
            </div>
            <div class="card card-primary card-outline">
      
          <div class="card-body">
            
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('inscription.persPhys') }}" >Principaux renseignement</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active"  href="#" >Activité</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#siege">Siège</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#etablissement">Etablissement</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#dirigeant">Dirigeant</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#vehicule">Vehicule</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#interlocuteur">Interlocuteur</a>
              </li>
            </ul>
            <div class="tab-content">
             
              
              <div class="tab-pane fade show active" id="persPhys" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                <br> 
                  
                <div class="col-lg-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                    <h4>Principaux renseignement sur le contribuable</h4>  <br>
                            <form action="{{ route('inscription.activite') }}" method="post">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Nom</p>
                                      <input class="form-control form-control-sm" type="text" name="nom_pers_phys">
                                    </div>
                                    @if($errors->has('nom_pers_phys'))
                                      <p class="text-danger">{{ $errors->first('nom_pers_phys') }}</p>
                                    @endif
                                  </div>  
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Prenom</p>
                                      <input class="form-control form-control-sm" type="text"  name="prenom_pers_phys">
                                    </div>
                                    @if($errors->has('prenom_pers_phys'))
                                      <p class="text-danger">{{ $errors->first('prenom_pers_phys') }}</p>
                                    @endif
                                  </div> 
                              </div>    
                                
                               <div class="row">    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Situation matrimoniale</p>
                                      <select class="form-control form-control-sm" style="width: 100%;" name="sit_mat">
                                        <option selected="selected">--Choisissez dans la liste--</option>
                                        <option>MARIE(E)</option>
                                        <option>CELIBATAIRE</option>
                                        <option>DIVORCE(E)</option>
                                        <option>VEUF(VE)</option>
                                        <option>INCONNU</option>
                                      
                                      </select>
                                    </div>  
                                  
                                  </div>   
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de naissance</p>
                                      <input class="form-control form-control-sm" type="date" id="date_naiss"  name="date_naiss">
                                    </div>
                                    @if($errors->has('date_naiss'))
                                      <p class="text-danger">{{ $errors->first('date_naiss') }}</p>
                                    @endif
                                  </div> 
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Lieu de naissance</p>
                                      <input class="form-control form-control-sm" type="text" id="lieu_naiss" name="lieu_naiss">
                                    </div>
                                    @if($errors->has('lieu_naiss'))
                                      <p class="text-danger">{{ $errors->first('lieu_naiss') }}</p>
                                    @endif
                                  </div>  
                                 
                                 
                                  <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                             Sexe  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="sexe" checked id="radioSuccess1" value="Masculin">
                                            <label for="radioSuccess1">Masculin</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio" name="sexe" id="radioSuccess2" value="Feminin">
                                            <label for="radioSuccess2">Feminin</label>
                                          </div>
                                        </div> 
                                  </div><br> 
                                

                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-12 grid-margin stretch-card">
                            <div class="card">
                              <div class="card-body">
                                <h4>Identité  du contribuable</h4>  <br>
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>CIN ou Numéro passeport ou carte de résident</p>
                                      <input class="form-control form-control-sm" type="text" name="cin">
                                    </div>
                                    @if($errors->has('cin'))
                                      <p class="text-danger">{{ $errors->first('cin') }}</p>
                                    @endif
                                  </div> 
                                 
                           
                               
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de delivrance cin (passeport ou carte de résident)</p>
                                      <input class="form-control form-control-sm" type="date" name="date_del_cin">
                                    </div>
                                    @if($errors->has('date_del_cin'))
                                      <p class="text-danger">{{ $errors->first('date_del_cin') }}</p>
                                    @endif 
                                  </div> 
                                  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Lieu de delivrance cin (passeport ou carte de résident)</p>
                                      <input class="form-control form-control-sm" type="text"  name="lieu_del_cin">
                                    </div>
                                    @if($errors->has('lieu_del_cin'))
                                      <p class="text-danger">{{ $errors->first('lieu_del_cin') }}</p>
                                    @endif 
                                  </div> 
                                 
                                 
                                  </div>
                              </div>
                          </div>        
                          <div class="col-lg-12 grid-margin stretch-card">
                            <div class="card">
                              <div class="card-body">
                              
                                <h4>Regime fiscal du contribuable</h4>  <br>        
                                 <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Regime fiscal</p>
                                      <input class="form-control form-control-sm" type="text" id="reg_fisc" name="reg_fisc">
                                    </div>
                                  </div>  
                                 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>date agrément</p>
                                      <input class="form-control form-control-sm" type="date" id="date_agre" name="date_agre">
                                    </div>
                                  </div> 
                                 </div> 
                                
                                 <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Ref agrément</p>
                                      <input class="form-control form-control-sm" type="text" id="ref_agre" name="ref_agre">
                                    </div>
                                  </div> 
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Periode de grace</p>
                                      <input class="form-control form-control-sm" type="text" id="peri_grace" name="peri_grace">
                                    </div>
                                  </div> 
                                 </div> 
                                
                                
                                <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de creation</p>
                                      <input class="form-control form-control-sm" type="date" id="date_creation" name="date_creation">
                                    </div>
                                  </div> 
                                
                                  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Capital</p>
                                      <input class="form-control form-control-sm" type="text" id="capital" name="capital">
                                    </div>
                                  </div>
                                </div> 
                                <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>RIB</p>
                                      <input class="form-control form-control-sm" type="text" id="rib" name="rib">
                                    </div>
                                  </div> 
                                </div> 
                                 
                              </div>
                          </div> 
                                  
                                  <div class="row">
                                    <div class="col-sm-3">
                                      <button type="reset" class="btn btn-danger btn-block">
                                        <i class="mdi  mdi-refresh"></i>
                                          Annuler
                                        </button>
                                    </div>
                                    <div class="col-sm-3">
                                      <button type="submit" class="btn btn-success btn-block">
                                        <i class="mdi mdi-file-document"></i>
                                            Enregister
                                      </button>
                                    </div>
                                   
                                  </div>
                                
                                </form> 
                            
                                @if(Session::has('success'))
                                  <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="close"> 
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>succées! {{ Session::get('success') }}</strong>
                                  </div>
                                @endif
                      </div>      
                      </div>   

              </div>
       
     
              
             

            </div>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>

  @endsection@extends('layouts.dash')

@section('content')
<div class="main-content">
    <!-- Navbar -->
    <nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="../index.html">Tables</a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{asset('plug/img/theme/team-4-800x800.jpg')}}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">Jessica Jones</span>
                </div>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="../examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#!" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </nav>
    <!-- End Navbar -->
    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
      <div class="container-fluid">
        <div class="header-body">
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Traffic</h5>
                      <span class="h2 font-weight-bold mb-0">350,897</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                        <i class="fas fa-chart-bar"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 3.48%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">New users</h5>
                      <span class="h2 font-weight-bold mb-0">2,356</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                        <i class="fas fa-chart-pie"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-danger mr-2"><i class="fas fa-arrow-down"></i> 3.48%</span>
                    <span class="text-nowrap">Since last week</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Sales</h5>
                      <span class="h2 font-weight-bold mb-0">924</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                        <i class="fas fa-users"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-warning mr-2"><i class="fas fa-arrow-down"></i> 1.10%</span>
                    <span class="text-nowrap">Since yesterday</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6">
              <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Performance</h5>
                      <span class="h2 font-weight-bold mb-0">49,65%</span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                        <i class="fas fa-percent"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-muted text-sm">
                    <span class="text-success mr-2"><i class="fas fa-arrow-up"></i> 12%</span>
                    <span class="text-nowrap">Since last month</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--7">
    
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Procedure de demande d'immatriculation fiscal</h3>
            </div>
            <div class="card card-primary card-outline">
      
          <div class="card-body">
            
            <ul class="nav nav-tabs">
              <li class="nav-item">
                <a class="nav-link active" href="{{ route('inscription.persPhys') }}" >Principaux renseignement</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active"  href="#" >Activité</a>
              </li>
             
              <li class="nav-item">
                <a class="nav-link active" href="#etablissement">Etablissement</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#dirigeant">Dirigeant</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#vehicule">Vehicule</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#interlocuteur">Interlocuteur</a>
              </li>
            </ul>
            <div class="tab-content">
             
              
              <div class="tab-pane fade show active" id="persPhys" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
                <br> 
                  
                <div class="col-lg-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                    <h4>Principaux renseignement sur le contribuable</h4>  <br>
                            <form action="{{ route('inscription.activite') }}" method="post">
                              {{ csrf_field() }}
                              <div class="row">
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Nom</p>
                                      <input class="form-control form-control-sm" type="text" name="nom_pers_phys">
                                    </div>
                                    @if($errors->has('nom_pers_phys'))
                                      <p class="text-danger">{{ $errors->first('nom_pers_phys') }}</p>
                                    @endif
                                  </div>  
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Prenom</p>
                                      <input class="form-control form-control-sm" type="text"  name="prenom_pers_phys">
                                    </div>
                                    @if($errors->has('prenom_pers_phys'))
                                      <p class="text-danger">{{ $errors->first('prenom_pers_phys') }}</p>
                                    @endif
                                  </div> 
                              </div>    
                                
                               <div class="row">    
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Situation matrimoniale</p>
                                      <select class="form-control form-control-sm" style="width: 100%;" name="sit_mat">
                                        <option selected="selected">--Choisissez dans la liste--</option>
                                        <option>MARIE(E)</option>
                                        <option>CELIBATAIRE</option>
                                        <option>DIVORCE(E)</option>
                                        <option>VEUF(VE)</option>
                                        <option>INCONNU</option>
                                      
                                      </select>
                                    </div>  
                                  
                                  </div>   
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de naissance</p>
                                      <input class="form-control form-control-sm" type="date" id="date_naiss"  name="date_naiss">
                                    </div>
                                    @if($errors->has('date_naiss'))
                                      <p class="text-danger">{{ $errors->first('date_naiss') }}</p>
                                    @endif
                                  </div> 
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Lieu de naissance</p>
                                      <input class="form-control form-control-sm" type="text" id="lieu_naiss" name="lieu_naiss">
                                    </div>
                                    @if($errors->has('lieu_naiss'))
                                      <p class="text-danger">{{ $errors->first('lieu_naiss') }}</p>
                                    @endif
                                  </div>  
                                 
                                 
                                  <div class="col-md-5">
                                        <div class="form-group clearfix"> <br> 
                                          <div class="icheck-success d-inline">
                                             Sexe  &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp <input type="radio" name="sexe" checked id="radioSuccess1" value="Masculin">
                                            <label for="radioSuccess1">Masculin</label>
                                          </div>
                                          <div class="icheck-success d-inline">
                                            <input type="radio" name="sexe" id="radioSuccess2" value="Feminin">
                                            <label for="radioSuccess2">Feminin</label>
                                          </div>
                                        </div> 
                                  </div><br> 
                                

                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-12 grid-margin stretch-card">
                            <div class="card">
                              <div class="card-body">
                                <h4>Identité  du contribuable</h4>  <br>
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>CIN ou Numéro passeport ou carte de résident</p>
                                      <input class="form-control form-control-sm" type="text" name="cin">
                                    </div>
                                    @if($errors->has('cin'))
                                      <p class="text-danger">{{ $errors->first('cin') }}</p>
                                    @endif
                                  </div> 
                                 
                           
                               
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de delivrance cin (passeport ou carte de résident)</p>
                                      <input class="form-control form-control-sm" type="date" name="date_del_cin">
                                    </div>
                                    @if($errors->has('date_del_cin'))
                                      <p class="text-danger">{{ $errors->first('date_del_cin') }}</p>
                                    @endif 
                                  </div> 
                                  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Lieu de delivrance cin (passeport ou carte de résident)</p>
                                      <input class="form-control form-control-sm" type="text"  name="lieu_del_cin">
                                    </div>
                                    @if($errors->has('lieu_del_cin'))
                                      <p class="text-danger">{{ $errors->first('lieu_del_cin') }}</p>
                                    @endif 
                                  </div> 
                                 
                                 
                                  </div>
                              </div>
                          </div>        
                          <div class="col-lg-12 grid-margin stretch-card">
                            <div class="card">
                              <div class="card-body">
                              
                                <h4>Regime fiscal du contribuable</h4>  <br>        
                                 <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Regime fiscal</p>
                                      <input class="form-control form-control-sm" type="text" id="reg_fisc" name="reg_fisc">
                                    </div>
                                  </div>  
                                 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>date agrément</p>
                                      <input class="form-control form-control-sm" type="date" id="date_agre" name="date_agre">
                                    </div>
                                  </div> 
                                 </div> 
                                
                                 <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Ref agrément</p>
                                      <input class="form-control form-control-sm" type="text" id="ref_agre" name="ref_agre">
                                    </div>
                                  </div> 
                                
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Periode de grace</p>
                                      <input class="form-control form-control-sm" type="text" id="peri_grace" name="peri_grace">
                                    </div>
                                  </div> 
                                 </div> 
                                
                                
                                <div class="row">  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Date de creation</p>
                                      <input class="form-control form-control-sm" type="date" id="date_creation" name="date_creation">
                                    </div>
                                  </div> 
                                
                                  
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>Capital</p>
                                      <input class="form-control form-control-sm" type="text" id="capital" name="capital">
                                    </div>
                                  </div>
                                </div> 
                                <div class="row"> 
                                  <div class="col-md-5">
                                    <div class="form-group">
                                      <p>RIB</p>
                                      <input class="form-control form-control-sm" type="text" id="rib" name="rib">
                                    </div>
                                  </div> 
                                </div> 
                                 
                              </div>
                          </div> 
                                  
                                  <div class="row">
                                    <div class="col-sm-3">
                                      
                                    </div>
                                    <div class="col-sm-3">
                                      <button type="submit" class="btn btn-success btn-block">
                                        <i class="mdi mdi-file-document"></i>
                                            Enregister
                                      </button>
                                    </div>
                                   
                                  </div>
                                
                                </form> 
                            
                                @if(Session::has('success'))
                                  <div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="close"> 
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                    <strong>succées! {{ Session::get('success') }}</strong>
                                  </div>
                                @endif
                      </div>      
                      </div>   

              </div>
       
     
              
             

            </div>
            </div>
            </div>
            </div>
          </div>
        </div>
      </div>

  @endsection