@extends('layouts.application')

@section('content')
<div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Inscription</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item">Renseignement</li>
                <li class="breadcrumb-item">Etablissement</li>
                <li class="breadcrumb-item">Dirigeant</li>
                <li class="breadcrumb-item active">Vehicules</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <section class="content">

        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card shadow">
                <div class="card-header border-0">
                  
                    <center><h1 class="mb-0" style="color:green;"><small>Renseignement sur les vehicules du contribuables</small></h1></center>
                </div>
                @if(Auth::guard('admin')->check())
                  <div class="table-responsive">
                
                          @if(Session::has('success'))
                                <div class="col-md-6">
                                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <strong>Succés! </button> {{Session::get('success')}}
                                  </div>
                                </div>
                          @endif
                          
                       
                          <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                              <tr>
                              <th>Id personne</th>
                                <th>Numéro vehicule</th>
                                <th>Marque</th>
                                <th>Genre</th>
                                <th>Type</th>
                                <th>Puissance</th>
                                <th>Nombre de place</th>
                                <th>Charge</th>
                                <th>Poid</th>
                                <th>Date de circulation</th>
                                <th>A usage professionnel</th>
                                <th>Date de début</th>
                                <th>Exploitation</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody>
                            
                       
                        
                                <tr scope="row">
                                      <td >{{ $vehicule->pers_phys_id }}</td>
                                      <td >{{ $vehicule->num_veh }}</td>
                                      <td >{{ $vehicule->marque }}</td>
                                      <td >{{ $vehicule->genre }}</td>
                                      <td >{{ $vehicule->type }}</td>
                                      <td>{{ $vehicule->puissance }}</td>
                                      <td>{{ $vehicule->nbr_place }}</td>
                                      <td>{{ $vehicule->charge }}</td>
                                      <td>{{ $vehicule->poid}}</td>
                                      <td>{{ $vehicule->date_circ}}</td>
                                      <td>{{ $vehicule->usage_proff}}</td>
                                      <td>{{ $vehicule->date_debut}}</td>
                                      <td>{{ $vehicule->exploitation}}</td>
                                      
                                      <td class="text-right">
                                        <a class="btn btn-danger btn-sm"  href="{{route('vehicules.delete',['id'=>$vehicule->id]) }}"><i  class="fas fa-trash"></i></a>
                                      </td>
                                </tr>

                            
                              
                            
                                
                            
                            
                              </tbody>
                          </table>
                      
                  </div>
                @endif
                <div class="card-footer py-4">
                  <center><a href="{{ route('vehicules.add') }}" class="btn btn-outline-success">Plus des vehicules</a></center>
                </div>
              </div> 
            </div> 
            <div class="col-12">
                <button type="reset" class="btn btn-secondary">Annuler</button>
                <a href="{{ route('interlocuteurs.add') }}" class="btn btn-outline-success float-right">Suivant</a>
            </div>
          </div>
        </div>
      </section>
</div>
@endsection