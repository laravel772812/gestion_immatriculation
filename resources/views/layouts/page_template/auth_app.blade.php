
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>

<div class="main-panel">
    @nclude('layouts.navbars.navs.auth_app')
    @yield('content')
   
</div>