
@include('layouts.navbars.nav')
<div class="main-content">
  
    @include('layouts.navbars.navs.header')
    <div class="container-fluid">
    @yield('content')
    </div>
    @include('layouts.foot')
   
</div>