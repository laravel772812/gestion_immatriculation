<div class="main-content">
  
     <!-- Header -->
     <div class="header bg-white pb-4 pt-5 pt-lg-8 d-flex align-items-center" style="min-height: 300px; background-image: url({{asset('plug/img/theme/republik.jpg')}}); background-repeat:no-repeat; background-position: center top;">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-Maroon">Bienvenu!</h1>
              <img src="{{asset('plug/img/brand/logo.png')}}" alt="Thumbnail Image" class="rounded img-raised">
              <p class="text-lead text-light">Connectez-vous s'il vous plait</p>
            </div>
          </div>
        </div>
      </div>
     
    </div>
        @yield('content')
      
    </div>
</div>
