<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-gradient-chocolate" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="./index.html">
        <img src="{{asset('plug/img/brand/blue.png')}}" class="navbar-brand-img" alt="...">
      </a>
    
   <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-dashboards">
                <i class="ni ni-shop text-primary"></i>
                <span class="nav-link-text">Accueil</span>
              </a>
              <div class="collapse show" id="navbar-dashboards">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                  
                  </li>
                  
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                <i class="ni ni-ungroup text-orange"></i>
                <span class="nav-link-text">Immatriculation</span>
              </a>
              <div class="collapse" id="navbar-examples">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                      <a href="../../pages/examples/login.html" class="nav-link">
                        <span class="sidenav-mini-icon"> <i class="ni ni-ungroup"></i> </span>
                        <span class="sidenav-normal">&nbsp A propos </span>
                      </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{route('inscription')}}" class="nav-link">
                      <span class="sidenav-mini-icon"> <i class="fas fa-edit"> </i> </span>
                      <span class="sidenav-normal">&nbsp Inscription </span>
                    </a>
                  </li>
                  @if(Auth::guard('admin')->check())
                  <li class="nav-item">
                    <a href="{{route('liste_inscrit')}}" class="nav-link">
                      <span class="sidenav-mini-icon"> <i class="fas fa-edit"> </i> </span>
                      <span class="sidenav-normal">&nbsp liste inscrit </span>
                    </a>
                  </li>
                  @endif
                 
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-components" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-components">
                <i class="ni ni-ui-04 text-info"></i>
                <span class="nav-link-text">Contact</span>
              </a>
              <div class="collapse" id="navbar-components">
                <ul class="nav nav-sm flex-column">
                  
                  
                </ul>
              </div>
            </li>
         
            <li class="nav-item">
              <a class="nav-link" href="../../pages/widgets.html">
                <i class="ni ni-archive-2 text-green"></i>
                <span class="nav-link-text">Site DGI</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../pages/charts.html">
                <i class="ni ni-chart-pie-35 text-info"></i>
                <span class="nav-link-text">Partenaire DGI</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../pages/calendar.html">
                <i class="ni ni-calendar-grid-58 text-red"></i>
                <span class="nav-link-text">Contact DGI</span>
              </a>
            </li>
           
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Documentation</span>
            <span class="docs-mini">D</span>
          </h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="../../docs/getting-started/overview.html" target="_blank">
                <i class="ni ni-spaceship"></i>
                <span class="nav-link-text">Getting started</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../docs/foundation/colors.html" target="_blank">
                <i class="ni ni-palette"></i>
                <span class="nav-link-text">Foundation</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../../docs/components/alerts.html" target="_blank">
                <i class="ni ni-ui-04"></i>
                <span class="nav-link-text">Components</span>
              </a>
            </li>
            @if(Auth::guard('web')->check())
              <li class="nav-item">
                <a class="nav-link" href="{{route('user.logout')}}" >
                  <i class="ni ni-chart-pie-35"></i>
                  <span class="nav-link-text">Logout</span>
                </a>
              </li>
             @else 
              <li class="nav-item">
                  <a class="nav-link" href="{{route('admin.logout')}}" >
                    <i class="ni ni-chart-pie-35"></i>
                    <span class="nav-link-text">Logout</span>
                  </a>
                </li>
              @endif
          </ul>
        </div>
      </div>
    </div>
  </nav>