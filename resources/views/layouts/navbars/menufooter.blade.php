    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
     
        <a data-toggle="tooltip" data-placement="top" title="Se deconnecter" href="{{route('admin.logout')}}">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
    </div>
</div>