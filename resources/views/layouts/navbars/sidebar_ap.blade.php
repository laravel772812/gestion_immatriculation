  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark sidebar-dark-success">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('dist/img/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light" style="font-size:15px;">Direction Regional des Impôts</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('dist/img/pic-1.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Accueil
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
             
            </ul>
          </li>
         
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit "></i>
              <p>
                Immatriculation
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('inscription')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inscription</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/UI/icons.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Se connecter</p>
                </a>
              </li>
        
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tree "></i>
              <p>
                Espace administrateur
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{route('confirmation')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Confirmation d'inscription</p>
                </a>
              </li>
              @if(Auth::guard('admin')->check())
              <li class="nav-item">
                <a href="{{route('inscrit')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Gerer inscription</p>
                </a>
              </li>
             
            
              <li class="nav-item">
                <a href="{{route('utilisateur.liste')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Gerer compte utilisateur</p>
                </a>
              </li>

                  <li class="nav-item">
                <a href="{{route('statistique')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Statistique</p>
                </a>
              </li>
              @endif
            </ul>
          </li>
       
          <li class="nav-header">Sortir</li>
          @if(Auth::guard('admin')->check())
          <li class="nav-item">
            <a href="{{route('admin.logout')}}" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Se deconnecter</p>
            </a>
          </li>
          @endif
          @if(Auth::guard('web')->check())
          <li class="nav-item">
            <a href="{{route('user.logout')}}" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Se deconnecter</p>
            </a>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>