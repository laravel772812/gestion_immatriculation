 <!-- top navigation -->
 <div class="top_nav" >
    <div class="nav_menu" style="min-height: 300px; background-color:white; background-image: url({{asset('plug/img/theme/republik.jpg')}}); background-repeat:no-repeat; background-position: center top;">
    <nav>
        <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="{{asset('plug/img/brand/logo.png')}}" alt="">  {{ Auth::user()->name }} <span class="caret"></span>
            <span class=" fa fa-angle-down"></span>
            </a>
            <ul class="dropdown-menu dropdown-usermenu pull-right">
       
          
            <li><a href="{{route('admin.logout')}}"><i class="fa fa-sign-out pull-right"></i>Se deconnecter</a></li>
            </ul>
        </li>

      
        </ul>
    </nav>
    </div>
</div>
<!-- /top navigation -->
