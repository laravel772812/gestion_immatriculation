 <!-- header -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
   <div class="" style="margin-left: inherit;">
    <img src="{{asset('dist/img/republik.jpg')}}">
   </div>
</nav>
  <!-- /.header -->
   <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-light navbar-light" >
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item" style="color:white">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Accueil</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('inscription')}}" class="nav-link">Inscription</a>
      </li>
      @if(Auth::guard('admin')->check())
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('confirmation')}}" class="nav-link">Confirmation</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('utilisateur.liste')}}" class="nav-link">Gerer compte</a>
      </li>
      @endif
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->