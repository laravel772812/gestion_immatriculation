<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @if(Auth::guard('web')->check())
  <title> Espace contribuable</title>
  <link href="{{asset('dist/img/logo-mfb.png')}}" rel="icon" type="image/png">
  @endif
  @if(Auth::guard('admin')->check())
  <title> Espace administrateur</title>
  <link href="{{asset('dist/img/logo-mfb.png')}}" rel="icon" type="image/png">
  @endif
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
.pagination {
  
    float:left;
    display: inline-block;
}

.pagination li {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
  
    border: 1px solid #ddd;
    margin: 0 4px;
}


.pagination li.active {
    background-color: #4CAF50;
    color: white;
    border: 1px solid #4CAF50;
}


.pagination li:hover:not(.active) {background-color: #ddd;
}
.table thead th{
    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.65rem;text-transform: uppercase;letter-spacing: 1px;border-bottom: 1px solid #e9ecef;white-space: nowrap;
}
table td{
    padding-top: 0.75rem;padding-bottom: 0.75rem;font-size: 0.90rem;font-weight: 600;;text-align: center;letter-spacing: 1px;white-space: nowrap;vertical-align: baseline;border-bottom: 1px solid #e9ecef; padding-top: 0.75rem;padding-bottom: 0.75rem;border-bottom: 1px solid #e9ecef;color:gray ;
}
</style>
  
</head>
    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">
            @auth
                @include('layouts.page_template.body')
            @endauth
            <!-- jQuery -->
            <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
            <!-- jQuery UI 1.11.4 -->
            <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script>
            $.widget.bridge('uibutton', $.ui.button)
            </script>
            <!-- Bootstrap 4 -->
            <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
            <!-- ChartJS -->
            <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
            <!-- Sparkline -->
            <script src="{{asset('plugins/sparklines/sparkline.js')}}"></script>
            <!-- JQVMap -->
            <script src="{{asset('plugins/jqvmap/jquery.vmap.min.js')}}"></script>
            <script src="{{asset('plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
            <!-- jQuery Knob Chart -->
            <script src="{{asset('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
            <!-- daterangepicker -->
            <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
            <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
            <!-- Tempusdominus Bootstrap 4 -->
            <script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
            <!-- Summernote -->
            <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
            <!-- overlayScrollbars -->
            <script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
            <!-- AdminLTE App -->
            <script src="{{asset('dist/js/adminlte.js')}}"></script>
            <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
            <script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
            <!-- AdminLTE for demo purposes -->
            <script src="{{asset('dist/js/demo.js')}}"></script>
           
        </div>
    </body>
</html>
