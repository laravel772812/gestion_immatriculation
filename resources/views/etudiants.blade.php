@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                
                   <span>Liste des etudiants</span>
                   <a href="{{ route('etudiant.add') }}" class="btn btn-xs pull-right btn-success">Ajouter un etudiant</a>
                
                </div>

                <div class="panel-body">
                   <table class="table table-bordered table-stripped">
                        <thead>
                          <tr>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>Email</th>
                            <th>Detail</th>
                            <th>Editer</th>
                            <th>Supprimer</th>
                          </tr>
                        </thead>
                        </tbody>
                                @foreach($etudiants as $etudiant)
                                    <tr>
                                        <td>{{ $etudiants->lastname }}</td>
                                        <td>{{ $etudiants->firstname }}</td>
                                        <td>{{ $etudiants->email  }}</td>
                                        <td>
                                        <a href ="{{ route('etudiant.show',['id' =>$etudiants->id]) }}" class="btn btn-sm btn-primary">Details</a>
                                        </td>
                                        <td>
                                        <a href ="{{ route('etudiant.edit',['id' =>$etudiants->id]) }}" class="btn btn-sm btn-warning">Editer</a>
                                        </td>
                                        <td>
                                        <a href ="{{ route('etudiant.delete',['id' =>$etudiants->id]) }}" class="btn btn-sm btn-danger">Supprimer</a>
                                        </td>
                                    </tr>
                                @endforeach    
                        </tbody>
                   </table>
                </div>
                <div class="text-center panel-footer">
                  
                </div>
            </div>
        </div>
        <div class=col-md-2>
           

             @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    succées! {{ Session::get('success') }}
                                   
                </div>
             @endif
        </div>
    </div>
</div>
@endsection